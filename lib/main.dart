// @dart=2.9
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:leader_upgrade/general/blocks/lang_cubit/lang_cubit.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'general/MyApp.dart';

void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //WidgetsFlutterBinding.ensureInitialized();
  //await GetStorage.init();
  //await Firebase.initializeApp();
  //await PushNotificationsService().init();
  //FirebaseMessaging.onBackgroundMessage();
  runApp(
    BlocProvider(
      create: (BuildContext context) => LangCubit(),
      child:  Phoenix(child: MyApp()),
    )
  );
}
