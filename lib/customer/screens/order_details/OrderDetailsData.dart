part of 'OrderDetailsImports.dart';

class OrderDetailsData {
  final GenericBloc<OrderDetailsModel?> detailsBloc = new GenericBloc(null);

  void fetchData(BuildContext context, int orderId,
      {bool refresh = true}) async {
    var data =
        await CustomerRepository(context).getOrderDetails(refresh, orderId);
    detailsBloc.onUpdateData(data);
  }

  void showBillDialog(BuildContext context) {
    showModalBottomSheet(
      isDismissible: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      context: context,
      builder: (context) => BuildBillDialog(
        orderDetailsModel: detailsBloc.state.data!,
      ),
    );
  }
}
