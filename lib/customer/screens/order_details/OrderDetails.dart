part of 'OrderDetailsImports.dart';

class OrderDetails extends StatefulWidget {
  final int orderId;

  const OrderDetails({required this.orderId,  });

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  final OrderDetailsData orderDetailsData = new OrderDetailsData();

  @override
  void initState() {
    orderDetailsData.fetchData(context, widget.orderId, refresh: false);
    orderDetailsData.fetchData(context, widget.orderId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(
        title: tr(context, "orderDetails"),
        actions: [CartIcon()],
      ),
      body: BlocBuilder<GenericBloc<OrderDetailsModel?>,
          GenericState<OrderDetailsModel?>>(
        bloc: orderDetailsData.detailsBloc,
        builder: (_, state) {
          if (state is GenericUpdateState) {
            return Column(
              children: [
                Flexible(
                  child: ListView(
                    children: [
                      BuildBody(
                        orderDetailsData: orderDetailsData,
                        model: state.data!,
                      ),
                      BuildReturnMsg(
                        msg: state.data!.reason ?? "",
                        statusId: state.data!.statusId,
                      ),
                      BuildDisplayBill(
                        statusId: state.data!.statusId,
                        orderDetailsData: orderDetailsData,
                      )
                    ],
                  ),
                ),
                state.data!.canReturned == false
                    ? const SizedBox()
                    : BuildReturnButton(
                        statusId: state.data!.statusId,
                        orderDetailsData: orderDetailsData,
                        orderId: state.data!.id,
                      )
              ],
            );
          } else {
            return LoadingDialog.showLoadingView();
          }
        },
      ),
    );
  }
}
