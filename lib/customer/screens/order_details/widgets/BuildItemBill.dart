part of 'OrderDetailsWidgetsImports.dart';

class BuildItemBill extends StatelessWidget {
  final String title;
  final String details;

  const BuildItemBill({required this.title, required this.details});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: MyText(
            title: "$title  ",
            color: MyColors.primary,
            size: 12,
          ),
        ),
        MyText(
          title: details,
          color: MyColors.black,
          size: 12,
        )
      ],
    );
  }
}
