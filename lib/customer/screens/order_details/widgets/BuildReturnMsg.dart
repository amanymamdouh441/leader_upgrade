part of 'OrderDetailsWidgetsImports.dart';

class BuildReturnMsg extends StatelessWidget {
  final String msg;
  final int statusId;

  const BuildReturnMsg({required this.msg, required this.statusId});

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: statusId == OrderStatus.ReturnRequest.getValue(),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.symmetric(vertical: 7),
              child: MyText(
                title: tr(context, "returnReason"),
                color: MyColors.black,
                size: 13,
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: MyColors.white,
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: MyColors.greyWhite,
                    spreadRadius: 1,
                    blurRadius: 1,
                  ),
                ],
              ),
              child: MyText(
                title: msg,
                color: MyColors.black,
                size: 12,
              ),
            )
          ],
        ),
      ),
    );
  }
}
