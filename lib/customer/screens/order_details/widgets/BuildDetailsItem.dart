part of 'OrderDetailsWidgetsImports.dart';

class BuildDetailsItem extends StatelessWidget {
  final String title;
  final String details;

  const BuildDetailsItem({required this.title, required this.details});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        children: [
          MyText(
            title: "$title : ",
            color: MyColors.primary,
            size: 14,
          ),
          MyText(
            title: details,
            color: MyColors.black,
            size: 13,
          )
        ],
      ),
    );
  }
}
