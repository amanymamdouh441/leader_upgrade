part of 'OrderDetailsWidgetsImports.dart';

class BuildBillDialog extends StatelessWidget {
  final OrderDetailsModel orderDetailsModel;

  const BuildBillDialog({required this.orderDetailsModel});

  @override
  Widget build(BuildContext context) {
    var totalPrice = orderDetailsModel.total + orderDetailsModel.shippingFee;
    return ListView(
      padding: const EdgeInsets.only(top: 10, bottom: 40),
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            IconButton(
              icon: Icon(Icons.close),
              onPressed: () => AutoRouter.of(context).pop(),
            )
          ],
        ),
        Image.asset(
          Res.logo,
          width: 90,
          height: 90,
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Divider(color: MyColors.black, height: 30),
              Row(
                children: [
                  BuildDetailsItem(
                    title: tr(context, "billNumb"),
                    details: "${orderDetailsModel.id}",
                  ),
                  BuildDetailsItem(
                    title: tr(context, "date"),
                    details: "${orderDetailsModel.date}",
                  ),
                ],
              ),
              Row(
                children: [
                  BuildDetailsItem(
                    title: tr(context, "time"),
                    details: "${orderDetailsModel.time}",
                  ),
                ],
              ),
              Divider(color: MyColors.black, height: 30),
              MyText(
                title: tr(context, "orderDetails"),
                color: MyColors.black,
                size: 13,
                fontWeight: FontWeight.bold,
              ),
              ...List.generate(
                orderDetailsModel.orderInfo.length,
                (index) => BuildItemBill(
                    title:
                        "${orderDetailsModel.orderInfo[index].name} x${orderDetailsModel.orderInfo[index].qty}",
                    details:
                        "${orderDetailsModel.orderInfo[index].price} ${tr(context, "sar")} "),
              ),
              Divider(color: MyColors.black, height: 30),
              MyText(
                title: tr(context, "totalOrder"),
                color: MyColors.black,
                size: 13,
                fontWeight: FontWeight.bold,
              ),
              BuildItemBill(
                  title: tr(context, "paymentWay"),
                  details: "${orderDetailsModel.typePay}  "),
              BuildItemBill(
                  title: tr(context, "shippingFee"),
                  details:
                      "${orderDetailsModel.shippingFee} ${tr(context, "sar")} "),
              BuildItemBill(
                  title: tr(context, "tax"),
                  details: "${orderDetailsModel.commission} % "),
              BuildItemBill(
                  title: tr(context, "TotalWithTax"),
                  details:
                      "${orderDetailsModel.total.toStringAsFixed(2)} ${tr(context, "sar")} "),
              BuildItemBill(
                  title: tr(context, "total"),
                  details:
                      "${totalPrice.toStringAsFixed(2)} ${tr(context, "sar")} "),
              Divider(color: MyColors.black, height: 30),
              Center(
                child: MyText(
                  title:
                      "${tr(context, "taxNumb")} : ${orderDetailsModel.taxNumber}",
                  color: MyColors.primary,
                  size: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Center(
                child: CachedImage(
                  url: orderDetailsModel.invoice ?? "",
                  height: 150,
                  width: 150,
                ),
              ),
              // Center(
              //   child: Container(
              //     height: 150,
              //     width: 150,
              //     margin: const EdgeInsets.symmetric(vertical: 20),
              //     child: QrImage(
              //       data: orderDetailsModel.invoice ?? "",
              //       version: QrVersions.auto,
              //       size: 200.0,
              //     ),
              //   ),
              // )
            ],
          ),
        )
      ],
    );
  }
}
