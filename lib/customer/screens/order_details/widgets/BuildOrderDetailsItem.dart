part of 'OrderDetailsWidgetsImports.dart';

class BuildOrderDetailsItem extends StatelessWidget {
  final OrderInfoModel infoModel;

  const BuildOrderDetailsItem({required this.infoModel});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 10, left: 10, bottom: 5),
          child: Divider(thickness: 1),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      title: infoModel.name,
                      color: MyColors.blackOpacity,
                      size: 10,
                    ),
                    MyText(
                      title: "${infoModel.price} ${tr(context, "sr")}",
                      color: MyColors.blackOpacity,
                      size: 10,
                    ),
                    MyText(
                      title: "${tr(context, "qty")} :  ${infoModel.qty}",
                      color: MyColors.blackOpacity,
                      size: 10,
                    ),
                    Wrap(
                      spacing: 15,
                      runSpacing: 10,
                      children:
                          List.generate(infoModel.subSpecs.length, (position) {
                        return BuildSubSpecsItem(
                            model: infoModel.subSpecs[position]);
                      }),
                    ),
                  ],
                ),
              ),
              CachedImage(
                url: infoModel.fileName,
                height: 100,
                width: 90,
              )
            ],
          ),
        ),
      ],
    );
  }
}
