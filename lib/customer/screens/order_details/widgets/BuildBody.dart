part of 'OrderDetailsWidgetsImports.dart';

class BuildBody extends StatelessWidget {
  final OrderDetailsData orderDetailsData;
  final OrderDetailsModel model;

  const BuildBody({required this.orderDetailsData, required this.model});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: MyColors.greyWhite,
            spreadRadius: 2,
            blurRadius: 2,
          ),
        ],
      ),
      child: Column(
        children: [
          BuildOrderInfo(model: model),
          Column(
            children: List.generate(
              model.orderInfo.length,
              (index) => BuildOrderDetailsItem(
                infoModel: model.orderInfo[index],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
