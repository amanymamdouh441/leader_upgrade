part of 'OrderDetailsWidgetsImports.dart';

class BuildDisplayBill extends StatelessWidget {
  final int statusId;
final OrderDetailsData orderDetailsData;
  const BuildDisplayBill({required this.statusId,required this.orderDetailsData});

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: statusId == OrderStatus.Finished.getValue(),
      child: InkWell(
        onTap: ()=>orderDetailsData.showBillDialog(context),
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: MyColors.greyWhite,
                spreadRadius: 1,
                blurRadius: 1,
              ),
            ],
          ),
          child: MyText(
            title: tr(context, "displayBill"),
            color: MyColors.primary,
            fontWeight: FontWeight.bold,
            size: 13,
          ),
        ),
      ),
    );
  }
}
