part of 'OrderDetailsWidgetsImports.dart';

class BuildOrderInfo extends StatelessWidget {
  final OrderDetailsModel model;

  const BuildOrderInfo({required this.model});

  @override
  Widget build(BuildContext context) {
    var totalPrice=model.total+model.shippingFee;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: MyText(
                  title: "${tr(context, "orderNum")} : ${model.id}",
                  color: MyColors.black,
                  size: 10,
                ),
              ),
              Container(
                height: 12,
                width: 12,
                decoration: BoxDecoration(
                    color: Colors.green[400], shape: BoxShape.circle),
                margin: const EdgeInsets.symmetric(horizontal: 7),
              ),
              MyText(
                title: model.status,
                color: MyColors.blackOpacity,
                size: 10,
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(5),
            child: Divider(thickness: 1),
          ),
          MyText(
            title: "${tr(context, "orderDate")} : ${model.date}",
            color: MyColors.blackOpacity,
            size: 10,
          ),
          Visibility(
            visible: model.statusId != OrderStatus.ReturnRequest.getValue(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                  title: "${tr(context, "payMethod")} : ${model.typePay}",
                  color: MyColors.blackOpacity,
                  size: 10,
                ),
                MyText(
                  title:
                      "${tr(context, "shippingFee")} :  ${model.shippingFee} ${tr(context, "sr")}",
                  color: MyColors.blackOpacity,
                  size: 10,
                ),
                MyText(
                  title:
                      "${tr(context, "tax")} :  ${model.commission} %",
                  color: MyColors.blackOpacity,
                  size: 10,
                ),
                MyText(
                  title:
                  " ${tr(context, "TotalWithTax")} : ${model.total.toStringAsFixed(2)} ${tr(context, "sr")}",
                  color: MyColors.blackOpacity,
                  size: 10,
                ),
                // MyText(
                //   title:
                //       "${tr(context, "price")} : ${model.total} ${tr(context, "includeAddedValue")}",
                //   color: MyColors.blackOpacity,
                //   size: 10,
                // ),
                MyText(
                  title:
                      "${tr(context, "total")} : ${totalPrice.toStringAsFixed(2)} ${tr(context, "sr")}",
                  color: MyColors.blackOpacity,
                  size: 10,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: MyText(
              title: tr(context, "order"),
              color: MyColors.black,
              size: 12,
            ),
          ),
        ],
      ),
    );
  }
}
