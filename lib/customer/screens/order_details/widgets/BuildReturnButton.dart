part of 'OrderDetailsWidgetsImports.dart';

class BuildReturnButton extends StatelessWidget {
  final int statusId;
  final OrderDetailsData orderDetailsData;
  final int orderId;

  const BuildReturnButton(
      {required this.statusId,
      required this.orderDetailsData,
      required this.orderId});

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: statusId == OrderStatus.Finished.getValue(),
      child: DefaultButton(
        title: tr(context, "returnOrder"),
        margin: const EdgeInsets.all(20),
        onTap: () => AutoRouter.of(context).push(ReturnRoute(orderId: orderId)),
      ),
    );
  }
}
