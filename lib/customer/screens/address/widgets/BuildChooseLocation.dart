part of 'AddressWidgetsImports.dart';

class BuildChooseLocation extends StatelessWidget {
  final AddressData addressData;

  const BuildChooseLocation({required this.addressData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationCubit, LocationState>(
      bloc: addressData.locationCubit,
      builder: (context, state) {
        if (state.model?.address == "") {
          return InkWell(
            onTap: () => addressData.onLocationClick(context),
            child: Container(
              height: 100,
              margin: const EdgeInsets.all(10),
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: MyColors.greyWhite,
                        spreadRadius: 2,
                        blurRadius: 2)
                  ]),
              alignment: Alignment.center,
              child: MyText(
                title: tr(context, "selectLocation"),
                color: MyColors.blackOpacity,
                size: 11,
              ),
            ),
          );
        } else {
          return BuildLocationItem(
            locationCubit: state.model!,
            addressData: addressData,
          );
        }
      },
    );

    /// >>>>>>>>>>>>>>select from User addresses ....



    /*Container(
      height: 100,
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
          ]),
      alignment: Alignment.center,
      child: DropdownTextField<UserAddressesModel>(
        dropKey: addressData.returnId,
        label: tr(context, "choose"),
        searchHint: tr(context, "search"),
        enableColor: MyColors.grey,
        selectedItem: addressData.userAddressesModel,
        margin: const EdgeInsets.symmetric(vertical: 10),
        validate: (UserAddressesModel value) =>
            value.validateDropDown(context),
        onChange: addressData.onSelectReturn,
        finData: (filter) async =>
        await CustomerRepository(context).getUserAddresses(true),
      ),
    );
*/
  }
}
