part of 'AddressWidgetsImports.dart';

class BuildLocationDetails extends StatelessWidget {
  final AddressData addressData;

  const BuildLocationDetails({required this.addressData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: addressData.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.all(10),
            child: MyText(
              title: tr(context, "locationDetails"),
              color: MyColors.blackOpacity,
              size: 12,
            ),
          ),
          GenericTextField(
            fieldTypes: FieldTypes.rich,
            controller: addressData.addressDetails,
            type: TextInputType.text,
            action: TextInputAction.done,
            validate: (value) => value!.noValidate( ),
            max: 5,
            hint: tr(context, "writeDetails"),
            margin: const EdgeInsets.symmetric(horizontal: 10),
          )
        ],
      ),
    );
  }
}
