import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/customer/screens/address/AddressImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/Inputs/DropdownTextField.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/validator/Validator.dart';

class BuildAramexCities extends StatelessWidget {
  final String? cityName;
  final Function onChange;
  final GlobalKey<State<StatefulWidget>>? dropKey;

  BuildAramexCities(
      {Key? key,
        required this.onChange,
      required this.cityName,
      required this.dropKey, })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
          ]),
      alignment: Alignment.center,
      child: DropdownTextField<String>(
        title: "aramix",
          itemAsString:(value)=>value ,
        dropKey: dropKey,
        label: tr(context, "choose"),
        searchHint: tr(context, "search"),
        enableColor: MyColors.grey,
        selectedItem: cityName,
        useName: false,
        margin: const EdgeInsets.symmetric(vertical: 10),
        validate: (value) => value.validateDropDown(context),
        onChange:(value)=> onChange(value!),
        onFind: (String cityName) async =>
        await CustomerRepository(context).getListAramexCities(cityName),
      )

    );
  }
}
