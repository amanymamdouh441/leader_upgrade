part of 'AddressImports.dart';

class Address extends StatefulWidget {
  final CartModel model;

  const Address({Key? key, required this.model}) : super(key: key);

  @override
  _AddressState createState() => _AddressState();
}

class _AddressState extends State<Address> {
  final AddressData addressData = AddressData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(title: tr(context, "deliverLocation")),
      body: Column(
        children: [
          Flexible(
            child: GenericListView(
              type: ListViewType.normal,
              padding: const EdgeInsets.all(10),
              children: [
                BuildChooseLocation(
                  addressData: addressData,
                ),
                // BuildLocationDetails(addressData: addressData),
                BuildAramexCities(
                  cityName: addressData.cityName ?? tr(context, 'chooseCity'),
                  dropKey: addressData.returnId,
                  onChange:(value)=> addressData.onSelectReturn(value),

                )
              ],
            ),
          ),
          DefaultButton(
            title: tr(context, "confirm"),
            margin: const EdgeInsets.all(20),
            onTap: () => addressData.navigateToCompleteOrder(
                context, widget.model, addressData.cityName),
          ),
        ],
      ),
    );
  }
}
