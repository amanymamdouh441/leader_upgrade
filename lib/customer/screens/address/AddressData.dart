part of 'AddressImports.dart';

class AddressData {
  final LocationCubit locationCubit = new LocationCubit();
  final GlobalKey<FormState> formKey = new GlobalKey();
  final TextEditingController addressDetails = new TextEditingController();
  final GlobalKey<DropdownSearchState> returnId = new GlobalKey();

  String? cityName;

  void onSelectReturn(String cityName) {
    this.cityName = cityName;
  }


  onLocationClick(BuildContext context) async {
    await Geolocator.requestPermission();
    var _loc = await Utils.getCurrentLocation();
    locationCubit.onLocationUpdated(LocationModel(
      lat: _loc?.latitude ?? 24.774265,
      lng: _loc?.longitude ?? 46.738586,
      address: "",
    ));
    Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (cxt) =>
            BlocProvider.value(
              value: locationCubit,
              child: LocationAddress(),
            ),
      ),
    );
  }

  void navigateToCompleteOrder(BuildContext context, CartModel cartModel,
      cityName) {
    if (
    // formKey.currentState!.validate()
    //     &&
        returnId.currentState!.getSelectedItem != null
    ) {
      if (locationCubit.state.model?.address == "") {
        CustomToast.showSimpleToast(msg: tr(context, "selectLocation"));
        return;
      }
      if (cityName == null) {
        CustomToast.showSimpleToast(msg: tr(context, 'chooseCity'));
        return;
      }
      AddOrderModel model = AddOrderModel(
        location: locationCubit.state.model?.address,
        lat: locationCubit.state.model?.lat.toString(),
        lng: locationCubit.state.model?.lng.toString(),
        addressDetails: addressDetails.text
      );
      AutoRouter.of(context).push(CompleteOrderRoute(
          addOrderModel: model,
          cartModel: cartModel,
          cityName: cityName
      ));
    } else {
      CustomToast.showSimpleToast(msg: tr(context, "selectLocation"));
    }
  }
}
