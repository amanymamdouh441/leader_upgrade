part of 'LanguagesImports.dart';

class LanguagesData {
  final GenericBloc<String> langCubit = GenericBloc("ar");
  final GlobalKey<CustomButtonState> btnKey =
      new GlobalKey<CustomButtonState>();

  void setLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var lang = prefs.getString("lang");
    langCubit.onUpdateData(lang ?? "ar");
  }

  setLanguage(BuildContext context) async {
    if (context.read<AuthCubit>().state.authorized) {
      await changeLanguage(context);
      print("============${langCubit.state.data}");
      await CustomerRepository(context).getCategories(true);
    } else {
      EasyLoading.dismiss().then((value) {
        Utils.changeLanguage(langCubit.state.data, context);
        Phoenix.rebirth(context);
      });
    }
  }

  Future<void> changeLanguage(BuildContext context) async {
    btnKey.currentState!.animateForward();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("lang", langCubit.state.data);
    print("___###__${langCubit.state.data}");
    Utils.changeLanguage(langCubit.state.data, context);
    await GeneralRepository(context).changeLanguage(langCubit.state.data);
    print("__***___${langCubit.state.data}");

    btnKey.currentState!.animateReverse();
  }
}
