import 'package:leader_upgrade/customer/screens/languages/LanguagesImports.dart';
import 'package:leader_upgrade/general/blocks/lang_cubit/lang_cubit.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/GenericListView.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'BuildLanguagesView.dart';
part 'BuildLangItem.dart';