part of 'LanguagesWidgetsImports.dart';

class BuildLanguagesView extends StatelessWidget {
  final LanguagesData languagesData;

  const BuildLanguagesView({required this.languagesData});

  @override
  Widget build(BuildContext context) {
     return BlocBuilder<GenericBloc<String>, GenericState<String>>(
      bloc: languagesData.langCubit,
      builder: (_, state) {
        return Flexible(
          child: GenericListView(
            type: ListViewType.normal,
            padding: const EdgeInsets.all(15),
            children: [
              BuildLangItem(
                title: tr(context, "langAr"),
                value: "ar",
                group: state.data,
                languagesData: languagesData,
                img: Res.arabic,
              ),
              BuildLangItem(
                title: tr(context,"langEn"),
                value: "en",
                group: state.data,
                languagesData: languagesData,
                img: Res.english,
              ),
            ],
          ),
        );
      },
    );
  }
}
