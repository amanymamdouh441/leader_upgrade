part of 'LanguagesWidgetsImports.dart';

class BuildLangItem extends StatelessWidget {
  final String title, value, group,img;
  final LanguagesData languagesData;

  BuildLangItem(
      {required this.value,
      required this.title,
      required this.group,
        required this.img,
      required this.languagesData});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: Image.asset(img,scale: 3),
          title: MyText(title: title, color: MyColors.blackOpacity, size: 12),
          trailing: Radio<String>(
            value: value,
            groupValue: group,
            activeColor: MyColors.primary,
            onChanged: (value) => languagesData.langCubit.onUpdateData(value!),
          ),
        ),
        Divider(color: MyColors.blackOpacity)
      ],
    );
  }
}
