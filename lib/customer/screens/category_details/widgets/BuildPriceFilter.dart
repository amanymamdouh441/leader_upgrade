part of 'CategoryDetailsWidgetsImports.dart';

class BuildPriceFilter extends StatelessWidget {
  final CategoryDetailsData categoryDetailsData;
  const BuildPriceFilter({required this.categoryDetailsData});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10, right: 10, left: 10),
          child: MyText(
            title: tr(context,"price"),
            color: MyColors.black,
            size: 12,
          ),
        ),
        BlocBuilder<GenericBloc<PriceRangeModel?>, GenericState<PriceRangeModel?>>(
          bloc: categoryDetailsData.rangeCubit,
          builder: (_,state){
            if (state.data==null) {
              return Container();
            }
            return Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 5, right: 10, left: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                        title: "${state.data!.value.start.toInt()}  ${tr(context, "sr")}",
                        color: MyColors.blackOpacity,
                        size: 10,
                      ),
                      MyText(
                        title: "${state.data!.value.end.toInt()}  ${tr(context, "sr")}",
                        color: MyColors.blackOpacity,
                        size: 10,
                      ),
                    ],
                  ),
                ),
                RangeSlider(
                  values: state.data!.value,
                  min: state.data!.initial.start,
                  max: state.data!.initial.end,
                  divisions: 10,
                  activeColor: MyColors.primary,
                  labels: RangeLabels(
                    state.data!.value.start.round().toString(),
                    state.data!.value.end.round().toString(),
                  ),
                  onChanged: (RangeValues values) => categoryDetailsData.changePriceValue(values, context),
                  inactiveColor: MyColors.greyWhite,

                ),
              ],
            );
          },
        ),
      ],
    );
  }
}
