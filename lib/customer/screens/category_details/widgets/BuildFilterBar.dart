part of 'CategoryDetailsWidgetsImports.dart';

class BuildFilterBar extends StatelessWidget {
  final CategoryDetailsData detailsData;

  const BuildFilterBar({required this.detailsData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<SpecificationModel>>, GenericState<List<SpecificationModel>>>(
      bloc: detailsData.specCubit,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return Container(
            height: 40,
            padding: const EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(color: MyColors.white, boxShadow: [
              BoxShadow(
                  color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2),
            ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BuildFilterItem(
                  title: tr(context, "arrange"),
                  onTap: () => detailsData.filterBottomSheet(context, detailsData),
                ),

                ...state.data.take(2).map((e){
                  return BuildFilterItem(
                    title: e.name,
                    onTap: () => detailsData.specificBottomSheet(context, e,state.data.indexOf(e)),
                  );
                }),

                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: VerticalDivider(thickness: 1, color: MyColors.black),
                ),

                BuildFilterItem(
                  title: tr(context, "filter"),
                  filter: true,
                  onTap: () {
                    detailsData.scaffold.currentState!.openDrawer();
                  },
                ),
              ],
            ),
          );
        }
        return Shimmer(
          duration: Duration(seconds: 3),
          interval: Duration(seconds: 5),
          color: MyColors.primary,
          child: Container(
            height: 40,
            decoration: BoxDecoration(color: MyColors.white, boxShadow: [
              BoxShadow(
                  color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2),
            ]),
          ),
        );
      },
    );
  }
}
