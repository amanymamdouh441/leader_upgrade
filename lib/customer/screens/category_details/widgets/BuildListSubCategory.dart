part of 'CategoryDetailsWidgetsImports.dart';

class BuildListSubCategory extends StatelessWidget {
  final ParentSubCategory subCategory;
  final CategoryDetailsData categoryDetailsData;
  final int levelIndex;

  const BuildListSubCategory({
    required this.subCategory,
    required this.categoryDetailsData,
    required this.levelIndex,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      color: Colors.white,
      padding: EdgeInsets.symmetric(vertical: 6),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: subCategory.subCategory.length,
        itemBuilder: (_, index) {
          return BuildSubCategoryItem(
            categoryDetailsData: categoryDetailsData,
            subCategory: subCategory.subCategory[index],
            selected: subCategory.selectedId,
            index: levelIndex,
          );
        },
      ),
    );
  }
}
