part of 'CategoryDetailsWidgetsImports.dart';

class BuildView extends StatelessWidget {
  final CategoryDetailsData categoryDetailsData;
  final int id;

  const BuildView({required this.categoryDetailsData, required this.id});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<ParentSubCategory>>,
        GenericState<List<ParentSubCategory>>>(
      bloc: categoryDetailsData.parentSubCatsCubit,
      builder: (_, state) {
        if (state is GenericUpdateState) {
          return Column(
            children: [
              ...List.generate(
                state.data.length,
                (index) => BuildListSubCategory(
                  subCategory: state.data[index],
                  categoryDetailsData: categoryDetailsData,
                  levelIndex: index,
                ),
              ),
              BuildFilterBar(detailsData: categoryDetailsData),
              Flexible(
                child: BuildCategoryView(
                  detailsData: categoryDetailsData,
                  catId:
                      state.data.isNotEmpty ? state.data.last.selectedId : id,
                ),
              ),
            ],
          );
        } else {
          return LoadingDialog.showLoadingView();
        }
      },
    );
  }
}
