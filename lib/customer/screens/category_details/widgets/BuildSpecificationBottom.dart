part of 'CategoryDetailsWidgetsImports.dart';

class BuildSpecificationBottom extends StatelessWidget {
  final SpecificationModel model;
  final Function(int id) onItemClick;
  final Function() onSearch;
  final GenericBloc<int> countCubit;

  const BuildSpecificationBottom({
    Key? key,
    required this.model,
    required this.onItemClick,
    required this.onSearch,
    required this.countCubit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.white,
      height: 250,
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title: model.name,
            color: MyColors.blackOpacity,
            size: 13,
          ),
          Flexible(
            child: GridView.builder(
              itemCount: model.subSpecifications.length,
              padding: const EdgeInsets.symmetric(vertical: 10),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 10,
                crossAxisSpacing: 15,
                childAspectRatio: 3,
              ),
              itemBuilder: (_, index) {
                return BuildSpecItem(
                  model: model.subSpecifications[index],
                  selected: model.selected,
                  onClick: onItemClick,
                );
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BlocBuilder<GenericBloc<int>, GenericState<int>>(
                  bloc: countCubit,
                  builder: (context, state) {
                    if (state is GenericUpdateState) {
                      return Column(
                        children: [
                          MyText(
                            title: "${state.data}",
                            color: MyColors.black,
                            size: 11,
                          ),
                          MyText(
                            title: tr(context, "product"),
                            color: MyColors.blackOpacity,
                            size: 11,
                          ),
                        ],
                      );
                    }
                    return LoadingDialog.showSmallLoadingView();
                  },
                ),
                Container(
                  width: 100,
                  height: 50,
                  child: DefaultButton(
                    title: tr(context, "confirm"),
                    onTap: onSearch,
                    borderRadius: BorderRadius.circular(3),
                    margin: const EdgeInsets.symmetric(vertical: 10),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
