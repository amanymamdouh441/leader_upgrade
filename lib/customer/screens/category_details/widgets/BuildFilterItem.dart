part of 'CategoryDetailsWidgetsImports.dart';

class BuildFilterItem extends StatelessWidget {
  final String title;
  final Function()? onTap;
  final bool? filter;
  const BuildFilterItem({required this.title, this.onTap, this.filter});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: [
          MyText(
            title: title,
            size: 10,
            color: MyColors.black,
            fontWeight: FontWeight.w600,
          ),
          Visibility(
            visible: filter?? false,
            child: Icon(Icons.filter_alt_outlined),
            replacement: Icon(Icons.keyboard_arrow_down_outlined),
          )
        ],
      ),
    );
  }
}
