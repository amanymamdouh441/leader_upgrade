part of 'CategoryDetailsWidgetsImports.dart';

class BuildSubCategoryItem extends StatelessWidget {
  final SubCategory subCategory;
  final CategoryDetailsData categoryDetailsData;
  final int selected;
  final int index;

  const BuildSubCategoryItem(
      {required this.subCategory,
      required this.categoryDetailsData,
      required this.selected,
      required this.index});

  @override
  Widget build(BuildContext context) {
    Color color = subCategory.id == selected || subCategory.parentId == selected
        ? MyColors.primary
        : MyColors.greyWhite;
    Color textColor =
        subCategory.id == selected || subCategory.parentId == selected
            ? MyColors.white
            : MyColors.primary;
    return InkWell(
      onTap: () {
        if (selected != subCategory.id) {
          categoryDetailsData.fetchSubCategories(
            context,
            subCategory.id == 0 ? subCategory.parentId ?? 0 : subCategory.id,
            subCategory.id == 0 ? index : index + 1,
          );
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        margin: EdgeInsets.symmetric(horizontal: 5),
        decoration:
            BoxDecoration(color: color, borderRadius: BorderRadius.circular(5)),
        alignment: Alignment.center,
        child: MyText(
          title: "${subCategory.name}",
          size: 10,
          color: textColor,
        ),
      ),
    );
  }
}
