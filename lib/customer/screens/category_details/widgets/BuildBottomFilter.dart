part of 'CategoryDetailsWidgetsImports.dart';

class BuildBottomFilter extends StatelessWidget {
  final CategoryDetailsData categoryDetailsData;

  const BuildBottomFilter({required this.categoryDetailsData});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.white,
      height: 300,
      padding: const EdgeInsets.only(top: 10, right: 15, left: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title: tr(context, "arrange"),
            color: MyColors.blackOpacity,
            size: 13,
          ),
          Flexible(
            child: GenericListView(
              type: ListViewType.normal,
              padding: const EdgeInsets.only(top: 5),
              children: [
                InkWell(
                  onTap: () => categoryDetailsData.setOrderType(3, context),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7),
                        child: MyText(
                          title: tr(context, "newArrive"),
                          color: MyColors.black,
                          size: 12,
                        ),
                      ),
                      Divider(thickness: 1),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () => categoryDetailsData.setOrderType(2, context),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7),
                        child: MyText(
                          title: tr(context, "lessPrice"),
                          color: MyColors.black,
                          size: 12,
                        ),
                      ),
                      Divider(thickness: 1),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () => categoryDetailsData.setOrderType(1, context),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7),
                        child: MyText(
                          title: tr(context, "highPrice"),
                          color: MyColors.black,
                          size: 12,
                        ),
                      ),
                      Divider(thickness: 1),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
