part of 'CategoryDetailsWidgetsImports.dart';

class BuildCategoryView extends StatelessWidget {
  final CategoryDetailsData detailsData;
  final int catId;

  const BuildCategoryView({required this.detailsData, required this.catId});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => detailsData.fetchCatProducts(context, 1),
      child: PagedGridView<int, ProductModel>(
        pagingController: detailsData.pagingController,
        padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
        gridDelegate: GridFixedHeightDelegate(
          crossAxisCount: 3,
          mainAxisSpacing: 10,
          crossAxisSpacing: 5,
          height: 180,
        ),
        builderDelegate: PagedChildBuilderDelegate<ProductModel>(
          noItemsFoundIndicatorBuilder: (context) => const BuildNoItemFound(
            title: "noProducts",
            message: "tryAgain",
          ),
          firstPageProgressIndicatorBuilder: (_) {
            return LoadingDialog.showLoadingView();
          },
          itemBuilder: (context, item, index) => BuildProductItem(
            model: item,
            onClosed: detailsData.onFavChanged,
          ),
        ),
      ),
    );
  }
}
