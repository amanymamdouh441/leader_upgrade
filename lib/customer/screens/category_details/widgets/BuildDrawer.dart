part of 'CategoryDetailsWidgetsImports.dart';

class BuildDrawer extends StatelessWidget {
  final CategoryDetailsData categoryDetailsData;

  const BuildDrawer({required this.categoryDetailsData});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 40),
      width: MediaQuery.of(context).size.width * 0.75,
      color: MyColors.white,
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: MyText(
              title: tr(context, "filter"),
              size: 14,
              color: MyColors.black,
            ),
          ),
          Divider(thickness: 1),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.done,
            controller: categoryDetailsData.search,
            validate: (value) => value!.noValidate(),
            prefixIcon: Icon(Icons.search, size: 20),
            hint: tr(context,"searchText"),
            margin: EdgeInsets.all(10),
            enableBorderColor: MyColors.grey,
            onChange: (value) => categoryDetailsData
                .fetchCatProductsCount(context, refresh: false),
            contentPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
          ),
          Flexible(
            child: ListView(
              children: [
                BlocBuilder<GenericBloc<List<SpecificationModel>>,
                    GenericState<List<SpecificationModel>>>(
                  bloc: categoryDetailsData.specCubit,
                  builder: (context, state) {
                    return Column(
                      children: List.generate(state.data.length, (index) {
                        return BuildSizeFilter(
                          categoryDetailsData: categoryDetailsData,
                          model: state.data[index],
                          position: index,
                        );
                      }),
                    );
                  },
                ),
                BuildPriceFilter(categoryDetailsData: categoryDetailsData),
              ],
            ),
          ),
          Divider(thickness: 1),
          Container(
            margin: const EdgeInsets.only(bottom: 5, right: 10, left: 10),
            child: Row(
              children: [
                BlocBuilder<GenericBloc<int>, GenericState<int>>(
                  bloc: categoryDetailsData.countCubit,
                  builder: (context, state) {
                    if (state is GenericUpdateState) {
                      return Column(
                        children: [
                          MyText(
                            title: "${state.data}",
                            color: MyColors.black,
                            size: 11,
                          ),
                          MyText(
                            title: tr(context, "product"),
                            color: MyColors.blackOpacity,
                            size: 11,
                          ),
                        ],
                      );
                    }
                    return LoadingDialog.showSmallLoadingView();
                  },
                ),
                Expanded(
                  child: DefaultButton(
                    title: tr(context, "confirm"),
                    onTap: () {
                      Navigator.of(context).pop();
                      categoryDetailsData.pagingController.refresh();
                    },
                    color: MyColors.black,
                    textColor: MyColors.white,
                    borderRadius: BorderRadius.circular(3),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
