part of 'CategoryDetailsWidgetsImports.dart';

class BuildSizeFilter extends StatelessWidget {
  final CategoryDetailsData categoryDetailsData;
  final SpecificationModel model;
  final int position;

  const BuildSizeFilter({
    required this.categoryDetailsData,
    required this.model,
    required this.position,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () => categoryDetailsData.onItemOpen(model, position),
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  title: model.name,
                  color: MyColors.black,
                  size: 12,
                ),
                Visibility(
                  visible: model.opened,
                  child: Icon(Icons.keyboard_arrow_up),
                  replacement: Icon(Icons.keyboard_arrow_down),
                )
              ],
            ),
          ),
        ),
        Visibility(
          visible: model.opened,
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 10),
            child: GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: model.subSpecifications.length,
              padding: const EdgeInsets.symmetric(vertical: 10),
              gridDelegate: GridFixedHeightDelegate(
                crossAxisCount: 3,
                mainAxisSpacing: 10,
                crossAxisSpacing: 15,
                height: 30,
              ),
              itemBuilder: (_, index) {
                return BuildSpecItem(
                  model: model.subSpecifications[index],
                  selected: model.selected,
                  onClick: (int id) =>
                      categoryDetailsData.onItemSelected(context,model, id, position),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
