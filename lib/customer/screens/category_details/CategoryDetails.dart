part of 'CategoryDetailsImports.dart';

class CategoryDetails extends StatefulWidget {
  final SubCategory model;

  const CategoryDetails({Key? key, required this.model}) : super(key: key);

  @override
  _CategoryDetailsState createState() => _CategoryDetailsState();
}

class _CategoryDetailsState extends State<CategoryDetails> {
  final CategoryDetailsData detailsData = CategoryDetailsData();

  @override
  void initState() {
    detailsData.fetchSubCategories(context, widget.model.id, 0);
    detailsData.initData(context, widget.model.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: detailsData.scaffold,
      appBar: DefaultAppBar(
        title: widget.model.name,
        actions: [
          CartIcon(),
        ],
      ),
      body: BuildView(
        categoryDetailsData: detailsData,
        id: widget.model.id,
      ),
      drawer: BuildDrawer(categoryDetailsData: detailsData),
    );
  }
}
