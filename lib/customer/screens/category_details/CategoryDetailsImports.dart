import 'dart:convert';

import 'package:leader_upgrade/customer/models/Dtos/FilterModel.dart';
import 'package:leader_upgrade/customer/models/Dtos/ParentSubCategory.dart';
import 'package:leader_upgrade/customer/models/Dtos/PriceRangeModel.dart';
import 'package:leader_upgrade/customer/models/product_model.dart';
import 'package:leader_upgrade/customer/models/specification_model.dart';
import 'package:leader_upgrade/customer/models/sub_category.dart';
import 'package:leader_upgrade/customer/screens/category_details/widgets/CategoryDetailsWidgetsImports.dart';
import 'package:leader_upgrade/customer/widgets/widgetsImports.dart';
import 'package:leader_upgrade/general/blocks/sub_cats_cubit/sub_cats_cubit.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'CategoryDetails.dart';
part 'CategoryDetailsData.dart';