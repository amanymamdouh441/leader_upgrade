part of 'CategoryDetailsImports.dart';

class CategoryDetailsData {
  final GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();
  final TextEditingController search = new TextEditingController();
  final GenericBloc<PriceRangeModel?> rangeCubit = GenericBloc(null);
  final GenericBloc<List<SpecificationModel>> specCubit = GenericBloc([]);
  final GenericBloc<SpecificationModel?> selectedSpecCubit = GenericBloc(null);
  final GenericBloc<int> countCubit = GenericBloc(0);
  final PagingController<int, ProductModel> pagingController =
      PagingController(firstPageKey: 1);
  final GenericBloc<List<ParentSubCategory>> parentSubCatsCubit =
      GenericBloc([]);
  late List<ProductModel> productModel;
  final GenericBloc<int> catTabCubit = GenericBloc(0);
  static const _pageSize = 10;
  int priceType = 3;
  int currentCat = 0;

  Future<void> fetchSpecifications(BuildContext context, int catId,
      {bool refresh = true}) async {
    print("*******************${currentCat}");

    var data =
        await CustomerRepository(context).getCatSpecifications(catId, refresh);
    specCubit.onUpdateData(data?.specifications ?? []);
    RangeValues rangeValues = RangeValues(
        data?.minPrice.toDouble() ?? 0, data?.maxPrice.toDouble() ?? 10000);
    rangeCubit.onUpdateData(
        PriceRangeModel(initial: rangeValues, value: rangeValues));
  }

  Future<void> fetchCatProducts(BuildContext context, int pageKey,
      {bool refresh = true}) async {
    var ids = specCubit.state.data
        .where((e) => e.selected.isNotEmpty)
        .map((e) => e.selected)
        .toList();
    FilterModel model = FilterModel(
      refresh: refresh,
      categoryId: currentCat,
      currentPage: pageKey,
      subIds: ids.expand((pair) => pair).toList(),
      searchTxt: search.text,
      typePrice: priceType,
      priceForm: rangeCubit.state.data?.value.start,
      priceTo: rangeCubit.state.data?.value.end,
    );
    print("###############${model.currentPage}");

    productModel = await CustomerRepository(context).getCatProducts(model);
    final isLastPage = productModel.length < _pageSize;
    if (pageKey == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(productModel);
    } else {
      final nextPageKey = pageKey + 1;
      pagingController.appendPage(productModel, nextPageKey);
    }
  }

  Future<void> fetchCatProductsCount(BuildContext context,
      {bool refresh = true}) async {
    var ids = specCubit.state.data
        .where((e) => e.selected.isNotEmpty)
        .map((e) => e.selected)
        .toList();
    FilterModel model = FilterModel(
      refresh: refresh,
      categoryId: currentCat,
      subIds: ids.expand((pair) => pair).toList(),
      searchTxt: search.text,
      typePrice: priceType,
      priceForm: rangeCubit.state.data?.value.start,
      priceTo: rangeCubit.state.data?.value.end,
    );
    print("@@@@@@@@@@@@@@@@@@@@@@${currentCat}");
    countCubit.emit(GenericInitialState(0));
    var data = await CustomerRepository(context).getCatProductsCount(model);
    countCubit.onUpdateData(data);
  }

  filterBottomSheet(BuildContext context, CategoryDetailsData detailsData) {
    showModalBottomSheet(
      context: context,
      builder: (_) {
        return BuildBottomFilter(categoryDetailsData: detailsData);
      },
    );
  }

  specificBottomSheet(
      BuildContext context, SpecificationModel model, int index) {
    selectedSpecCubit.onUpdateData(model);
    showModalBottomSheet(
      context: context,
      builder: (_) {
        return BlocBuilder<GenericBloc<SpecificationModel?>,
            GenericState<SpecificationModel?>>(
          bloc: selectedSpecCubit,
          builder: (context, state) {
            return BuildSpecificationBottom(
              model: state.data!,
              countCubit: countCubit,
              onItemClick: (id) =>
                  onItemSelected(context, state.data!, id, index),
              onSearch: () {
                Navigator.of(context).pop();
                pagingController.refresh();
              },
            );
          },
        );
      },
    );
  }

  onItemSelected(
      BuildContext context, SpecificationModel model, int id, int index) {
    if (model.selected.contains(id)) {
      model.selected.remove(id);
    } else {
      model.selected.add(id);
    }
    specCubit.state.data[index] = model;
    specCubit.onUpdateData(specCubit.state.data);
    selectedSpecCubit.onUpdateData(model);
    fetchCatProductsCount(context, refresh: false);
    fetchCatProductsCount(context);
  }

  onItemOpen(SpecificationModel model, int index) {
    model.opened = !model.opened;
    specCubit.state.data[index] = model;
    specCubit.onUpdateData(specCubit.state.data);
  }

  setOrderType(int type, BuildContext context) {
    Navigator.of(context).pop();
    priceType = type;
    pagingController.refresh();
  }

  changePriceValue(RangeValues values, BuildContext context) {
    rangeCubit.state.data!.value = values;
    rangeCubit.onUpdateData(rangeCubit.state.data!);
    fetchCatProductsCount(context, refresh: false);
    fetchCatProductsCount(context);
  }

  onFavChanged(ProductModel? model) {
    if (model != null) {
      int index =
          pagingController.itemList!.indexWhere((e) => e.id == model.id);
      pagingController.itemList![index] = model;
      var data = pagingController.itemList;
      pagingController.itemList = [];
      pagingController.itemList = data;
    }
  }

  void updateCount(BuildContext context) async {
    await fetchCatProductsCount(context, refresh: false);
    Navigator.of(context).pop();
    pagingController.refresh();
  }

  void initData(BuildContext context, int catId) {
    currentCat = catId;
    fetchSpecifications(context, catId, refresh: true);
    fetchSpecifications(context, catId).then((value) {
      fetchCatProducts(context, 1);
      pagingController.addPageRequestListener((pageKey) {
        fetchCatProducts(context, pageKey, refresh: true);
      });
    });
    fetchCatProductsCount(context, refresh: true);
  }

  Future<void> fetchSubCategories(BuildContext context, int id, int index,
      {bool refresh = true}) async {
    final cubit = parentSubCatsCubit.state.data;

    var data = await CustomerRepository(context).getSubCategories(id, refresh);
    cubit.removeRange(index, cubit.length);

    if (data.isNotEmpty) {
      data.insert(0,
          SubCategory(id: 0, img: "", name: tr(context, "all"), parentId: id));
      cubit
          .add(ParentSubCategory(subCategory: data, selectedId: data.first.id));
      if (index == 0) {
        cubit[index].selectedId = 0;
      } else {
        cubit[index - 1].selectedId = id;
      }
    } else if (cubit.isNotEmpty) {
      cubit[index - 1].selectedId = id;
    }

    parentSubCatsCubit.onUpdateData(cubit);
    currentCat = id;
    fetchSpecifications(context, currentCat, refresh: true).then((value) {
      fetchCatProductsCount(context, refresh: true);
      pagingController.refresh();
    });
  }
}
