import 'dart:typed_data';

import 'package:leader_upgrade/customer/models/user_addresses_model.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/customer/screens/add_update_address/widgets/AddAddressesWidgetsImports.dart';
import 'package:leader_upgrade/customer/screens/user_addresses/UserAddressesImports.dart';
import 'package:leader_upgrade/general/models/LocationModel.dart';
import 'package:leader_upgrade/general/screens/location_address/LocationAddressImports.dart';
import 'package:leader_upgrade/general/screens/location_address/location_cubit/location_cubit.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:flutter/cupertino.dart';

import 'package:geolocator/geolocator.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/models/LocationModel.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/tf_validator.dart';

part 'AddUpdateAddresses.dart';

part 'AddUpdateAddressesData.dart';
