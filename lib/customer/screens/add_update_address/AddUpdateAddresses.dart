part of 'AddAddressesImports.dart';

class AddUpdateAddress extends StatefulWidget {
  final String? bottomButtonName, appBarTitle;
  final double? lat, lng;
  final bool addAddress;
  final int? id;

  const AddUpdateAddress(
      {Key? key,
      this.bottomButtonName,
      this.appBarTitle,
      this.id,
      this.lng,
      this.lat,
      required this.addAddress})
      : super(key: key);

  @override
  _AddUpdateAddressState createState() => _AddUpdateAddressState();
}

class _AddUpdateAddressState extends State<AddUpdateAddress> {
  final AddUpdateAddressData addUpdateAddressData = AddUpdateAddressData();

  // final UserAddressesData userAddressesData = UserAddressesData();

  @override
  void initState() {
    print(
        context.read<LocationCubit>().state.model!.lng.toString() + '!!!!!!!!');
    if (widget.addAddress == true) {
      var loc = context.read<LocationCubit>().state.model;
      double lat = loc?.lat ?? 24.774265;
      double lng = loc?.lng ?? 46.738586;
      addUpdateAddressData.locationModel = LocationModel(lat: lat, lng: lng);
      addUpdateAddressData.getLocationAddress(context);
    } else {
      addUpdateAddressData.locationModel =
          LocationModel(lat: widget.lat!, lng: widget.lng!);
      addUpdateAddressData.getLocationAddress(context);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: AppBar(
        title: MyText(
          title: tr(context, widget.appBarTitle ?? ""),
          size: 14,
          color: MyColors.black,
          fontWeight: FontWeight.bold,
        ),
        centerTitle: false,
        backgroundColor: MyColors.white,
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            size: 23,
            color: MyColors.black,
          ),
          onPressed: () => Navigator.of(context).pop(false),
        ),
      ),
      body: BuildGoogleMapView(
        addUpdateAddressData: addUpdateAddressData,
      ),
      floatingActionButton: BuildAddAddressesItem(
        addUpdateAddressData: addUpdateAddressData,
        bottomButtonName: widget.bottomButtonName!,
        onTap: () => addUpdateAddressData.changeLocation(context),

      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
