part of 'AddAddressesWidgetsImports.dart';

class BuildAddAddressesItem extends StatelessWidget {
  final String bottomButtonName;
   final AddUpdateAddressData addUpdateAddressData;
  final Function() onTap;

  const BuildAddAddressesItem(
      {Key? key,
      required this.bottomButtonName,
       required this.addUpdateAddressData,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var locationAddress=context.watch<LocationCubit>().state.model!.address;
    return Container(
      height: MediaQuery.of(context).size.height / 5,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
        color: MyColors.white,
        boxShadow: [
          BoxShadow(
            color: MyColors.greyWhite,
            spreadRadius: 2,
            blurRadius: 2,
          ),
        ],
      ),
      child: Column(
        children: [
          Expanded(
            flex: 4,
            child: Container(
              margin: const EdgeInsets.only(bottom: 5),
              child: MyText(
                title: locationAddress,
                color: MyColors.blackOpacity,
                overflow: TextOverflow.clip,
                size: 11.5,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: DefaultButton(
              title: tr(context, bottomButtonName),
              onTap: onTap,
              borderRadius: BorderRadius.circular(5),
              margin: EdgeInsets.symmetric(vertical: 10),
            ),
          )
        ],
      ),
    );
  }
}
