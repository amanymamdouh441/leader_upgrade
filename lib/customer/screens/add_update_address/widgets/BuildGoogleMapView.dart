part of 'AddAddressesWidgetsImports.dart';

class BuildGoogleMapView extends StatelessWidget {
  final AddUpdateAddressData addUpdateAddressData;

  const BuildGoogleMapView({required this.addUpdateAddressData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationCubit, LocationState>(
      builder: (context, state) {
        CameraPosition _initialLoc = CameraPosition(
          target: LatLng(state.model!.lat, state.model!.lng),
          zoom: 16.4746,
        );
        return Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              child: GoogleMap(
                  mapType: MapType.normal,
                   initialCameraPosition: _initialLoc,
                  onMapCreated: (GoogleMapController controller) {
                    addUpdateAddressData.controller.complete(controller);
                  },
                  myLocationButtonEnabled: true,
                  myLocationEnabled: true,
                  rotateGesturesEnabled: true,
                  scrollGesturesEnabled: true,
                  trafficEnabled: true,
                  zoomControlsEnabled: true,
                  tiltGesturesEnabled: true,
                  compassEnabled: true,
                  indoorViewEnabled: true,
                  buildingsEnabled: true,
                  mapToolbarEnabled: true,
                  zoomGesturesEnabled: true,
                  onCameraIdle: () {
                    addUpdateAddressData.getLocationAddress(context);
                  },
                  onTap: (location) {
                    addUpdateAddressData.getLocationAddress(context);
                  },
                  onCameraMove: (loc) {
                    addUpdateAddressData.locationModel = LocationModel(
                        lat: loc.target.latitude,
                        lng: loc.target.longitude,
                        address: "",
                    );
                  }
              ),
            ),
            ImageIcon(
              AssetImage(Res.marker),
              size: 50,
              color: Colors.red,
            ),
          ],
        );
      },
    );
  }
}
