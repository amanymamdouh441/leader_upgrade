import 'package:leader_upgrade/customer/screens/add_update_address/AddAddressesImports.dart';
import 'package:leader_upgrade/customer/screens/user_addresses/UserAddressesImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/screens/location_address/location_cubit/location_cubit.dart';

import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/widgets/DefaultButton.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/models/LocationModel.dart';
import 'package:leader_upgrade/general/screens/location_address/location_cubit/location_cubit.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'BuildAddAddressesItem.dart';
part 'BuildGoogleMapView.dart';