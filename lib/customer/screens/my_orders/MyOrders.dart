part of 'MyOrdersImports.dart';

class MyOrders extends StatefulWidget {
  final int index;

  const MyOrders({required this.index});

  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> with TickerProviderStateMixin {
  MyOrdersData myOrdersData = MyOrdersData();

  @override
  void initState() {
    myOrdersData.tabController = TabController(length: 6, vsync: this);
    myOrdersData.tabController.animateTo(widget.index);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        backgroundColor: MyColors.secondary,
        appBar: DefaultAppBar(
          title: tr(context, "orders"),
          actions: [CartIcon()],
          leading: Container(),
        ),
        body: Visibility(
          visible: context.read<AuthCubit>().state.authorized,
          child: Column(
            children: [
              BuildOrderTabs(myOrdersData: myOrdersData),
              BuildOrdersTabView(myOrdersData: myOrdersData)
            ],
          ),
          replacement: Center(
            child: MyText(
              title: tr(context, "LoginFirst"),
              color: MyColors.primary,
              size: 15,
            ),
          ),
        ),
      ),
    );
  }
}
