part of 'ReturnedOrdersImports.dart';

class ReturnedOrdersData {
  final GenericBloc<List<OrderModel>> orderBloc = new GenericBloc([]);

  void fetchData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getReturnOrders(refresh);
    orderBloc.onUpdateData(data);
  }
}