part of 'ReturnedOrdersImports.dart';

class ReturnedOrders extends StatefulWidget {
  const ReturnedOrders({Key? key}) : super(key: key);

  @override
  _ReturnedOrdersState createState() => _ReturnedOrdersState();
}

class _ReturnedOrdersState extends State<ReturnedOrders> {
  final ReturnedOrdersData returnedOrdersData = ReturnedOrdersData();

  @override
  Widget build(BuildContext context) {
    return GenericListView<OrderModel>(
      padding: EdgeInsets.symmetric(vertical: 10),
      type: ListViewType.api,
      onRefresh: returnedOrdersData.fetchData,
      cubit: returnedOrdersData.orderBloc,
      params: [context],
      emptyStr: tr(context, "noData"),
      itemBuilder: (_, index, item) {
        return BuildMyOrderItem(model: item);
      },
    );
  }
}
