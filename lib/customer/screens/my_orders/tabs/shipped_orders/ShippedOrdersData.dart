part of 'ShippedOrdersImports.dart';

class ShippedOrdersData {
  final GenericBloc<List<OrderModel>> orderBloc = new GenericBloc([]);

  void fetchData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getShippingOrders(refresh);
    orderBloc.onUpdateData(data);
  }
}