part of 'ShippedOrdersImports.dart';

class ShippedOrders extends StatefulWidget {
  const ShippedOrders({Key? key}) : super(key: key);

  @override
  _ShippedOrdersState createState() => _ShippedOrdersState();
}

class _ShippedOrdersState extends State<ShippedOrders> {
  final ShippedOrdersData shippedOrdersData = ShippedOrdersData();

  @override
  Widget build(BuildContext context) {
    return GenericListView<OrderModel>(
      padding: EdgeInsets.symmetric(vertical: 10),
      type: ListViewType.api,
      onRefresh: shippedOrdersData.fetchData,
      cubit: shippedOrdersData.orderBloc,
      params: [context],
      emptyStr: tr(context, "noData"),
      itemBuilder: (_, index, item) {
        return BuildMyOrderItem(model: item);
      },
    );
  }
}
