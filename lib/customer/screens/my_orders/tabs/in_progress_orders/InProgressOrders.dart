part of 'InProgressOrdersImports.dart';

class InProgressOrders extends StatefulWidget {
  const InProgressOrders({Key? key}) : super(key: key);

  @override
  _InProgressOrdersState createState() => _InProgressOrdersState();
}

class _InProgressOrdersState extends State<InProgressOrders> {
  final InProgressOrdersData inProgressOrdersData = InProgressOrdersData();

  @override
  Widget build(BuildContext context) {
    return


    //   Column(children: [
    //   InkWell(
    //     onTap: () => returnData.setProductImage(),
    //     child: Container(
    //       height: 100,
    //       width: MediaQuery.of(context).size.width * 0.3,
    //       margin: const EdgeInsets.symmetric(vertical: 5),
    //       decoration: BoxDecoration(
    //         color: MyColors.white,
    //         borderRadius: BorderRadius.circular(10),
    //       ),
    //       alignment: Alignment.center,
    //       child: Image.asset(Res.addImage, height: 50),
    //     ),
    //   ),
    //   BlocBuilder<GenericBloc<List<File>>, GenericState<List<File>>>(
    //     bloc: returnData.productCubit,
    //     builder: (BuildContext context, state) {
    //       return Container(
    //         width: MediaQuery.of(context).size.width,
    //         padding: const EdgeInsets.symmetric(
    //             horizontal: 15,
    //             vertical: 10
    //         ),
    //         child: Wrap(
    //           spacing: 10,
    //           runSpacing: 10,
    //           alignment: WrapAlignment.start,
    //           runAlignment: WrapAlignment.start,
    //           crossAxisAlignment: WrapCrossAlignment.start,
    //           children: List.generate(
    //             state.data.length,
    //                 (index) {
    //               return Container(
    //                 width: 90,
    //                 height: 90,
    //                 decoration: BoxDecoration(
    //                   borderRadius: BorderRadius.circular(5),
    //                   image: DecorationImage(
    //                     fit: BoxFit.fill,
    //                     image: FileImage(
    //                       state.data[index],
    //                     ),
    //                   ),
    //                 ),
    //                 alignment: Alignment.topRight,
    //                 child: InkWell(
    //                   onTap: () =>returnData.deleteImages(index),
    //                   child: Container(
    //                     width: 25,
    //                     height: 25,
    //                     decoration: BoxDecoration(
    //                       color: Colors.red,
    //                       borderRadius: BorderRadius.circular(5),
    //                     ),
    //                     child: Icon(
    //                       Icons.close,
    //                       size: 15,
    //                       color: Colors.white,
    //                     ),
    //                   ),
    //                 ),
    //               );
    //             },
    //           ),
    //         ),
    //       );
    //     },
    //   ),
    // ],);


    //   Container(
    //   height: 100,
    //   width: 60,
    //   child: BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
    //     bloc: returnData.billCubit,
    //     builder: (context, state) {
    //       if (state.data != null) {
    //         return Container(
    //            alignment: Alignment.topRight,
    //            margin: const EdgeInsets.symmetric(vertical: 100),
    //           decoration: BoxDecoration(
    //             color: MyColors.white,
    //             borderRadius: BorderRadius.circular(10),
    //             image: DecorationImage(
    //               image: FileImage(state.data!),
    //               fit: BoxFit.cover,
    //             ),
    //           ),
    //           child: InkWell(
    //             onTap: () => returnData.setBillImage(),
    //             child: Container(
    //               height: 35,
    //               width: 35,
    //               decoration: BoxDecoration(
    //                 color: MyColors.primary,
    //                 shape: BoxShape.circle,
    //               ),
    //               alignment: Alignment.center,
    //               child: Icon(Icons.edit, color: MyColors.white, size: 20),
    //             ),
    //           ),
    //         );
    //       }
    //       return InkWell(
    //         onTap: () => returnData.setBillImage(),
    //         child: Container(
    //           height: 100,
    //           width: MediaQuery.of(context).size.width * 0.4,
    //           margin: const EdgeInsets.symmetric(vertical: 100),
    //           decoration: BoxDecoration(
    //              borderRadius: BorderRadius.circular(10),
    //           ),
    //           alignment: Alignment.center,
    //           child: Image.asset(Res.addImage, height: 50),
    //         ),
    //       );
    //     },
    //   ),
    // );

      GenericListView<OrderModel>(
      padding: EdgeInsets.symmetric(vertical: 10),
      type: ListViewType.api,
      onRefresh: inProgressOrdersData.fetchData,
      cubit: inProgressOrdersData.orderBloc,
      params: [context],
      emptyStr: tr(context, "noData"),
      itemBuilder: (_, index, item) {
        return BuildMyOrderItem(model: item);
      },
    );
  }
}
