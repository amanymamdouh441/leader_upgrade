part of 'InProgressOrdersImports.dart';

class InProgressOrdersData {
  final GenericBloc<List<OrderModel>> orderBloc = new GenericBloc([]);

  void fetchData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getProgressOrders(refresh);
    orderBloc.onUpdateData(data);
  }
}