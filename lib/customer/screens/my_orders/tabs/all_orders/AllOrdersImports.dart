import 'package:leader_upgrade/customer/models/order_model.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/customer/screens/my_orders/widgets/MyOrdersWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'AllOrders.dart';
part 'AllOrdersData.dart';