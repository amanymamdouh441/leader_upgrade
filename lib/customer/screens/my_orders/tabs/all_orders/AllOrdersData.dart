part of 'AllOrdersImports.dart';

class AllOrdersData {
  final GenericBloc<List<OrderModel>> orderBloc = new GenericBloc([]);

  void fetchData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getAllOrders(refresh);
    orderBloc.onUpdateData(data);
  }
}
