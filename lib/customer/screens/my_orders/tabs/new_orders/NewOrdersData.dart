part of 'NewOrdersImports.dart';

class NewOrdersData{
  final GenericBloc<List<OrderModel>> orderBloc = new GenericBloc([]);

  void fetchData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getNewOrders(refresh);
    orderBloc.onUpdateData(data);
  }
}