part of 'NewOrdersImports.dart';

class NewOrders extends StatefulWidget {
  const NewOrders({Key? key}) : super(key: key);

  @override
  _NewOrdersState createState() => _NewOrdersState();
}

class _NewOrdersState extends State<NewOrders> {
final  NewOrdersData newOrdersData = NewOrdersData();
  @override
  Widget build(BuildContext context) {
    return GenericListView<OrderModel>(
      padding: EdgeInsets.symmetric(vertical: 10),
      type: ListViewType.api,
      onRefresh: newOrdersData.fetchData,
      cubit: newOrdersData.orderBloc,
      params: [context],
      emptyStr: tr(context, "noData"),
      itemBuilder: (_, index, item) {
        return BuildMyOrderItem(model: item);
      },
    );
  }
}
