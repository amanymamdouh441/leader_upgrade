import 'package:leader_upgrade/customer/models/order_model.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/customer/screens/my_orders/widgets/MyOrdersWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/GenericListView.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'NewOrders.dart';
part 'NewOrdersData.dart';