part of 'FinishedOrdersImports.dart';

class FinishedOrders extends StatefulWidget {
  const FinishedOrders({Key? key}) : super(key: key);

  @override
  _FinishedOrdersState createState() => _FinishedOrdersState();
}

class _FinishedOrdersState extends State<FinishedOrders> {
  final FinishedOrdersData finishedOrdersData = FinishedOrdersData();

  @override
  Widget build(BuildContext context) {
    return GenericListView<OrderModel>(
      padding: EdgeInsets.symmetric(vertical: 10),
      type: ListViewType.api,
      onRefresh: finishedOrdersData.fetchData,
      cubit: finishedOrdersData.orderBloc,
      params: [context],
      emptyStr: tr(context, "noData"),
      itemBuilder: (_, index, item) {
        return BuildMyOrderItem(model: item);
      },
    );
  }
}
