part of 'FinishedOrdersImports.dart';

class FinishedOrdersData {
  final GenericBloc<List<OrderModel>> orderBloc = new GenericBloc([]);

  void fetchData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getFinishedOrders(refresh);
    orderBloc.onUpdateData(data);
  }
}
