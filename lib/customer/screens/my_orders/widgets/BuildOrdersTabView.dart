part of 'MyOrdersWidgetsImports.dart';

class BuildOrdersTabView extends StatelessWidget {
  final MyOrdersData myOrdersData;
  const BuildOrdersTabView({required this.myOrdersData});

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: myOrdersData.tabController,
        children: [
          AllOrders(),
          NewOrders(),
          InProgressOrders(),
          ShippedOrders(),
          FinishedOrders(),
          ReturnedOrders(),
        ],
      ),
    );
  }
}
