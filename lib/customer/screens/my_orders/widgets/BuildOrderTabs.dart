part of 'MyOrdersWidgetsImports.dart';

class BuildOrderTabs extends StatelessWidget {
  final MyOrdersData myOrdersData;

  const BuildOrderTabs({required this.myOrdersData});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(color: MyColors.white, boxShadow: [
        BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2),
      ]),
      child: TabBar(
        unselectedLabelColor: MyColors.blackOpacity,
        labelColor: MyColors.black,
        indicatorColor: MyColors.black,
        indicatorSize: TabBarIndicatorSize.label,
        isScrollable: true,
        controller: myOrdersData.tabController,
        tabs: [
          Tab(text: tr(context, "allOrders")),
          Tab(text: tr(context, "new")),
          Tab(text: tr(context, "inProgress")),
          Tab(text: tr(context, "onDelivery")),
          Tab(text: tr(context, "finished")),
          Tab(text: tr(context, "returned")),
        ],
      ),
    );
  }
}
