part of 'MyOrdersWidgetsImports.dart';

class BuildMyOrderItem extends StatelessWidget {
  final OrderModel model;

  const BuildMyOrderItem({required this.model});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(color: MyColors.white, boxShadow: [
        BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2),
      ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: MyText(
                  title: " ${tr(context, "orderNum")} :${model.id}",
                  color: MyColors.black,
                  size: 11,
                ),
              ),
              Container(
                height: 12,
                width: 12,
                decoration: BoxDecoration(
                    color: Colors.green[400], shape: BoxShape.circle),
                margin: const EdgeInsets.symmetric(horizontal: 7),
              ),
              MyText(
                title: model.status,
                color: MyColors.blackOpacity,
                size: 11,
              )
            ],
          ),
          MyText(
            title:
                "${model.countProduct} ${tr(context, "products")} ،  ${tr(context, "total")} :  ${model.total.toStringAsFixed(2)} ${tr(context, "sr")} ",
            color: MyColors.blackOpacity,
            size: 11,
          ),
          Container(
            height: 120,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: model.imgs.length,
              itemBuilder: (_, index) {
                return Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                  child: CachedImage(
                    url: model.imgs[index],
                    width: 80,
                  ),
                );
              },
            ),
          ),
          OpenContainer(
            closedElevation: 0,
            openElevation: 0,
            closedColor: Colors.transparent,
            openColor: Colors.white,
            middleColor: Colors.transparent,
            transitionDuration: Duration(milliseconds: 800),
            transitionType: ContainerTransitionType.fadeThrough,
            openBuilder: (context, action) => OrderDetails(orderId: model.id, ),
            closedBuilder: (context, action) => Container(
              margin: const EdgeInsets.symmetric(vertical: 5),
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
              decoration: BoxDecoration(
                color: MyColors.white,
                border: Border.all(color: MyColors.blackOpacity),
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                      color: MyColors.greyWhite,
                      spreadRadius: 3,
                      blurRadius: 2,
                      offset: Offset(1, 1)),
                ],
              ),
              child: MyText(
                title: tr(context, "displayDetails"),
                color: MyColors.blackOpacity,
                size: 11,
              ),
            ),
          )
        ],
      ),
    );
  }
}
