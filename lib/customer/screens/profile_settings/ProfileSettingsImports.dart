import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/models/Dtos/UpdateUserModel.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/customer/screens/profile_settings/widgets/ProfileSettingsWidgetsImports.dart';
import 'package:leader_upgrade/general/blocks/user_cubit/user_cubit.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/resources/GeneralRepoImports.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:leader_upgrade/res.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';
import 'package:dio_helper/utils/GlobalState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'ProfileSettings.dart';
part 'ProfileSettingsData.dart';