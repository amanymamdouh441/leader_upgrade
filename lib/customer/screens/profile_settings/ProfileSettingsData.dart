part of 'ProfileSettingsImports.dart';

class ProfileSettingsData {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GenericBloc<File?> fileCubit = new GenericBloc(null);
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phone = TextEditingController();

  removeAccount(BuildContext context) async {
    await GeneralRepository(context).deleteAccount();
  }

  setInitialData(BuildContext context) {
    var user = context.read<UserCubit>().state.model;
    print("______${user?.lang}");
    name.text = user?.userName ?? "";
    email.text = user?.email ?? "";
    phone.text = user?.phone ?? "";
  }

  pickProfileImage(   ) async {
    var image = await Utils.getImage();
    fileCubit.onUpdateData(image);
  }

  updateUserData(BuildContext context) async {
    if (formKey.currentState!.validate()) {
      String phoneEn = Utils.convertDigitsToLatin(phone.text);
      if (phoneEn.length != 10) {
        CustomToast.showSimpleToast(msg: tr(context, "phoneValidation"));
        return;
      }
      UpdateUserModel model = UpdateUserModel(
          email: email.text,
          userName: name.text,
          phone: phoneEn,
          imgProfile: fileCubit.state.data);
      GeneralRepository(context).updateProfile(model);
    }
  }
}
