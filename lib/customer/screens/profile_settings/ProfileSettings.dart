part of 'ProfileSettingsImports.dart';

class ProfileSettings extends StatefulWidget {
  const ProfileSettings({Key? key}) : super(key: key);

  @override
  _ProfileSettingsState createState() => _ProfileSettingsState();
}

class _ProfileSettingsState extends State<ProfileSettings> {

 final ProfileSettingsData settingsData = ProfileSettingsData();

 @override
  void initState() {
    settingsData.setInitialData(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: tr(context,"setting"),
        actions: [
          InkWell(
            onTap: ()=> GeneralRepository(context).logout(),
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 10),
              child: Image.asset(
                Res.logout,
                width: 25,
              ),
            ),
          ),
        ],
      ),
      body: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.only(top: 20, right: 15, left: 15),
        children: [
          BuildProfileSetPhoto(settingsData: settingsData),
          BuildProfileForm(profileSettingsData: settingsData)
        ],
      ),
      bottomNavigationBar: BuildProfileButtons(settingsData: settingsData),
    );
  }
}
