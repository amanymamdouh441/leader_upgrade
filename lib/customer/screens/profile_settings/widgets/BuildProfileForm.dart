part of 'ProfileSettingsWidgetsImports.dart';

class BuildProfileForm extends StatelessWidget {
  final ProfileSettingsData profileSettingsData;

  const BuildProfileForm({required this.profileSettingsData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: profileSettingsData.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(title: tr(context, "name"), color: MyColors.blackOpacity, size: 11),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value)=> value!.validateEmpty(context),
            hint: tr(context, "name"),
            suffixIcon: Icon(Icons.edit),
            enableBorderColor: MyColors.grey,
            controller: profileSettingsData.name,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),
          MyText(title: tr(context, "mail"), color: MyColors.blackOpacity, size: 11),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.emailAddress,
            action: TextInputAction.next,
            validate: (value)=> value!.validateEmail(context),
            hint:tr(context, "mail"),
            suffixIcon: Icon(Icons.edit),
            enableBorderColor: MyColors.grey,
            controller: profileSettingsData.email,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),
          MyText(title: tr(context, "phone"), color: MyColors.blackOpacity, size: 11),
          GenericTextField(
            suffixIcon: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MyText(title: "966+", color: MyColors.black, size: 14),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 7),
                  padding: const EdgeInsets.only(top: 3),
                  child: Image.asset(Res.arabic, scale: 3),
                ),
              ],
            ),
            fieldTypes: FieldTypes.normal,
            type: TextInputType.phone,
            action: TextInputAction.done,
            validate: (value)=> value!.validateEmpty(context),
            hint: tr(context, "phone"),
            enableBorderColor: MyColors.grey,
            controller: profileSettingsData.phone,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),
        ],
      ),
    );
  }
}
