part of 'ProfileSettingsWidgetsImports.dart';

class BuildProfileButtons extends StatelessWidget {
  final ProfileSettingsData settingsData;
  const BuildProfileButtons({Key? key, required this.settingsData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      child: Column(
        children: [
          DefaultButton(
            title: tr(context, "changePassword"),
            onTap: ()=> AutoRouter.of(context).push(ChangePasswordRoute()),
            color: MyColors.white,
            borderColor: MyColors.primary,
            textColor: MyColors.primary,
            margin: const EdgeInsets.symmetric(horizontal: 20),
          ),
          DefaultButton(
            title: tr(context, "saveChanges"),
            onTap: () => settingsData.updateUserData(context),
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          ),
          DefaultButton(
            title: tr(context, "deleteAccount"),
            onTap: () => settingsData.removeAccount(context),
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          ),
        ],
      ),
    );
  }
}
