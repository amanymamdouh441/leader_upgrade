import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/general/blocks/user_cubit/user_cubit.dart';
import 'package:leader_upgrade/general/resources/GeneralRepoImports.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:leader_upgrade/customer/screens/profile_settings/ProfileSettingsImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildProfileButtons.dart';
part 'BuildProfileSetPhoto.dart';
part 'BuildProfileForm.dart';