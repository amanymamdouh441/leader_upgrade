part of 'ProfileSettingsWidgetsImports.dart';

class BuildProfileSetPhoto extends StatelessWidget {
  final ProfileSettingsData settingsData;
  const BuildProfileSetPhoto({Key? key, required this.settingsData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var user = context.watch<UserCubit>().state.model;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
              bloc: settingsData.fileCubit,
              builder: (context, state) {
                if (state.data != null) {
                  return Container(
                    height: 150,
                    width: 150,
                    margin: const EdgeInsets.only(bottom: 20),
                    decoration: BoxDecoration(
                      color: MyColors.grey.withOpacity(0.7),
                      shape: BoxShape.circle,
                      border: Border.all(color: MyColors.grey),
                      image: DecorationImage(
                        image: FileImage(state.data!),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                }
                return Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: CachedImage(
                    url: user?.imgProfile ?? "",
                    height: 150,
                    width: 150,
                    borderColor: MyColors.grey,
                    haveRadius: false,
                    boxShape: BoxShape.circle,
                    fit: BoxFit.cover,
                  ),
                );
              },
            ),
            Positioned(
              right: 10,
              child: InkWell(
                onTap: ()=> settingsData.pickProfileImage(),
                child: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                    color: MyColors.black,
                    shape: BoxShape.circle,
                  ),
                  alignment: Alignment.center,
                  child: Icon(Icons.edit, color: MyColors.white, size: 20),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
