part of 'UserAddressesImports.dart';

class UserAddresses extends StatefulWidget {
  @override
  _UserAddressesState createState() => _UserAddressesState();
}

class _UserAddressesState extends State<UserAddresses> {
  final UserAddressesData userAddressesData = UserAddressesData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(
        title: tr(context, "myAddresses"),
      ),
      bottomNavigationBar: DefaultButton(
        title: tr(context, "addNewAddress"),
        onTap: () {
          userAddressesData.onAddLocationClick(context);
        },
        borderRadius: BorderRadius.circular(5),
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      ),
      body: GenericListView<UserAddressesModel>(
        type: ListViewType.api,
        cubit: userAddressesData.userAddressesCubit,
        onRefresh: userAddressesData.fetchData,
        params: [context],
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        emptyStr: tr(context, "noData"),
        itemBuilder: (_, index, item) {
          return BuildMyAddressesItem(
            userAddressesModel: item,
            userAddressesData:userAddressesData ,
            index: index,
          );
        },
      ),
    );
  }
}
