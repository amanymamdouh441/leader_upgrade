import 'package:leader_upgrade/customer/models/user_addresses_model.dart';
import 'package:leader_upgrade/customer/screens/add_update_address/AddAddressesImports.dart';
import 'package:leader_upgrade/customer/screens/user_addresses/UserAddressesImports.dart';
 import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import '../../../../res.dart';

part 'BuildMyAddressesItem.dart';