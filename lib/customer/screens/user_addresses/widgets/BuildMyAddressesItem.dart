part of 'MyAddressesWidgetsImports.dart';

class BuildMyAddressesItem extends StatelessWidget {
  final UserAddressesModel? userAddressesModel;
  final UserAddressesData userAddressesData;
  final int index;

  const BuildMyAddressesItem({
    Key? key,
    required this.userAddressesModel,
    required this.userAddressesData,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 7),
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: MyColors.white.withOpacity(0.8)),
          boxShadow: [
            BoxShadow(
              color: MyColors.greyWhite,
              spreadRadius: 0,
              blurRadius: 2,
            ),
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () => userAddressesData.deleteUserAddress(
                context, userAddressesModel!.id!,index),
            child: Icon(
              Icons.delete,
              color: MyColors.delete,
            ),
          ),
          const SizedBox(
            width: 15,
          ),
          Expanded(
            child: Column(
              children: [
                MyText(
                  title: userAddressesModel!.name ?? '',
                  color: MyColors.grey,
                  size: 10,
                  fontWeight: FontWeight.w600,
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 4,
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 4,
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    border: Border.all(color: MyColors.greyWhite),
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.white,
                        blurRadius: .5,
                      ),
                    ],
                  ),
                  child: Image.asset(
                    Res.map,
                    fit: BoxFit.contain,
                  ),
                ),
                InkWell(
                  onTap: () {
                    userAddressesData.onUpdateLocationClick(
                        context,
                        double.parse(userAddressesModel!.lat!),
                        double.parse(userAddressesModel!.lng!),
                        userAddressesModel!.name!,
                        userAddressesModel!.id!);
                  },
                  child: Container(
                    alignment: Alignment.bottomCenter,
                    decoration: BoxDecoration(
                        color: MyColors.black.withOpacity(0.3),
                        borderRadius: BorderRadius.circular(5)),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Text(
                          tr(context, "edit"),
                          style: TextStyle(color: MyColors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
