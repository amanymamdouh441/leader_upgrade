part of 'UserAddressesImports.dart';

class UserAddressesData {
  final GenericBloc<List<UserAddressesModel>> userAddressesCubit =
      GenericBloc([]);
  final LocationCubit addLocationCubit = new LocationCubit();
  final LocationCubit updateLocationCubit = new LocationCubit();

  fetchData(BuildContext context, {bool refresh = true}) async {
    final data = await CustomerRepository(context).getUserAddresses(refresh);
    print('addressDAtalength${data.length}');
    userAddressesCubit.onUpdateData(data);
  }

  // add Address
  void addUserAddress(BuildContext context, LocationModel model) async {
    UserAddressesModel? data =
        await CustomerRepository(context).addUserAddress(model);
    if (data != null) {
      print("====== address ===>${data.toJson()}");
      userAddressesCubit.state.data.insert(0, data);
      userAddressesCubit.onUpdateData(userAddressesCubit.state.data);
    }
  }

  // update Address

  void updateUserAddress(
      BuildContext context, LocationModel model, int addressId) async {
    UserAddressesModel? data =
        await CustomerRepository(context).updateUserAddress(model, addressId);

    if (data != null) {
      print("= = = = ssss = = = =${data.toJson()}");
      List<UserAddressesModel> list = userAddressesCubit.state.data;
      list.map((element) {
        if (element.id == data.id) {
          print("=== element before===>${element.toJson()}");
          element.lat = data.lat;
          element.lng = data.lng;
          element.name = data.name;
          print("=== element after===>${element.toJson()}");
        }

        return element;
      }).toList();
      userAddressesCubit.onUpdateData(list);
    }
  }

// delete Address
  void deleteUserAddress(BuildContext context, int addressId, int index) async {
    var data = await CustomerRepository(context).deleteAddress(addressId);
    if (data) {
      userAddressesCubit.state.data.removeAt(index);
      userAddressesCubit.onUpdateData(userAddressesCubit.state.data);
    }
  }

  onAddLocationClick(BuildContext context) async {
    await Geolocator.requestPermission();
    var _loc = await Utils.getCurrentLocation();
    addLocationCubit.onLocationUpdated(LocationModel(
      lat: _loc?.latitude ?? 24.774265,
      lng: _loc?.longitude ?? 46.738586,
      address: "",
    ));

    Navigator.of(context)
        .push<bool>(
      CupertinoPageRoute(
        builder: (cxt) => BlocProvider.value(
          value: addLocationCubit,
          child: AddUpdateAddress(
            appBarTitle: "addNewAddress",
            bottomButtonName: 'add',
            addAddress: true,
          ),
        ),
      ),
    )
        .then((value) {
      if (value != null) {
        if (value) {
          if (addLocationCubit.state.model != null) {
            addUserAddress(context, addLocationCubit.state.model!);
          }
        }
      }
    });
  }

  onUpdateLocationClick(BuildContext context, lat, lng, address, int id) async {
    updateLocationCubit.onLocationUpdated(LocationModel(
      lat: lat,
      lng: lng,
      address: address,
    ));
    Navigator.of(context)
        .push(
      CupertinoPageRoute(
        builder: (cxt) => BlocProvider.value(
          value: updateLocationCubit,
          child: AddUpdateAddress(
            appBarTitle: "updateAddress",
            bottomButtonName: 'update',
            addAddress: false,
            lng: lng,
            lat: lat,
            id: id,
          ),
        ),
      ),
    )
        .then((value) {
      if (value != null) {
        if (value) {
          if (updateLocationCubit.state.model != null) {
            updateUserAddress(context, updateLocationCubit.state.model!, id);
          }
        }
      }
    });
  }
}
