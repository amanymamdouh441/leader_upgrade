part of 'CompleteOrderWidgetsImports.dart';

class BuildSelectedSepcItem extends StatelessWidget {
  final SubSpecificationModel? model;

  const BuildSelectedSepcItem(
      {required this.model});

  @override
  Widget build(BuildContext context) {
    if (model==null) {
      return Container();
    }
    return Container(
      width: 90,
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(3),
          border: Border.all(color: MyColors.black, width: 1)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Offstage(
            offstage: model!.code == null,
            child: Container(
              height: 10,
              width: 10,
              color: Color(
                int.parse(
                  model!.code?.replaceFirst("#", "0xff") ??
                      "0xff000",
                ),
              ),
            ),
          ),
          SizedBox(width: 5),
          MyText(
            title: model!.name,
            color: MyColors.black,
            size: 10,
            fontWeight: FontWeight.w700,
          ),
        ],
      ),
    );
  }
}
