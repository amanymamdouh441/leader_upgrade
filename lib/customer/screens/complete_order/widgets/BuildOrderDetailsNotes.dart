part of 'CompleteOrderWidgetsImports.dart';

class BuildOrderDetailsNotes extends StatelessWidget {
  final CartModel cartModel;
  final CompleteOrderData completeOrderData;

  const BuildOrderDetailsNotes(
      {required this.cartModel, required this.completeOrderData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title: tr(context, "addDiscount"),
            color: MyColors.blackOpacity,
            size: 12,
          ),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            controller: completeOrderData.coupon,
            fillColor: MyColors.primary.withOpacity(0.05),
            hint: tr(context, "addDiscount"),
            suffixIcon: Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () =>
                    completeOrderData.useCoupon(context, cartModel.total),
                child: MyText(
                  title: tr(context, "active"),
                  color: MyColors.primary,
                  size: 13,
                ),
              ),
            ),
            validate: (val) {},
          ),
          SizedBox(height: 10),
          MyText(
            title: tr(context, "notes"),
            color: MyColors.blackOpacity,
            size: 12,
          ),
          GenericTextField(
            fieldTypes: FieldTypes.rich,
            type: TextInputType.text,
            controller: completeOrderData.orderDetails,
            action: TextInputAction.done,
            validate: (value) => value!.validateEmpty(context),
            hint: tr(context, "WriteNotesHere"),
            max: 3,
          ),
        ],
      ),
    );
  }
}
