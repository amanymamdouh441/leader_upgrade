part of 'CompleteOrderWidgetsImports.dart';

class BuildCompleteOrdersDetails extends StatelessWidget {
  final CartModel cartModel;
  final CompleteOrderData completeOrderData;

  const BuildCompleteOrdersDetails(
      {required this.cartModel, required this.completeOrderData});

  @override
  Widget build(BuildContext context) {
    var totalPrice=cartModel.total+cartModel.shippingFee;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title:
                " ${tr(context, "shippingFee")} : ${cartModel.shippingFee} ${tr(context, "sr")} ",
            color: MyColors.blackOpacity,
            size: 10,
          ),
          MyText(
            title:
            " ${tr(context, "tax")} : ${cartModel.commission} % ",
            color: MyColors.blackOpacity,
            size: 10,
          ),
          MyText(
            title:
                " ${tr(context, "TotalWithTax")} : ${cartModel.total.toStringAsFixed(2)} ${tr(context, "sr")}",
            color: MyColors.blackOpacity,
            size: 10,
          ),
          MyText(
            title:
            " ${tr(context, "total")} : ${totalPrice.toStringAsFixed(2)} ${tr(context, "sr")}",
            color: MyColors.blackOpacity,
            size: 10,
          ),
          BlocBuilder<GenericBloc<CouponModel?>, GenericState<CouponModel?>>(
            bloc: completeOrderData.couponBloc,
            builder: (_, state) {
              if (state.data?.lasttotal != null) {
                return MyText(
                  title:
                      " ${tr(context, "totalAfterDiscount")} : ${state.data?.lasttotal.toStringAsFixed(2)} ${tr(context, "includeAddedValue")} ",
                  color: MyColors.blackOpacity,
                  size: 10,
                );
              } else {
                return Container();
              }
            },
          ),

          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: cartModel.cartItems.length,
            itemBuilder: (_, index) {
              return BuildSelectedOrderItem(
                cartItemModel: cartModel.cartItems[index],
              );
            },
          )
        ],
      ),
    );
  }
}
