part of 'CompleteOrderWidgetsImports.dart';

class BuildSelectedOrderItem extends StatelessWidget {
  final CartItemModel cartItemModel;

  const BuildSelectedOrderItem({required this.cartItemModel});

  @override
  Widget build(BuildContext context) {
    var subSpecs = cartItemModel.cats.map((e) {
      e.subSpecifications = e.subSpecifications
          .where((subElem) => subElem.isChecked == true)
          .toList();
      return e.subSpecifications.isEmpty ? null : e.subSpecifications.first;
    }).toList();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 10, left: 10),
          child: Divider(thickness: 1),
        ),
        MyText(
          title:
          " ${tr(context, "order")}",
          color: MyColors.black,
          fontWeight: FontWeight.bold,
          size: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                    title: cartItemModel.productName,
                    color: MyColors.blackOpacity,
                    size: 10,
                  ),
                  MyText(
                    title: "${cartItemModel.price} ${tr(context, "sr")}",
                    color: MyColors.blackOpacity,
                    size: 10,
                  ),
                  MyText(
                    title: " ${tr(context, "qty")} : ${cartItemModel.qty}",
                    color: MyColors.blackOpacity,
                    size: 10,
                  ),
                  Wrap(
                    spacing: 15,
                    runSpacing: 10,
                    children: List.generate(subSpecs.length, (position) {
                      return BuildSelectedSepcItem(model: subSpecs[position]);
                    }),
                  ),
                ],
              ),
            ),
            CachedImage(
              url: cartItemModel.img,
              height: 100,
              width: 90,
            )
          ],
        ),
        // Container(
        //   margin: const EdgeInsets.symmetric(vertical: 5),
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //     children: [
        //       Flexible(
        //         child: Column(
        //           crossAxisAlignment: CrossAxisAlignment.start,
        //           children: [
        //             MyText(
        //               title: cartItemModel.productName,
        //               color: MyColors.blackOpacity,
        //               size: 10,
        //             ),
        //             MyText(
        //               title: "${cartItemModel.price} ${tr(context, "sr")}",
        //               color: MyColors.blackOpacity,
        //               size: 10,
        //             ),
        //             MyText(
        //               title: " ${tr(context, "qty")} : ${cartItemModel.qty}",
        //               color: MyColors.blackOpacity,
        //               size: 10,
        //             ),
        //             Wrap(
        //               spacing: 15,
        //               runSpacing: 10,
        //               children: List.generate(subSpecs.length, (position) {
        //                 return BuildSelectedSepcItem(model: subSpecs[position]);
        //               }),
        //             ),
        //           ],
        //         ),
        //       ),
        //       CachedImage(
        //         url: cartItemModel.img,
        //         height: 100,
        //         width: 90,
        //       )
        //     ],
        //   ),
        // ),
      ],
    );
  }
}
