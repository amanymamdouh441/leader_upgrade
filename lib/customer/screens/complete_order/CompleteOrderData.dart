part of 'CompleteOrderImports.dart';

class CompleteOrderData {
  final GenericBloc<int> selectedCubit = GenericBloc(1);
  final TextEditingController orderDetails = new TextEditingController();
  final GenericBloc<CouponModel?> couponBloc = GenericBloc(null);
  final TextEditingController coupon = new TextEditingController();

  void useCoupon(BuildContext context, num total) async {
    var data = await CustomerRepository(context).useCoupon(coupon.text, total);
    if (data != null) {
      couponBloc.onUpdateData(data);
    }
  }

  void addOrder(
      BuildContext context, AddOrderModel model, CartModel cartModel,cityName) async {


    model.notes = orderDetails.text;
     model.cityName=cityName;
    model.typePay = selectedCubit.state.data;
    model.total = couponBloc.state.data?.lasttotal ?? cartModel.total;
    model.shippingFee = cartModel.shippingFee;
    model.commission = cartModel.commission;
    model.coponId = couponBloc.state.data?.coponId;
    var data = await CustomerRepository(context).addOrder(model);

    if (data) {
      context.read<CartCountCubit>().onUpdateCount(0);
      AutoRouter.of(context).push(SuccessRoute(payment: true));
    }
  }
}
