part of 'CompleteOrderImports.dart';

class CompleteOrder extends StatefulWidget {
  final CartModel cartModel;
  final AddOrderModel addOrderModel;
  final String cityName;

  const CompleteOrder(
      {required this.cartModel,
      required this.addOrderModel,
      required this.cityName});

  @override
  _CompleteOrderState createState() => _CompleteOrderState();
}

class _CompleteOrderState extends State<CompleteOrder> {
  final CompleteOrderData completeOrderData = CompleteOrderData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(title: tr(context, "confirmOrder")),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        children: [
          BuildCompleteOrdersDetails(
            cartModel: widget.cartModel,
            completeOrderData: completeOrderData,
          ),
          BuildOrderDetailsNotes(
            cartModel: widget.cartModel,
            completeOrderData: completeOrderData,
          ),
          BuildChoosePayment(completeOrderData: completeOrderData),
        ],
      ),
      bottomNavigationBar: DefaultButton(
        title: tr(context, "confirm"),
        onTap: () => completeOrderData.addOrder(
            context, widget.addOrderModel, widget.cartModel, widget.cityName),
        margin: const EdgeInsets.all(20),
      ),
    );
  }
}
