part of 'ReturnImports.dart';

class ReturnData {
  final GlobalKey<FormState> formKey =   GlobalKey<FormState>();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey<CustomButtonState>();
  final GenericBloc<List<File>> productCubit =   GenericBloc([]);
  final GenericBloc<File?> billCubit =   GenericBloc(null);
  final TextEditingController msg =   TextEditingController();
  DropDownModel? returnModel;
  //String? returnReason;
  final GlobalKey<DropdownSearchState> returnId =   GlobalKey();

  void onSelectReturn(DropDownModel model) {
    returnModel = model;
  }
  // void onSelectReturn(String model) {
  //   returnReason = model;
  // }

  void setProductImage() async {
    print("object");
    var image = await Utils.getImages();
    if (image.length > 0) {
      var data = productCubit.state.data;
      data.addAll(image);
      productCubit.onUpdateData(data);
    }
  }

  void deleteImages(int index) async {
    print(index.toString() + 'index');
    productCubit.state.data.removeAt(index);
    productCubit.onUpdateData(productCubit.state.data);
  }

  void setBillImage(imageSource,  ) async {
    print("setBillImage");

    var image = await Utils.getReturnImage(imageSource, );
    billCubit.onUpdateData(image);
  }

  void addOrder(BuildContext context, int orderId) async {
    log("eeeeeeeeeeeee$orderId");
   // FocusScope.of(context).requestFocus(FocusNode());
    if(returnModel == null){
      CustomToast.showSimpleToast(msg: tr(context,"returnReason"));
      return ;
    }
    // if(msg.text.isEmpty){
    //   CustomToast.showSimpleToast(msg: tr(context,"returnNotes"));
    //   return ;
    // }
    if (productCubit.state.data.isEmpty || billCubit.state.data == null) {
      CustomToast.showSimpleToast(msg: tr(context, "addBillValue"));
      return;
    }

      AddReturnModel model = AddReturnModel(
        orderId: orderId,
        note: msg.text,
        payImg: billCubit.state.data,
        productImg: productCubit.state.data,
        returnReasonId: returnModel?.id,
      );
      var data = await CustomerRepository(context).addReturn(model);
      if (data) {
        log("eeeeeeeeeeee$orderId");
        AutoRouter.of(context).push(SuccessRoute(payment: false));
      }
    }

}
