part of 'ReturnImports.dart';

class Return extends StatefulWidget {
  final int orderId;

  const Return({required this.orderId});

  @override
  _ReturnState createState() => _ReturnState();
}

class _ReturnState extends State<Return> {
  final ReturnData returnData = ReturnData();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(title: tr(context,"returnOrder")),
      body: Column(
        children: [
          Flexible(
            child: ListView(
              padding: const EdgeInsets.all(20),
              children: [
                Form(
                  key: returnData.formKey,
                  child: Column(
                    children: [
                      BuildReturnReason(returnData: returnData),
                      BuildReturnNotes(returnData: returnData),
                    ],
                  ),
                ),
                BuildProductBill(returnData: returnData),
                BuildProductPhoto(returnData: returnData),
              ],
            ),
          ),
          DefaultButton(
            title: tr(context, "send"),
            margin: const EdgeInsets.all(20),
            onTap: () {
              returnData.addOrder(context, widget.orderId);
              log("dddddddddddd");

    }
          )
        ],
      ),
    );
  }
}
