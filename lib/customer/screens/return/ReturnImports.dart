import 'dart:developer';
import 'dart:io';

import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/models/Dtos/AddReturnModel.dart';
import 'package:leader_upgrade/customer/models/drop_down_model.dart';
import 'package:leader_upgrade/customer/models/user_addresses_model.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/customer/screens/return/widgets/ReturnWidgetsImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tf_custom_widgets/Inputs/custom_dropDown/CustomDropDown.dart';
import 'package:tf_custom_widgets/utils/CustomButtonAnimation.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/DefaultButton.dart';
import 'package:tf_validator/tf_validator.dart';

part 'Return.dart';
part 'ReturnData.dart';