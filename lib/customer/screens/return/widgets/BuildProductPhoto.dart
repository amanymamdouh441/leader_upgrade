part of 'ReturnWidgetsImports.dart';

class BuildProductPhoto extends StatelessWidget {
  final ReturnData returnData;

  const BuildProductPhoto({required this.returnData});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10),
          child: Row(
            children: [
              MyText(
                title: tr(context, "productImg"),
                color: MyColors.black,
                size: 12,
              ),
              MyText(
                title: " * ",
                color: Colors.red,
                size: 12,
              ),
            ],
          ),
        ),
        InkWell(
          onTap: () => returnData.setProductImage(),
          child: Container(
            height: 100,
            width: MediaQuery.of(context).size.width * 0.3,
            margin: const EdgeInsets.symmetric(vertical: 5),
            decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            alignment: Alignment.center,
            child: Image.asset(Res.addImage, height: 50),
          ),
        ),
        BlocBuilder<GenericBloc<List<File>>, GenericState<List<File>>>(
          bloc: returnData.productCubit,
          builder: (BuildContext context, state) {
            return Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10
              ),
              child: Wrap(
                spacing: 10,
                runSpacing: 10,
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.start,
                crossAxisAlignment: WrapCrossAlignment.start,
                children: List.generate(
                  state.data.length,
                      (index) {
                    return Container(
                      width: 90,
                      height: 90,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: FileImage(
                            state.data[index],
                          ),
                        ),
                      ),
                      alignment: Alignment.topRight,
                      child: InkWell(
                        onTap: () =>returnData.deleteImages(index),
                        child: Container(
                          width: 25,
                          height: 25,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Icon(
                            Icons.close,
                            size: 15,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
