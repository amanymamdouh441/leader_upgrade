import 'dart:io';

import 'package:leader_upgrade/customer/models/drop_down_model.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/customer/screens/return/ReturnImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/widgets/show_dialog.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tf_custom_widgets/Inputs/DropdownTextField.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_validator/tf_validator.dart';
part 'BuildProductBill.dart';
part 'BuildProductPhoto.dart';
part 'BuildReturnNotes.dart';
part 'BuildReturnReason.dart';