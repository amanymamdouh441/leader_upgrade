part of 'ReturnWidgetsImports.dart';

class BuildReturnReason extends StatelessWidget {
final ReturnData returnData;

  const BuildReturnReason({required this.returnData}) ;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            MyText(
              title: tr(context, "prodReturnReason"),
              color: MyColors.black,
              size: 11,
            ),
            MyText(
              title: " * ",
              color: Colors.red,
              size: 11,
            ),
          ],
        ),

        DropdownTextField<DropDownModel>(
          title: 'reasons',
          itemAsString: (item)=> item.name,
          dropKey: returnData.returnId,
          label: tr(context, "choose"),
          searchHint: tr(context, "search"),
          enableColor: MyColors.grey,
          selectedItem: returnData.returnModel,
          margin: const EdgeInsets.symmetric(vertical: 10),
          validate: ( v)=> "",
          //validate: ( value) => value.validateDropDown(context),
          onChange:(value)=> returnData.onSelectReturn(value!),
          useName: true,
          onFind: (filter) async =>
          await CustomerRepository(context).getReturnReasons(false),
        ),


        // DropdownTextField<DropDownModel>(
        //   title: 'reasons',
        //   itemAsString: (item)=> item.name,
        //   dropKey: returnData.returnId,
        //   label: tr(context, "choose"),
        //   searchHint: tr(context, "search"),
        //   enableColor: MyColors.grey,
        //   selectedItem: returnData.returnModel,
        //   margin: const EdgeInsets.symmetric(vertical: 10),
        //   validate: ( v)=> "",
        //   //validate: ( value) => value.validateDropDown(context),
        //   onChange:(value)=> returnData.onSelectReturn(value!),
        //   useName: true,
        //   onFind: (filter) async =>
        //   await CustomerRepository(context).getReturnReasons(false),
        // ),
      ],
    );
  }
}
