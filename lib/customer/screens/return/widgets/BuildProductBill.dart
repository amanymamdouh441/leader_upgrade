part of 'ReturnWidgetsImports.dart';

class BuildProductBill extends StatelessWidget {
  final ReturnData returnData;

  const BuildProductBill({required this.returnData});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 20),
          child: Row(
            children: [
              MyText(
                title: tr(context, "billImg"),
                color: MyColors.black,
                size: 12,
              ),
              MyText(
                title: " * ",
                color: Colors.red,
                size: 12,
              ),
            ],
          ),
        ),
        BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
          bloc: returnData.billCubit,
          builder: (context, state) {
            if (state.data != null) {
              return Container(
                height: 100,
                alignment: Alignment.topRight,
                width: MediaQuery.of(context).size.width * 0.4,
                margin: const EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: FileImage(state.data!),
                    fit: BoxFit.cover,
                  ),
                ),
                child: InkWell(
                  onTap: () => showAlertDialog(
                    context: context,
                  returnData: returnData),
                  child: Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                      color: MyColors.primary,
                      shape: BoxShape.circle,
                    ),
                    alignment: Alignment.center,
                    child: Icon(Icons.edit, color: MyColors.white, size: 20),
                  ),
                ),
              );
            }
            return InkWell(
              onTap: () => showAlertDialog(
                  context: context,

                  returnData: returnData),
              child: Container(
                height: 100,
                width: MediaQuery.of(context).size.width * 0.4,
                margin: const EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                alignment: Alignment.center,
                child: Image.asset(Res.addImage, height: 50),
              ),
            );
          },
        ),
      ],
    );
  }
}
