part of 'ReturnWidgetsImports.dart';

class BuildReturnNotes extends StatelessWidget {
final ReturnData returnData;

  const BuildReturnNotes({super.key, required this.returnData}) ;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10, bottom: 5),
          child: MyText(
            title: tr(context, "anotherNote"),
            color: MyColors.black,
            size: 11,
          ),
        ),
        GenericTextField(
          fieldTypes: FieldTypes.rich,
          type: TextInputType.text,
          action: TextInputAction.done,
          controller: returnData.msg,
          validate: (value) => value!.validateEmpty(context),
          hint: tr(context, "WriteNotesHere"),
          max: 5,
          fillColor: MyColors.white,
        ),
      ],
    );
  }
}
