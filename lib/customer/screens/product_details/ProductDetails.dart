part of 'ProductDetailsImports.dart';

class ProductDetails extends StatefulWidget {
  final int id;
  final String fromWhere;
  final String fromNormal;

  //fromWhere = print لو قسم طباعه
  //fromNormal = special لو قسم طباعه خاصه

  const ProductDetails(
      {Key? key,
      required this.id,
      this.fromWhere = "anyWhere",
      this.fromNormal = "normal"})
      : super(key: key);

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final ProductDetailsData productDetailsDta = ProductDetailsData();

  @override
  void initState() {
    print("??????????${widget.fromNormal}");
    productDetailsDta.fetchProductDetails(context, widget.id, refresh: false);
    productDetailsDta.fetchProductDetails(context, widget.id);

    // productDetailsDta.fetchSpecifications(context, widget.id, refresh: false);
    // productDetailsDta.fetchSpecifications(context, widget.id);
    // productDetailsDta.favCubit.onUpdateData(widget.favorite);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
          BlocBuilder<GenericBloc<ProductModel?>, GenericState<ProductModel?>>(
        bloc: productDetailsDta.productCubit,
        builder: (_, state) {
          if (state is GenericUpdateState) {
            return ListView(
              padding: EdgeInsets.zero,
              children: [
                BuildProductDetailsSlider(
                  images: state.data!.images ?? [],
                  onBack: () => AutoRouter.of(context).pop(state.data),
                ),
                BuildProductInfo(
                  model: state.data!,
                  detailsDta: productDetailsDta,
                  fromWhere: widget.fromWhere,
                  fromNormal: widget.fromNormal,
                ),
                BuildProductSpec(detailsDta: productDetailsDta),
                BuildAddPrintInputs(
                  fromWhere: widget.fromWhere,
                  fromNormal: widget.fromNormal,
                  productDetailsData: productDetailsDta,
                ),
              ],
            );
          } else {
            return LoadingDialog.showLoadingView();
          }
        },
      ),
      bottomNavigationBar: BuildBottomButtons(
        detailsDta: productDetailsDta,
        fromWhere: widget.fromWhere,
        fromNormal: widget.fromNormal,
      ),
    );
  }
}
