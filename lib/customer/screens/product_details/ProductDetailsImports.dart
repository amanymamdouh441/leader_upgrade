import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/models/Dtos/AddCartModel.dart';
import 'package:leader_upgrade/customer/models/Dtos/AddPrintOrderModel.dart';
import 'package:leader_upgrade/customer/models/product_model.dart';
import 'package:leader_upgrade/customer/models/specification_model.dart';
import 'package:leader_upgrade/customer/models/sub_specification_model.dart';
import 'package:leader_upgrade/general/blocks/auth_cubit/auth_cubit.dart';
import 'package:leader_upgrade/general/blocks/cart_count_cubit/cart_count_cubit.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/utilities/db/ProductTable.dart';
import 'package:leader_upgrade/general/utilities/db/db.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'widgets/ProductDetailsWidgetsImports.dart';

part 'ProductDetails.dart';
part 'ProductDetailsData.dart';