part of 'ProductDetailsImports.dart';

class ProductDetailsData {
  final GenericBloc<List<SpecificationModel>> specCubit = GenericBloc([]);
  final GenericBloc<bool> favCubit = GenericBloc(false);
  final GenericBloc<int> countCubit = GenericBloc(1);
  final GenericBloc<File?> fileCubit = GenericBloc(null);
  final TextEditingController printImage = new TextEditingController();
  final TextEditingController msg = new TextEditingController();
  final TextEditingController name = new TextEditingController();
  final TextEditingController productName = new TextEditingController();
  final TextEditingController number = new TextEditingController();
  final GenericBloc<ProductModel?> productCubit = GenericBloc(null);
  List<SubSpecificationModel>? selectedOne;


  Future<void> fetchProductDetails(BuildContext context, int proId,
      {bool refresh = true}) async {
    var data =
    await CustomerRepository(context).getProductDetails(proId, refresh);
    productCubit.onUpdateData(data);
    print("@@@@@@@@@@@@@${productCubit.state.data}");
    favCubit.onUpdateData(productCubit.state.data!.favorite);
    specCubit.onUpdateData(data.specification!);

  }



 Future<void> setPrintImage() async {
    var image = await Utils.getImage();
    if (image != null) {
      fileCubit.onUpdateData(image);

    }
  }

  onItemSelected(SpecificationModel model, int id, int index, int position) {
    model.selected.clear();
    model.selected.add(id);
    model.subSpecifications = model.subSpecifications.map((e) {
      e.isChecked = false;
      return e;
    }).toList();
    model.subSpecifications[position].isChecked = true;

    selectedOne = model.subSpecifications
        .where((element) => element.isChecked == true)
        .map((e) {
      return e;
    }).toList();

    specCubit.state.data[index] = model;
    specCubit.onUpdateData(specCubit.state.data);
  }

  checkAddFav(BuildContext context, ProductModel model) async {
    if (context.read<AuthCubit>().state.authorized) {
      addToFav(context, model);
    } else {
      CustomToast.showAuthDialog(context: context);
    }
  }

  addToFav(BuildContext context, ProductModel model) async {
    var result = await CustomerRepository(context).addToFavourite(model.id);
    if (result) {
      model.favorite = !model.favorite;
      favCubit.onUpdateData(!favCubit.state.data);
    }
  }

  void increaseQty() {
    if (countCubit.state.data != 0) {
      var newQty = countCubit.state.data + 1;

      countCubit.onUpdateData(newQty);
    }
  }

  void decreaseQty() {
    if (countCubit.state.data > 1) {
      var newQty = countCubit.state.data - 1;
      countCubit.onUpdateData(newQty);
    }
  }

  changeProCount(int value) {
    if (value <= 0) return;
    countCubit.onUpdateData(value);
  }

  checkAddCart(BuildContext context, ProductModel model) async {
    if (context.read<AuthCubit>().state.authorized) {
      addToCartServer(context, model);
    } else {
      addToCartLocal(context, model);
    }
  }

  addToCartServer(BuildContext context, ProductModel model) async {
    if (specCubit.state.data.any((e) => e.selected.isEmpty)) {
      CustomToast.showSimpleToast(
          msg: tr(context, "selectSpecs"), color: MyColors.gold);
      return;
    }

    var ids = specCubit.state.data
        .where((e) => e.selected.isNotEmpty)
        .map((e) => e.selected)
        .toList();
    AddCartModel cartModel = AddCartModel(
      subIds: ids.expand((element) => element).toList(),
      productId: model.id,
      qty: countCubit.state.data,
    );
    var result = await CustomerRepository(context).addToCart(cartModel);
    if (result) {
      var cubit = context.read<CartCountCubit>();
      int count = cubit.state.count + 1;
      cubit.onUpdateCount(count);
    }
  }

  addToCartLocal(BuildContext context, ProductModel model) async {
    if (specCubit.state.data.any((e) => e.selected.isEmpty)) {
      CustomToast.showSimpleToast(
          msg: tr(context, "selectSpecs"), color: MyColors.gold);
      return;
    }

    ProductTableData cartModel = ProductTableData(
      cats: json.encode(specCubit.state.data),
      proId: model.id,
      qty: countCubit.state.data,
      name: model.name,
      image: model.mainImg,
      price: model.price.toDouble(),
    );
    var db = context.read<MyDatabase>();
    var result = await db.addProduct(cartModel.toCompanion(true));
    if (result != 0) {
      CustomToast.showSimpleToast(
          msg: tr(context, "addedCartSuccess"), color: MyColors.gold);
      var cubit = context.read<CartCountCubit>();
      int count = cubit.state.count + 1;
      cubit.onUpdateCount(count);
    }
  }

  void navigateToPrintAddress(
      BuildContext context, ProductModel productModel, String fromNormal) {
    if (specCubit.state.data.any((e) => e.selected.isEmpty)) {
      CustomToast.showSimpleToast(
          msg: tr(context, "selectSpecs"), color: MyColors.gold);
      return;
    }
    if (fileCubit.state.data == null && fromNormal == "special") {
      CustomToast.showSimpleToast(
          msg: tr(context, "shouldAddPrintImage"), color: MyColors.gold);
      return;
    }
    var ids = specCubit.state.data
        .where((e) => e.selected.isNotEmpty)
        .map((e) => e.selected)
        .toList();
    var subSpecificationName = selectedOne?.first.name;
    AddPrintOrderModel model = AddPrintOrderModel(
      subSpecificationIds: ids.expand((element) => element).toList().toString(),
      subSpecificationNames: subSpecificationName,
      printImage: fileCubit.state.data,
      productImage: productModel.mainImg,
      productId: productModel.id,
      productName: productModel.name,
      qty: countCubit.state.data,
      productPrice: productModel.price,
      total: productModel.price * countCubit.state.data,
      // shippingFee: ,

      // notes: ,
      // typePay: ,
      isNormalPrint: fromNormal == "normal" ? true : false,
      isSpecialPrint: fromNormal == "special" ? true : false,
      printNotes: msg.text,
      printNumber: number.text,
      printName: name.text,
      printProductName: productName.text,
    );
    print("___________________${model.toJson()}");
    print("________________${subSpecificationName}");

    AutoRouter.of(context).push(PrintAddressRoute(addPrintOrderModel: model));
  }
}
