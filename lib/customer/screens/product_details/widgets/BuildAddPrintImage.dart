part of 'ProductDetailsWidgetsImports.dart';

class BuildAddPrintImage extends StatelessWidget {
  final String fromWhere;
  final String fromNormal;
  final ProductDetailsData productDetailsData;

  const BuildAddPrintImage(
      {required this.fromWhere,
      required this.fromNormal,
      required this.productDetailsData});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GenericBloc<File?>, GenericState<File?>>(
      bloc: productDetailsData.fileCubit,
      listener: (context, state) {
        if (state.data != null) {
          productDetailsData.printImage.text = state.data!.path.split("/").last;
        }
      },
      builder: (context, state) {
        if (state.data == null) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  title: tr(context, "printImage"),
                  color: MyColors.black,
                  size: 12),
              GenericTextField(
                fieldTypes: FieldTypes.clickable,
                hint: tr(context, "addPrintImage"),
                controller: productDetailsData.printImage,
                margin: const EdgeInsets.symmetric(vertical: 10),
                action: TextInputAction.next,
                type: TextInputType.text,
                onTab: () => productDetailsData
                    .setPrintImage(),
                validate: (value) => value!.validateEmpty(context),
                enableBorderColor: MyColors.grey,
                suffixIcon: Icon(Icons.upload_rounded),
              ),
            ],
          );
        } else {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  title: tr(context, "printImage"),
                  color: MyColors.black,
                  size: 12),
              GenericTextField(
                fieldTypes: FieldTypes.clickable,
                hint: tr(context, "addPrintImage"),
                controller: productDetailsData.printImage,
                margin: const EdgeInsets.symmetric(vertical: 10),
                action: TextInputAction.next,
                type: TextInputType.text,
                onTab: () => productDetailsData
                    .setPrintImage(),
                validate: (value) => value!.validateEmpty(context),
                enableBorderColor: MyColors.grey,
                suffixIcon: Icon(Icons.upload_rounded),
              ),
              Stack(
                children: [
                  Center(
                    child: Container(
                      width: 250,
                      height: 120,
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                        color: MyColors.white,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: MyColors.grey, width: .5),
                        image: DecorationImage(
                          image: FileImage(state.data!),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 3,
                    left: 0,
                    right: 230,
                    child: GestureDetector(
                      onTap: () => productDetailsData
                          .setPrintImage(),
                      child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: MyColors.blackOpacity,
                            width: 1,
                          ),
                        ),
                        child: Icon(
                          Icons.camera_alt,
                          color: MyColors.blackOpacity,
                          size: 17,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
        }
      },
    );
  }
}
