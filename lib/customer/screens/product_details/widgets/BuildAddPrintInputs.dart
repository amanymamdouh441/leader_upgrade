part of 'ProductDetailsWidgetsImports.dart';

class BuildAddPrintInputs extends StatelessWidget {
  final String fromWhere;
  final String fromNormal;
  final ProductDetailsData productDetailsData;

  const BuildAddPrintInputs(
      {required this.fromWhere,
      required this.fromNormal,
      required this.productDetailsData});

  @override
  Widget build(BuildContext context) {
    Widget inputsWidget = Container();
    if (fromWhere == "print" && fromNormal == "special") {
      inputsWidget = BuildSpecialPrintInputs(
        fromNormal: fromNormal,
        fromWhere: fromWhere,
        productDetailsData: productDetailsData,
      );
    }
    if (fromWhere == "print" && fromNormal == "normal") {
      inputsWidget = BuildNormalPrintInputs(
        fromNormal: fromNormal,
        fromWhere: fromWhere,
        productDetailsData: productDetailsData,
      );
    }
    return inputsWidget;
  }
}
