part of 'ProductDetailsWidgetsImports.dart';

class BuildBottomButtons extends StatelessWidget {
  final ProductDetailsData detailsDta;
  final String fromWhere;
  final String fromNormal;

  const BuildBottomButtons(
      {Key? key,
      required this.detailsDta,
      required this.fromNormal,
      required this.fromWhere})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<ProductModel?>,GenericState<ProductModel?>>(
      bloc: detailsDta.productCubit,
      builder: (_,state){
        return  Container(
          margin: const EdgeInsets.all(10),
          child: Row(
            children: [
              Expanded(
                flex: 3,
                child: Visibility(
                  visible: fromWhere == "print",
                  child: DefaultButton(
                    title: tr(context, "checkout"),
                    onTap: () => detailsDta.navigateToPrintAddress(context, state.data!,fromNormal),
                  ),
                  replacement: DefaultButton(
                    title: tr(context, "addCart"),
                    onTap: () => detailsDta.checkAddCart(context, state.data!),
                  ),
                ),
              ),
              Bounce(
                onPressed: () => detailsDta.checkAddFav(context, state.data!),
                duration: Duration(milliseconds: 150),
                child: BlocBuilder<GenericBloc, GenericState>(
                  bloc: detailsDta.favCubit,
                  builder: (context, state) {
                    return Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: Icon(
                        state.data ? Icons.favorite : Icons.favorite_border,
                        size: 40,
                        color: state.data ? Colors.red : MyColors.blackOpacity,
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        );
      },

    );

  }
}
