part of 'ProductDetailsWidgetsImports.dart';

class BuildNormalPrintInputs extends StatelessWidget {
  final String fromWhere;
  final String fromNormal;
  final ProductDetailsData productDetailsData;

  const BuildNormalPrintInputs(
      {required this.fromWhere,
      required this.fromNormal,
      required this.productDetailsData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(title: "اسم المنتج", color: MyColors.black, size: 12),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
            hint: "مثال: فانيلة نادي الهلال او نادي تشيلسي",
            enableBorderColor: MyColors.grey,
            controller: productDetailsData.productName,
            margin: const EdgeInsets.symmetric(vertical: 10),
          ),
          MyText(title: tr(context, "name"), color: MyColors.black, size: 12),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
            hint: "مثال: محمد",
            enableBorderColor: MyColors.grey,
            controller: productDetailsData.name,
            margin: const EdgeInsets.symmetric(vertical: 10),
          ),
          MyText(title: tr(context, "number"), color: MyColors.black, size: 12),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.number,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
            hint: "مثال: 20",
            enableBorderColor: MyColors.grey,
            controller: productDetailsData.number,
            margin: const EdgeInsets.symmetric(vertical: 10),
          ),
          MyText(title: tr(context, "notes"), color: MyColors.black, size: 12),
          GenericTextField(
            fieldTypes: FieldTypes.rich,
            max: 2,
            type: TextInputType.multiline,
            action: TextInputAction.newline,
            validate: (value) => value!.validateEmpty(context),
            hint: "مثال: اكتب جميع ملاحظاتك + رقم جوالك لكي نتواصل معك.",
            enableBorderColor: MyColors.grey,
            controller: productDetailsData.msg,
            margin: const EdgeInsets.symmetric(vertical: 10),
          ),
        ],
      ),
    );
  }
}
