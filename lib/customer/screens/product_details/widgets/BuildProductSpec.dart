part of 'ProductDetailsWidgetsImports.dart';

class BuildProductSpec extends StatelessWidget {
  final ProductDetailsData detailsDta;

  const BuildProductSpec({Key? key, required this.detailsDta})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<SpecificationModel>>,
        GenericState<List<SpecificationModel>>>(
      bloc: detailsDta.specCubit,
      builder: (context, state) {
        return Column(
          children: List.generate(state.data.length, (index) {
            return Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                    title: state.data[index].name,
                    color: MyColors.blackOpacity,
                    size: 12,
                  ),
                  GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: state.data[index].subSpecifications.length,
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    gridDelegate: GridFixedHeightDelegate(
                      crossAxisCount: 3,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 15,
                      height: 35,
                    ),
                    itemBuilder: (_, position) {
                      return BuildSpecItem(
                        model: state.data[index].subSpecifications[position],
                        selected: state.data[index].selected,
                        onClick: (int id)=> detailsDta.onItemSelected(state.data[index], id, index,position),
                      );
                    },
                  ),
                ],
              ),
            );
          }),
        );
      },
    );
  }
}
