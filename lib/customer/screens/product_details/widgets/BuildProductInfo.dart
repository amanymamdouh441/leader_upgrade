part of 'ProductDetailsWidgetsImports.dart';

class BuildProductInfo extends StatelessWidget {
  final ProductModel model;
  final ProductDetailsData detailsDta;
  final String fromWhere;
  final String fromNormal;

  const BuildProductInfo({
    Key? key,
    required this.model,
    required this.detailsDta,
    required this.fromWhere,
    required this.fromNormal,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title: model.name,
            color: MyColors.primary,
            size: 15,
            fontWeight: FontWeight.bold,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                title: "${model.price} ${tr(context, "sr")}",
                color: MyColors.black,
                size: 16,
              ),

              BlocBuilder<GenericBloc<int>, GenericState<int>>(
                bloc: detailsDta.countCubit,
                builder: (context, state) {
                  return Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(color: MyColors.blackOpacity)),
                    child: Row(
                      children: [
                        Bounce(
                          onPressed: detailsDta.increaseQty,
                          duration: Duration(milliseconds: 200),
                          child: Icon(
                            Icons.add,
                            color: MyColors.blackOpacity,
                            size: 17,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                          child: VerticalDivider(
                            color: MyColors.blackOpacity,
                            thickness: 0.5,
                          ),
                        ),
                        MyText(
                          title: "${state.data}",
                          color: Colors.red,
                          size: 12,
                        ),
                        SizedBox(
                          height: 15,
                          child: VerticalDivider(
                            color: MyColors.blackOpacity,
                            thickness: 0.5,
                          ),
                        ),
                        Bounce(
                          onPressed: detailsDta.decreaseQty,
                          duration: Duration(milliseconds: 200),
                          child: Icon(
                            MdiIcons.minus,
                            color: MyColors.blackOpacity,
                            size: 17,
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),

            ],
          ),
          Visibility(
            visible: fromWhere == "print",
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MyText(
                    title: tr(context, "printType"),
                    color: MyColors.black,
                    size: 12,
                  ),
                  MyText(
                    title: "${model.typePrint}",
                    color: MyColors.primary,
                    size: 14,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
