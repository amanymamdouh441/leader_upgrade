part of 'ProductDetailsWidgetsImports.dart';

class BuildProductDetailsSlider extends StatelessWidget {
  final List<ImageModel> images;
  final Function() onBack;

  const BuildProductDetailsSlider({
    Key? key,
    required this.images,
    required this.onBack,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget slider = Container(
      height: MediaQuery.of(context).size.height * 0.4,
      alignment: Alignment.center,
      child: SvgPicture.asset(Res.noImage),
    );
    if (images.isNotEmpty) {
      slider = Container(
        height: MediaQuery.of(context).size.height * 0.4,
        child: Swiper(
          itemCount: images.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () => AutoRouter.of(context).push(ImageZoomRoute(
                images: images,
                index: index,
              )),
              child: CachedImage(
                url: images[index].img,
                fit: BoxFit.cover,
              ),
            );
          },
          autoplay: true,
          scrollDirection: Axis.horizontal,
          pagination: SwiperPagination(alignment: Alignment.bottomCenter),
        ),
      );
    }
    return Stack(
      children: [
        slider,
        AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          toolbarHeight: 40,
          leading: InkWell(
            onTap: onBack,
            child: Container(
              alignment: Alignment.center,
              decoration:
                  BoxDecoration(color: Colors.black26, shape: BoxShape.circle),
              child: Icon(
                Icons.arrow_back_ios,
                color: MyColors.white,
                size: 20,
              ),
            ),
          ),
          actions: [CartIcon()],
        )
      ],
    );
  }
}
