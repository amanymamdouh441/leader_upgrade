part of 'ProductDetailsWidgetsImports.dart';

class BuildSpecialPrintInputs extends StatelessWidget {
  final String fromWhere;
  final String fromNormal;
  final ProductDetailsData productDetailsData;

  const BuildSpecialPrintInputs(
      {required this.fromWhere,
      required this.fromNormal,
      required this.productDetailsData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BuildAddPrintImage(
            fromWhere: fromWhere,
            fromNormal: fromNormal,
            productDetailsData: productDetailsData,
          ),
          MyText(title: tr(context, "notes"), color: MyColors.black, size: 12),
          GenericTextField(
            fieldTypes: FieldTypes.rich,
            max: 2,
            type: TextInputType.text,
            action: TextInputAction.done,
            validate: (value) => value!.validateEmpty(context),
            hint: "مثال: اكتب جميع ملاحظاتك + رقم جوالك لكي نتواصل معك.",
            enableBorderColor: MyColors.grey,
            controller: productDetailsData.msg,
            margin: const EdgeInsets.symmetric(vertical: 10),
          ),
        ],
      ),
    );
  }
}
