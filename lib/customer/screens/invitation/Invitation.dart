part of 'InvitationImports.dart';

class Invitation extends StatefulWidget {
  const Invitation({Key? key}) : super(key: key);

  @override
  _InvitationState createState() => _InvitationState();
}

class _InvitationState extends State<Invitation> {
  final InvitationData invitationData = InvitationData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(
        title: tr(context, "invitationCode"),
        actions: [NotifyIcon()],
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 30),
        children: [
          Image.asset(Res.invite, scale: 3.5),
          BuildInviteText(invitationData: invitationData),
        ],
      ),
    );
  }
}
