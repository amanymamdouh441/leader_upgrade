part of 'InvitationImports.dart';

class InvitationData {
  void shareApp(BuildContext context) {
    var invite = context.read<UserCubit>().state.model?.invitationCode;
    Utils.shareApp("$invite");
  }
}
