import 'package:leader_upgrade/customer/screens/invitation/InvitationImports.dart';
import 'package:leader_upgrade/general/blocks/user_cubit/user_cubit.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
part 'BuildInviteText.dart';