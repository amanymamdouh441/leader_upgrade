part of 'InvitationWidgetsImports.dart';

class BuildInviteText extends StatelessWidget {
final InvitationData invitationData;

  const BuildInviteText({required this.invitationData}) ;
  @override
  Widget build(BuildContext context) {
    var invite=context.read<UserCubit>().state.model?.invitationCode;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          MyText(
            title: tr(context, "yourCodeInvitation"),
            color: MyColors.black,
            size: 17,
            fontWeight: FontWeight.bold,
          ),
          MyText(
            title: "$invite",
            color: MyColors.primary,
            size: 40,
            fontWeight: FontWeight.bold,
          ),
          InkWell(
            onTap: ()=>invitationData.shareApp(context),
            child: Container(
              width: MediaQuery.of(context).size.width * .5,
              padding: const EdgeInsets.symmetric(vertical: 10),
              margin: const EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                color: MyColors.primary,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(Icons.share, color: MyColors.white),
                  SizedBox(width: 5),
                  MyText(
                    title: tr(context,"share"),
                    color: MyColors.white,
                    size: 17,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
