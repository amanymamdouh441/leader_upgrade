part of 'RegisterImports.dart';

class RegisterData {
  GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<CustomButtonState> btnKey =
      new GlobalKey<CustomButtonState>();
  final GenericBloc<bool> termsCubit = GenericBloc(false);
  final GenericBloc<bool> passwordBloc = new GenericBloc(false);
  final GenericBloc<bool> confirmPasswordBloc = new GenericBloc(false);

  final TextEditingController name = new TextEditingController();
  final TextEditingController invitationCode = new TextEditingController();

  final TextEditingController phone = new TextEditingController();
  final TextEditingController email = new TextEditingController();
  final TextEditingController password = new TextEditingController();
  final TextEditingController confirmPass = new TextEditingController();

  void userRegister(BuildContext context) async {
    FocusScope.of(context).requestFocus(FocusNode());
    FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
    if (formKey.currentState!.validate()) {
      String phoneEn = Utils.convertDigitsToLatin(phone.text);
      if (phoneEn.length != 9) {
        Fluttertoast.showToast(
            msg: tr(context, "phoneValidation"),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: DioUtils.primaryColor,
            textColor: Colors.white,
            fontSize: 16.0);        return;
      }
      if (!termsCubit.state.data) {
        CustomToast.showSimpleToast(msg: tr(context, "acceptTerm"));
        return;
      }
      btnKey.currentState!.animateForward();
      RegisterModel model = RegisterModel(
        phone: phoneEn,
        userName: name.text,
        projectName: "Leader",
        password: password.text,
        InvitationCode: invitationCode.text,
        deviceId: await firebaseMessaging.getToken(),
        deviceType: Platform.isIOS ? "ios" : "android",
        email: email.text,
      );
      await GeneralRepository(context).userRegister(model);
      btnKey.currentState!.animateReverse();
    }
  }
}
