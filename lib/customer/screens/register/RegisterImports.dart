import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/models/Dtos/RegisterModel.dart';
import 'package:leader_upgrade/customer/screens/register/widgets/RegisterWidgetsImports.dart';
import 'package:leader_upgrade/general/blocks/lang_cubit/lang_cubit.dart';
import 'package:leader_upgrade/general/resources/GeneralRepoImports.dart';
import 'package:leader_upgrade/general/screens/login/widgets/LoginWidgetsImports.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:leader_upgrade/general/widgets/AuthScaffold.dart';
import 'package:leader_upgrade/general/widgets/HeaderLogo.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';
import 'package:dio_helper/utils/DioUtils.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/utils/CustomButtonAnimation.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'Register.dart';
part 'RegisterData.dart';