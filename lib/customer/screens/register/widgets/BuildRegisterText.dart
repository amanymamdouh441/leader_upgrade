part of 'RegisterWidgetsImports.dart';

class BuildRegisterText extends StatelessWidget {
  const BuildRegisterText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyText(
      title: tr(context,"register"),
      size: 16,
      color: MyColors.primary,
    );
  }
}
