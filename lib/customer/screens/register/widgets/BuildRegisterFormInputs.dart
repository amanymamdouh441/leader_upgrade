part of 'RegisterWidgetsImports.dart';

class BuildRegisterFormInputs extends StatelessWidget {
  final RegisterData registerData;

  const BuildRegisterFormInputs({required this.registerData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: registerData.formKey,
      child: Column(
        children: [
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label: tr(context, "name"),
            controller: registerData.name,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.text,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey,
          ),
          GenericTextField(
            suffixIcon: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MyText(title: "966+", color: MyColors.black, size: 14),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 7),
                  padding: const EdgeInsets.only(top: 3),
                  child: Image.asset(Res.arabic, scale: 3),
                ),
              ],
            ),
            fieldTypes: FieldTypes.normal,
            label: tr(context, "phone"),
            controller: registerData.phone,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.phone,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey,
          ),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label: tr(context, "mail"),
            controller: registerData.email,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.emailAddress,
            validate: (value) => value!.validateEmail(context),
            enableBorderColor: MyColors.grey,
          ),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label:
                "${tr(context, "invitationCode")} ( ${tr(context, "optional")} )",
            controller: registerData.invitationCode,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.text,
            validate: (value) => value!.noValidate(),
            enableBorderColor: MyColors.grey,
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: registerData.passwordBloc,
            builder: (_, state) {
              return GenericTextField(
                fieldTypes: state.data == true
                    ? FieldTypes.normal
                    : FieldTypes.password,
                label: tr(context, "password"),
                //hintColor: Colors.white70,
                controller: registerData.password,
                validate: (value) => value!.validatePassword(context),
                type: TextInputType.text,
                action: TextInputAction.next,
                margin: const EdgeInsets.symmetric(vertical: 10),
                enableBorderColor: MyColors.grey,
                suffixIcon: IconButton(
                  icon: Icon(
                    state.data ? Icons.visibility : Icons.visibility_off,
                  ),
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    registerData.passwordBloc.onUpdateData(!state.data);
                  },
                ),
              );
            },
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: registerData.confirmPasswordBloc,
            builder: (_, state) {
              return GenericTextField(
                fieldTypes: state.data == true
                    ? FieldTypes.normal
                    : FieldTypes.password,
                label: tr(context, "confirmPassword"),
              //  hintColor: Colors.white70,
                controller: registerData.confirmPass,
                validate: (value) => value!.validatePasswordConfirm(context,
                    pass: registerData.password.text),
                type: TextInputType.text,
                action: TextInputAction.done,
                enableBorderColor: MyColors.grey,
                margin: const EdgeInsets.symmetric(vertical: 10),
                suffixIcon: IconButton(
                  icon: Icon(
                    state.data ? Icons.visibility : Icons.visibility_off,
                  ),
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    registerData.confirmPasswordBloc.onUpdateData(!state.data);
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
