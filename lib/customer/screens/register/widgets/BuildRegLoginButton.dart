part of 'RegisterWidgetsImports.dart';

class BuildRegLoginButton extends StatelessWidget {
  const BuildRegLoginButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultButton(
      title: tr(context, "login"),
      onTap: ()=> AutoRouter.of(context).pop(),
      color: MyColors.white,
      textColor: MyColors.primary,
      borderColor: MyColors.primary,
      margin: const EdgeInsets.symmetric(vertical: 10),
    );
  }
}
