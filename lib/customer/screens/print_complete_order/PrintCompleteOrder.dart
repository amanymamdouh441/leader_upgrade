part of 'PrintCompleteOrderImports.dart';

class PrintCompleteOrder extends StatefulWidget {
  final AddPrintOrderModel addPrintOrderModel;
  final PrintAddressData printAddressData;

  const PrintCompleteOrder(
      {required this.addPrintOrderModel, required this.printAddressData});

  @override
  _PrintCompleteOrderState createState() => _PrintCompleteOrderState();
}

class _PrintCompleteOrderState extends State<PrintCompleteOrder> {
  final PrintCompleteOrderData printCompleteOrderData =
      PrintCompleteOrderData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(title: tr(context, "confirmOrder")),
      body: Column(
        children: [
          Flexible(
            child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              children: [
                BuildCompleteOrdersDetails(
                    addPrintOrderModel: widget.addPrintOrderModel,
                    printCompleteOrderData: printCompleteOrderData,
                    printAddressData: widget.printAddressData),
                BuildOrderDetailsNotes(
                    printCompleteOrderData: printCompleteOrderData,
                    addPrintOrderModel: widget.addPrintOrderModel,
                    printAddressData: widget.printAddressData),
                BuildChoosePayment(
                    printCompleteOrderData: printCompleteOrderData),

              ],
            ),
          ),
          DefaultButton(
            title: tr(context, "confirm"),
            onTap: () => printCompleteOrderData.addPrintOrder(
                context, widget.addPrintOrderModel),
            margin: const EdgeInsets.all(20),
          )
        ],
      ),
    );
  }
}
