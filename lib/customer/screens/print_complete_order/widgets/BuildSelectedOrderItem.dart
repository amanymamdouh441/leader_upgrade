part of 'PrintCompleteOrderWidgetsImports.dart';

class BuildSelectedOrderItem extends StatelessWidget {
  final AddPrintOrderModel addPrintOrderModel;

  const BuildSelectedOrderItem({required this.addPrintOrderModel});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 10, left: 10, bottom: 5),
          child: Divider(thickness: 1),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                    title: addPrintOrderModel.productName ?? "",
                    color: MyColors.blackOpacity,
                    size: 10,
                  ),
                  MyText(
                    title:
                        "${addPrintOrderModel.productPrice} ${tr(context, "sr")}",
                    color: MyColors.blackOpacity,
                    size: 10,
                  ),
                  MyText(
                    title: " ${tr(context, "qty")} : ${addPrintOrderModel.qty}",
                    color: MyColors.blackOpacity,
                    size: 10,
                  ),
                  BuildSelectedSepcItem(
                    name: addPrintOrderModel.subSpecificationNames ?? "",
                  ),
                ],
              ),
              CachedImage(
                url: addPrintOrderModel.productImage ?? "",
                height: 100,
                width: 90,
              )
            ],
          ),
        ),
      ],
    );
  }
}
