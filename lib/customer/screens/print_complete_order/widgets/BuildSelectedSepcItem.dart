part of 'PrintCompleteOrderWidgetsImports.dart';

class BuildSelectedSepcItem extends StatelessWidget {
  final String name;

  const BuildSelectedSepcItem({required this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(3),
          border: Border.all(color: MyColors.black, width: 1)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MyText(
            title: name,
            color: MyColors.black,
            size: 10,
            fontWeight: FontWeight.w700,
          ),
        ],
      ),
    );
  }
}
