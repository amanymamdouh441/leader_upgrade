part of 'PrintCompleteOrderWidgetsImports.dart';

class BuildPaymentMethod extends StatelessWidget {
  final Color textColor;
  final Color borderColor;
  final Color selectedColor;
  final String title;
  final Function() onTap;

  const BuildPaymentMethod(
      {required this.textColor,
      required this.borderColor,
      required this.selectedColor,
      required this.title,
      required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: MediaQuery.of(context).size.width*.4,
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        margin: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: selectedColor,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: borderColor,
          ),
        ),
        alignment: Alignment.center,
        child: MyText(
          title: title,
          color: textColor,
          size: 11,
        ),
      ),
    );
  }
}
