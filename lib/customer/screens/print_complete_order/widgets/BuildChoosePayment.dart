part of 'PrintCompleteOrderWidgetsImports.dart';

class BuildChoosePayment extends StatelessWidget {
  final PrintCompleteOrderData printCompleteOrderData;

  const BuildChoosePayment({required this.printCompleteOrderData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: MyText(
              title: tr(context, "paymentWays"),
              color: MyColors.blackOpacity,
              size: 12,
            ),
          ),
          BlocBuilder<GenericBloc<int>, GenericState<int>>(
            bloc: printCompleteOrderData.selectedCubit,
            builder: (_, state) {
              return Center(
                child: Wrap(
                  children: [
                    BuildPaymentMethod(
                      borderColor: state.data == 1
                          ? MyColors.blackOpacity
                          : MyColors.white,
                      textColor: state.data == 1
                          ? MyColors.white
                          : MyColors.blackOpacity,
                      selectedColor: state.data == 1
                          ? MyColors.primary.withOpacity(0.6)
                          : MyColors.white,
                      title: tr(context, "cash"),
                      onTap: () =>
                          printCompleteOrderData.selectedCubit.onUpdateData(1),
                    ),
                    BuildPaymentMethod(
                      title: tr(context, "online"),
                      borderColor: state.data == 2
                          ? MyColors.blackOpacity
                          : MyColors.white,
                      textColor: state.data == 2
                          ? MyColors.white
                          : MyColors.blackOpacity,
                      selectedColor: state.data == 2
                          ? MyColors.primary.withOpacity(0.6)
                          : MyColors.white,
                      onTap: () =>
                          printCompleteOrderData.selectedCubit.onUpdateData(2),
                    ),
                    BuildPaymentMethod(
                      borderColor: state.data == 3
                          ? MyColors.blackOpacity
                          : MyColors.white,
                      textColor: state.data == 3
                          ? MyColors.white
                          : MyColors.blackOpacity,
                      selectedColor: state.data == 3
                          ? MyColors.primary.withOpacity(0.6)
                          : MyColors.white,
                      title: "محفظه",
                      onTap: () =>
                          printCompleteOrderData.selectedCubit.onUpdateData(3),
                    ),
                  ],
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
