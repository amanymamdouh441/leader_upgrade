part of 'PrintCompleteOrderWidgetsImports.dart';

class BuildCompleteOrdersDetails extends StatelessWidget {
  final AddPrintOrderModel addPrintOrderModel;
  final PrintCompleteOrderData printCompleteOrderData;
  final PrintAddressData printAddressData;

  const BuildCompleteOrdersDetails(
      {required this.addPrintOrderModel,
      required this.printCompleteOrderData,
      required this.printAddressData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
          ]),
      child:
          BlocBuilder<GenericBloc<SettingModel?>, GenericState<SettingModel?>>(
        bloc: printAddressData.orderSettingCubit,
        builder: (_, state) {
          if (state is GenericUpdateState) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                  title:
                      " ${tr(context, "shippingFee")} : ${state.data?.shippingFee} ${tr(context, "sr")} ",
                  color: MyColors.blackOpacity,
                  size: 10,
                ),
                MyText(
                  title:
                      " ${tr(context, "total")} : ${addPrintOrderModel.total! + printAddressData.commissionRatio!} ${tr(context, "includeAddedValue")} ",
                  color: MyColors.blackOpacity,
                  size: 10,
                ),
                BlocBuilder<GenericBloc<CouponModel?>,
                    GenericState<CouponModel?>>(
                  bloc: printCompleteOrderData.couponBloc,
                  builder: (_, state) {
                    if (state.data?.lasttotal != null) {
                      return MyText(
                        title:
                            " ${tr(context, "totalAfterDiscount")} : ${state.data!.lasttotal} ${tr(context, "includeAddedValue")} ",
                        color: MyColors.blackOpacity,
                        size: 10,
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
                BuildSelectedOrderItem(addPrintOrderModel: addPrintOrderModel),
              ],
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
