part of 'PrintCompleteOrderImports.dart';

class PrintCompleteOrderData {
  final GenericBloc<int> selectedCubit = GenericBloc(1);
  final TextEditingController orderDetails = new TextEditingController();
  final TextEditingController coupon = new TextEditingController();
  final GenericBloc<CouponModel?> couponBloc = GenericBloc(null);

  void useCoupon(BuildContext context, num total, num commissionRatio) async {
    var commissionTotal = total + commissionRatio;
    var data = await CustomerRepository(context)
        .useCoupon(coupon.text, commissionTotal);
    if (data != null) {
      couponBloc.onUpdateData(data);
    }
  }

  void addPrintOrder(
      BuildContext context, AddPrintOrderModel addPrintOrderModel) async {
    addPrintOrderModel.coponId = couponBloc.state.data?.coponId;
    addPrintOrderModel.notes = orderDetails.text;
    addPrintOrderModel.typePay = selectedCubit.state.data;
    addPrintOrderModel.total =
        couponBloc.state.data?.lasttotal ?? addPrintOrderModel.total;
    var data =
        await CustomerRepository(context).addPrintOrder(addPrintOrderModel);
    if (data) {
      AutoRouter.of(context).push(SuccessRoute(payment: true));
    }
  }
}
