part of 'WalletWidgetsImports.dart';

class BuildMoneyWallet extends StatelessWidget {
final num money;

  const BuildMoneyWallet({required this.money}) ;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MyText(
          title: tr(context, "currentCharge"),
          color: MyColors.black,
          size: 17,
          fontWeight: FontWeight.bold,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              title: "$money",
              color: MyColors.primary,
              size: 45,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(width: 10),
            MyText(
              title:  tr(context, "sar"),
              color: MyColors.black,
              size: 15,
            ),
            // Column(
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: [
            //
            //     MyText(
            //       title: " سعودي",
            //       color: MyColors.black,
            //       size: 15,
            //     ),
            //   ],
            // ),
          ],
        )
      ],
    );
  }
}
