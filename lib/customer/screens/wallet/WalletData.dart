part of 'WalletImports.dart';

class WalletData {
  final GenericBloc<num> walletCubit = GenericBloc(0);

  void fetchData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getWallet(refresh);
    walletCubit.onUpdateData(data);
  }
}
