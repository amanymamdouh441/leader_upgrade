part of 'WalletImports.dart';

class Wallet extends StatefulWidget {
  const Wallet({Key? key}) : super(key: key);

  @override
  _WalletState createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  final WalletData walletData = WalletData();

  @override
  void initState() {
    walletData.fetchData(context, refresh: false);
    walletData.fetchData(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(
        title: tr(context, "wallet"),
        actions: [NotifyIcon()],
      ),
      body: BlocBuilder<GenericBloc<num>, GenericState<num>>(
        bloc: walletData.walletCubit,
        builder: (_, state) {
          if (state is GenericUpdateState) {
            return ListView(
              children: [
                Image.asset(Res.walletLogo, scale: 3.5),
                BuildMoneyWallet(money: state.data),
              ],
            );
          } else {
            return Center(
              child: LoadingDialog.showLoadingView(),
            );
          }
        },
      ),
    );
  }
}
