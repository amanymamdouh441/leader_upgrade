import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/customer/widgets/widgetsImports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'Locations.dart';
part 'LocationsData.dart';