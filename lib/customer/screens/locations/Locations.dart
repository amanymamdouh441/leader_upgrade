part of 'LocationsImports.dart';

class Locations extends StatefulWidget {
  const Locations({Key? key}) : super(key: key);

  @override
  _LocationsState createState() => _LocationsState();
}

class _LocationsState extends State<Locations> {
  LocationsData locationsData = LocationsData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(title: "العناوين"),
      body: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.only(top: 10),
        children: List.generate(
          5,
          (index) =>
              Container(),
              // BuildLocationItem(),
        ),
      ),
    );
  }
}
