part of 'SearchImports.dart';

class SearchData {
  final TextEditingController search = new TextEditingController();

  final GenericBloc<List<SpecificationModel>> specCubit = GenericBloc([]);
  final PagingController<int, ProductModel> pagingController =
      PagingController(firstPageKey: 1);
  late List<ProductModel> productModel;
  static const _pageSize = 10;

  Future<void> fetchCatProducts(BuildContext context, int pageKey,
      {bool refresh = true}) async {
    var ids = specCubit.state.data
        .where((e) => e.selected.isNotEmpty)
        .map((e) => e.selected)
        .toList();
    FilterModel model = FilterModel(
      refresh: refresh,
      categoryId: 0,
      currentPage: pageKey,
      subIds: ids.expand((pair) => pair).toList(),
      searchTxt: search.text,
    );
    print(">>>>>>>>>>>>>>>${model.currentPage}");
    productModel = await CustomerRepository(context).getCatProducts(model);
    final isLastPage = productModel.length < _pageSize;
    if (pageKey == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(productModel);
    } else {
      final nextPageKey = pageKey + 1;
      pagingController.appendPage(productModel, nextPageKey);
    }
  }
}
