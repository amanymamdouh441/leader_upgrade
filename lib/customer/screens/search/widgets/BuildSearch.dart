part of 'SearchWidgetsImports.dart';

class BuildSearch extends StatelessWidget {
  final SearchData searchData;

  const BuildSearch({required this.searchData});

  @override
  Widget build(BuildContext context) {
    return GenericTextField(
      hint: tr(context, "searchForProd"),
      controller: searchData.search,
      prefixIcon: InkWell(
        onTap: () => searchData.pagingController.refresh(),
        child: Icon(
          Icons.search_rounded,
          size: 25,
          color: MyColors.primary,
        ),
      ),
      fieldTypes: FieldTypes.normal,
      type: TextInputType.text,
      margin: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
      enableBorderColor: MyColors.primary,
      action: TextInputAction.search,
      onSubmit: () => searchData.pagingController.refresh(),
      validate: (value) => value!.noValidate(),
    );
  }
}
