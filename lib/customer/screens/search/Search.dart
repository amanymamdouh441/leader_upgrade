part of 'SearchImports.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  final SearchData searchData = new SearchData();

  @override
  void initState() {
    searchData.fetchCatProducts(context, 1);
    searchData.pagingController.addPageRequestListener((pageKey) {
      searchData.fetchCatProducts(context, pageKey,refresh: true);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: tr(context,"search")),
      body: Column(
        children: [
          BuildSearch(searchData: searchData),
          Flexible(
            child: RefreshIndicator(
              onRefresh: ()=>searchData.fetchCatProducts(context, 1),
              child: PagedGridView<int, ProductModel>(
                pagingController: searchData.pagingController,
                padding: EdgeInsets.symmetric(horizontal: 10),
                gridDelegate: GridFixedHeightDelegate(
                  crossAxisCount: 3,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 5,
                  height: 180,
                ),
                builderDelegate: PagedChildBuilderDelegate<ProductModel>(
                  noItemsFoundIndicatorBuilder: (context) => const BuildNoItemFound(
                    title: "noProducts",
                    message: "tryAgain",
                  ),
                  firstPageProgressIndicatorBuilder: (_){
                    return LoadingDialog.showLoadingView();
                  },
                  itemBuilder: (context, item, index) => BuildProductItem(
                    model: item,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
