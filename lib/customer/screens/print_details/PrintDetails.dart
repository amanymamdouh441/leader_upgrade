part of 'PrintDetailsImports.dart';

class PrintDetails extends StatefulWidget {
  final DropDownModel model;
  const PrintDetails({required this.model});

  @override
  _PrintDetailsState createState() => _PrintDetailsState();
}

class _PrintDetailsState extends State<PrintDetails> {
  final PrintDetailsData printDetailsData = PrintDetailsData();

  @override
  void initState() {
    printDetailsData.fetchCatProducts(context, 1, widget.model.id,
        refresh: true);
    printDetailsData.pagingController.addPageRequestListener((pageKey) {
      printDetailsData.fetchCatProducts(context, pageKey, widget.model.id);
      print(">>>>>>>>>>>>>>>>>>>>");
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: widget.model.name,
        actions: [
          CartIcon(),
        ],
      ),
      body: BuildPrintCategoryView(
        printDetailsData: printDetailsData,
        catId: widget.model.id,
      ),
    );
  }
}
