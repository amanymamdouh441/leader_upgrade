part of 'PrintDetailsWidgetsImports.dart';

class BuildPrintCategoryView extends StatelessWidget {
  final PrintDetailsData printDetailsData;
  final int catId;

  const BuildPrintCategoryView(
      {required this.printDetailsData, required this.catId});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => printDetailsData.fetchCatProducts(context, 1, catId),
      child: PagedGridView<int, ProductModel>(
        pagingController: printDetailsData.pagingController,
        padding: EdgeInsets.symmetric(vertical: 10),
        gridDelegate: GridFixedHeightDelegate(
          crossAxisCount: 3,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          height: 200,
        ),
        builderDelegate: PagedChildBuilderDelegate<ProductModel>(
          noItemsFoundIndicatorBuilder: (context) => const BuildNoItemFound(
            title: "noProducts",
            message: "tryAgain",
          ),
          firstPageProgressIndicatorBuilder: (_) {
            return LoadingDialog.showLoadingView();
          },
          itemBuilder: (context, item, index) => BuildPrintItem(
            model: item,
            onClosed: printDetailsData.onFavChanged,
            catId: catId,
          ),
        ),
      ),
    );
  }
}
