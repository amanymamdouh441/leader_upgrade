part of 'PrintDetailsImports.dart';

class PrintDetailsData {
  final PagingController<int, ProductModel> pagingController =
      PagingController(firstPageKey: 1);
  static const _pageSize = 10;

  Future<void> fetchCatProducts(BuildContext context, int pageKey, int catId,
      {bool refresh = true}) async {
    var data = await CustomerRepository(context)
        .getPrintDetails(catId, pageKey, refresh);
    if (pageKey == 1) {
      pagingController.itemList = [];
    }
    final isLastPage = data.length < _pageSize;
    if (isLastPage) {
      pagingController.appendLastPage(data);
    } else {
      final nextPageKey = pageKey + data.length;
      pagingController.appendPage(data, nextPageKey);
    }
  }

  onFavChanged(ProductModel? model) {
    if (model != null) {
      int index =
          pagingController.itemList!.indexWhere((e) => e.id == model.id);
      pagingController.itemList![index] = model;
      var data = pagingController.itemList;
      pagingController.itemList = [];
      pagingController.itemList = data;
    }
  }
}
