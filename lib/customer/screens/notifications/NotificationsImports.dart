
import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/models/Dtos/NotificationEnum.dart';
import 'package:leader_upgrade/customer/models/notify_model.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/customer/screens/notifications/widgets/NotificationsWidgetsImports.dart';
import 'package:leader_upgrade/general/blocks/notify_count_cubit/notify_count_cubit.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'Notifications.dart';
part 'NotificationsData.dart';