part of 'NotificationsImports.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key? key}) : super(key: key);

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  final NotificationsData notificationsData = NotificationsData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(title: tr(context,"notifications")),
      body: GenericListView(
        type: ListViewType.api,
        cubit: notificationsData.notifyCubit,
        onRefresh: notificationsData.fetchData,
        params: [context],
        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 0),
        emptyStr: tr(context, "noData"),
        itemBuilder: (_, index, item) {
          return BuildNotifyItem(
            notifyModel: item,
            notificationsData: notificationsData,
          );
        },
      ),
    );
  }
}
