import 'package:leader_upgrade/customer/models/notify_model.dart';
import 'package:leader_upgrade/customer/screens/notifications/NotificationsImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildNotifyItem.dart';