part of 'NotificationsWidgetsImports.dart';

class BuildNotifyItem extends StatelessWidget {
  final NotifyModel notifyModel;
  final NotificationsData notificationsData;

  const BuildNotifyItem(
      {required this.notifyModel, required this.notificationsData});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => notificationsData.navigate(
        context,
        notifyModel.type,
        notifyModel.orderId,
      ),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 7),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: MyColors.grey.withOpacity(0.8)),
            boxShadow: [
              BoxShadow(
                color: MyColors.greyWhite,
                spreadRadius: 2,
                blurRadius: 2,
              ),
            ]),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              width: 55,
              height: 55,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: MyColors.greyWhite),
                boxShadow: [
                  BoxShadow(
                    color: MyColors.white,
                    blurRadius: .5,
                  ),
                ],
              ),
              child: Image.asset(Res.logo),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: MyText(
                  title: notifyModel.text,
                  color: MyColors.blackOpacity,
                  size: 10,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            MyText(
              title: notifyModel.date,
              color: MyColors.grey,
              size: 10,
              fontWeight: FontWeight.w600,
            )
          ],
        ),
      ),
    );
  }
}
