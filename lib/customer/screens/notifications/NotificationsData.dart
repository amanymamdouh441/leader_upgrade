part of 'NotificationsImports.dart';

class NotificationsData {
  final GenericBloc<List<NotifyModel>> notifyCubit = GenericBloc([]);

  fetchData(BuildContext context, {bool refresh = true}) async {
    final data = await CustomerRepository(context).getNotify(refresh);
    notifyCubit.onUpdateData(data);
    context.read<NotifyCountCubit>().onUpdateCount(0);
  }

  void navigate(BuildContext context, int type, int orderId) {
    if (type == NotificationType.Order.getValue()) {

      AutoRouter.of(context).push(OrderDetailsRoute(orderId: orderId, ));
    }
  }
}
