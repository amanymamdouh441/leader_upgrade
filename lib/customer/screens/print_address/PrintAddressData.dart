part of 'PrintAddressImports.dart';

class PrintAddressData {
  final LocationCubit locationCubit = new LocationCubit();
  final GlobalKey<FormState> formKey = new GlobalKey();
  final TextEditingController addressDetails = new TextEditingController();
  final GenericBloc<SettingModel?> orderSettingCubit = GenericBloc(null);
  num? commissionRatio;
  final GlobalKey<DropdownSearchState> returnId = new GlobalKey();

  String? cityName;

  void onSelectReturn(String cityName) {
    this.cityName = cityName;
  }

  void getShippingFee(
      BuildContext context, AddPrintOrderModel addPrintOrderModel,
      {bool refresh = true}) async {
    var data = await CustomerRepository(context).getOrderSetting(refresh);
    orderSettingCubit.onUpdateData(data);
    commissionRatio =
        (addPrintOrderModel.total! * orderSettingCubit.state.data!.commission) /
            100;
  }

  onLocationClick(BuildContext context) async {
    await Geolocator.requestPermission();
    var _loc = await Utils.getCurrentLocation();
    locationCubit.onLocationUpdated(LocationModel(
      lat: _loc?.latitude ?? 24.774265,
      lng: _loc?.longitude ?? 46.738586,
      address: "",
    ));
    Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (cxt) => BlocProvider.value(
          value: locationCubit,
          child: LocationAddress(),
        ),
      ),
    );
  }

  void navigateToCompleteOrder(BuildContext context, AddPrintOrderModel model,
      PrintAddressData printAddressData) {
    if (
    // formKey.currentState!.validate()&&
        returnId.currentState!.getSelectedItem != null) {
      if (locationCubit.state.model?.address == "" ) {
        CustomToast.showSimpleToast(msg: tr(context, "selectLocation"));
        return;
      }
      if(cityName == null){
        CustomToast.showSimpleToast(msg: tr(context, 'chooseCity'));
        return;
      }
      model.lat = locationCubit.state.model?.lat.toString();
      model.lng = locationCubit.state.model?.lng.toString();
      model.cityName = cityName;
      model.location = locationCubit.state.model?.address;
      model.addressDetails = addressDetails.text;
      model.shippingFee = orderSettingCubit.state.data?.shippingFee;
      AutoRouter.of(context).push(PrintCompleteOrderRoute(
          addPrintOrderModel: model, printAddressData: printAddressData));
    }
  }
}
