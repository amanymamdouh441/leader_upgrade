part of 'PrintAddressWidgetsImports.dart';

class BuildChoosePrintLocation extends StatelessWidget {
  final PrintAddressData printAddressData;

  const BuildChoosePrintLocation({required this.printAddressData});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationCubit, LocationState>(
      bloc: printAddressData.locationCubit,
      builder: (context, state) {
        if (state.model?.address == "") {
          return InkWell(
            onTap: () => printAddressData.onLocationClick(context),
            child: Container(
              height: 100,
              margin: const EdgeInsets.all(10),
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: MyColors.greyWhite,
                        spreadRadius: 2,
                        blurRadius: 2)
                  ]),
              alignment: Alignment.center,
              child: MyText(
                title: tr(context, "selectLocation"),
                color: MyColors.blackOpacity,
                size: 11,
              ),
            ),
          );
        } else {
          return BuildPrintLocationItem(
            locationModel: state.model!,
            addressData: printAddressData,
          );
        }
      },
    );
  }
}
