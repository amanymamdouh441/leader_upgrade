part of 'PrintAddressWidgetsImports.dart';

class BuildPrintLocationDetails extends StatelessWidget {
final PrintAddressData printAddressData;

  const BuildPrintLocationDetails({required this.printAddressData}) ;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: printAddressData.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.all(10),
            child: MyText(
              title: tr(context, "locationDetails"),
              color: MyColors.blackOpacity,
              size: 12,
            ),
          ),
          GenericTextField(
            fieldTypes: FieldTypes.rich,
            controller: printAddressData.addressDetails,
            type: TextInputType.text,
            action: TextInputAction.done,
            validate: (value) => value!.validateEmpty(context),
            max: 5,
            hint: tr(context, "writeDetails"),
            margin: const EdgeInsets.symmetric(horizontal: 10),
          )
        ],
      ),
    );
  }
}
