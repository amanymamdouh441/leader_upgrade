part of 'PrintAddressImports.dart';

class PrintAddress extends StatefulWidget {
  final AddPrintOrderModel addPrintOrderModel;

  const PrintAddress({required this.addPrintOrderModel});

  @override
  _PrintAddressState createState() => _PrintAddressState();
}

class _PrintAddressState extends State<PrintAddress> {
  final PrintAddressData printAddressData = PrintAddressData();

  @override
  void initState() {
    print(printAddressData.cityName.toString() + 'ghssdhd');
    printAddressData.getShippingFee(context, widget.addPrintOrderModel,
        refresh: false);
    printAddressData.getShippingFee(context, widget.addPrintOrderModel);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(title: tr(context, "deliverLocation")),
      body: Column(
        children: [
          Flexible(
            child: GenericListView(
              type: ListViewType.normal,
              padding: const EdgeInsets.all(10),
              children: [
                BuildChoosePrintLocation(printAddressData: printAddressData),
                BuildAramexCities(
                  cityName:
                      printAddressData.cityName ?? tr(context, 'chooseCity'),
                  dropKey: printAddressData.returnId,
                  onChange: printAddressData.onSelectReturn,
                ),
                // BuildPrintLocationDetails(printAddressData: printAddressData),
              ],
            ),
          ),
          DefaultButton(
            title: tr(context, "confirm"),
            margin: const EdgeInsets.all(20),
            onTap: () => printAddressData.navigateToCompleteOrder(
              context,
              widget.addPrintOrderModel,
              printAddressData,
            ),
          ),
        ],
      ),
    );
  }
}
