import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/screens/home/HomeImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'Success.dart';
part 'SuccessData.dart';
