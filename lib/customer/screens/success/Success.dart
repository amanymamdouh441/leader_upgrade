part of 'SuccessImports.dart';

class Success extends StatefulWidget {
  final bool payment;

  const Success({required this.payment});

  @override
  _SuccessState createState() => _SuccessState();
}

class _SuccessState extends State<Success> {
  final SuccessData successData = new SuccessData();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: successData.onBackPressed,
      child: Scaffold(
        body: Visibility(
          visible: widget.payment,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  Res.sucesspay,
                  scale: 4,
                ),
                Flexible(
                  child: Container(
                    margin: const EdgeInsets.all(20),
                    child: MyText(
                      alien: TextAlign.center,
                      title: tr(context, "successOrder"),
                      color: MyColors.black,
                      size: 14,
                    ),
                  ),
                ),
              ],
            ),
          ),
          replacement: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  Res.success,
                  scale: 4,
                ),
                Flexible(
                  child: Container(
                    margin: const EdgeInsets.all(20),
                    child: MyText(
                      alien: TextAlign.center,
                      title: tr(context, "orderSend"),
                      color: MyColors.black,
                      size: 14,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: DefaultButton(
          title: tr(context, "backHome"),
          margin: const EdgeInsets.all(20),
          onTap: () => AutoRouter.of(context).push(HomeRoute()),
        ),
      ),
    );
  }
}
