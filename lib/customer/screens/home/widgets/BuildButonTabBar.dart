part of 'HomeWidgetsImports.dart';

class BuildBottomTabBar extends StatelessWidget {
  final HomeData homeData;

  const BuildBottomTabBar({required this.homeData});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75,
      decoration: BoxDecoration(
        color: MyColors.primary,
        boxShadow: [
          BoxShadow(color: MyColors.greyWhite, blurRadius: 5, spreadRadius: 5)
        ],
      ),
      alignment: Alignment.topCenter,
      child: TabBar(
        onTap: (index) => homeData.onChangePage(index, context),
        unselectedLabelColor: Colors.white54,
        unselectedLabelStyle: GoogleFonts.cairo(fontSize: 11),
        labelStyle:
            GoogleFonts.cairo(fontSize: 12, fontWeight: FontWeight.bold),
        labelColor: MyColors.white,
        indicatorColor: MyColors.primary,
        tabs: [
          BuildTabItem(
            title: tr(context, "leader"),
            image: Res.logonav,
          ),
          BuildTabItem(
            title: tr(context, "orders"),
            image: Res.orders,
          ),
          BuildTabItem(
            title: tr(context, "fav"),
            image: Res.fav,
          ),
          BuildTabItem(
            title: tr(context, "profile"),
            image: Res.profile,
          ),
        ],
      ),
    );
  }
}
