part of 'HomeWidgetsImports.dart';

class BuildTabBarPages extends StatelessWidget {
  final HomeData homeData;

  const BuildTabBarPages({required this.homeData});

  @override
  Widget build(BuildContext context) {
    return TabBarView(
      physics: NeverScrollableScrollPhysics(),
      children: [
        Leader(),
        MyOrders(index: 0),
        Favorite(),
        Profile(),
      ],
    );
  }
}
