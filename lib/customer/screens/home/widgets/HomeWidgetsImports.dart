import 'package:leader_upgrade/customer/screens/home/HomeImports.dart';
import 'package:leader_upgrade/customer/screens/home/tabs/favorite/FavoriteImports.dart';
import 'package:leader_upgrade/customer/screens/home/tabs/leader/LeaderImports.dart';
import 'package:leader_upgrade/customer/screens/home/tabs/profile/ProfileImports.dart';
import 'package:leader_upgrade/customer/screens/home/tabs/shop/ShopImports.dart';
import 'package:leader_upgrade/customer/screens/my_orders/MyOrdersImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';


part 'BuildTabItem.dart';
part 'BuildButonTabBar.dart';
part 'BuildTabBarPages.dart';
