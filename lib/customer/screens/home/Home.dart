part of 'HomeImports.dart';

class Home extends StatefulWidget {
  final int? index;

  const Home({this.index});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  final HomeData homeData = HomeData();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: homeData.onBackPressed,
      child: DefaultTabController(
        length: 4,
        initialIndex: widget.index ?? 0,
        child: Scaffold(
          body: BuildTabBarPages(homeData: homeData),
          bottomNavigationBar: BuildBottomTabBar(homeData: homeData),
        ),
      ),
    );
  }
}
