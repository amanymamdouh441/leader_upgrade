part of 'HomeImports.dart';

class HomeData {
  Future<bool> onBackPressed() async {
    SystemNavigator.pop();
    return true;
  }

  void onChangePage(int index, BuildContext context) {
    bool result = context.read<AuthCubit>().state.authorized;
    if (!result) {
      if (index == 1 || index == 2) {
        CustomToast.showAuthDialog(context: context);
        return;
      }
    }
  }
}
