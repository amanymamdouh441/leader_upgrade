
import 'package:animations/animations.dart';
import 'package:auto_route/auto_route.dart';

import 'package:leader_upgrade/customer/screens/home/tabs/profile/ProfileImports.dart';
import 'package:leader_upgrade/customer/screens/languages/LanguagesImports.dart';
 import 'package:leader_upgrade/customer/screens/user_addresses/UserAddressesImports.dart';
import 'package:leader_upgrade/customer/screens/wallet/WalletImports.dart';
import 'package:leader_upgrade/customer/screens/invitation/InvitationImports.dart';

 import 'package:leader_upgrade/customer/screens/my_orders/MyOrdersImports.dart';
import 'package:leader_upgrade/customer/screens/profile_settings/ProfileSettingsImports.dart';
import 'package:leader_upgrade/general/blocks/auth_cubit/auth_cubit.dart';
 import 'package:leader_upgrade/general/blocks/user_cubit/user_cubit.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/resources/GeneralRepoImports.dart';
import 'package:leader_upgrade/general/screens/about/AboutImports.dart';
import 'package:leader_upgrade/general/screens/contact_us/ContactUsImports.dart';
import 'package:leader_upgrade/general/screens/login/LoginImports.dart';
 import 'package:leader_upgrade/general/screens/terms/TermsImports.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/customer/widgets/widgetsImports.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'BuildProfileAppBar.dart';
part 'BuildMyOrders.dart';
part 'BuildOrderItem.dart';
part 'BuildOthers.dart';
part 'BuildOthersItem.dart';
part 'BuildShare.dart';
part 'BuildOthersItemLogIn.dart';
part 'BuildOthersItemLogOut.dart';

