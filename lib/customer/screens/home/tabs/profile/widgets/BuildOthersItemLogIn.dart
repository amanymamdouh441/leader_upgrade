part of 'ProfileWidgetsImports.dart';

class BuildOthersItemLogIn extends StatelessWidget {
  final Icon icon;
  final String title;
  final Widget? page;
  final bool? showDivider;

  const BuildOthersItemLogIn(
      {required this.icon, required this.title, this.page, this.showDivider});

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      openElevation: 0,
      closedColor: Colors.transparent,
      openColor: Colors.white,
      middleColor: Colors.transparent,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (context, action) => page??Container(),
      closedBuilder: (context, action) => Container(
        margin: const EdgeInsets.only(top: 10),
        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 2),
        child: Column(
          children: [
            Row(
              children: [
                icon,
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child: MyText(
                      title: title,
                      color: MyColors.black,
                      size: 12,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Icon(Icons.arrow_forward_ios, size: 15)
              ],
            ),
            Visibility(
              visible: showDivider?? true,
              child: Divider(thickness: 1),
            )
          ],
        ),
      ),
    );
  }
}
