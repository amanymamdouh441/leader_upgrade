part of 'ProfileWidgetsImports.dart';

class BuildProfileAppBar extends PreferredSize {
  final Size preferredSize;

  BuildProfileAppBar({
    this.preferredSize = const Size.fromHeight(kToolbarHeight + 10),
  }) : super(child: Container(), preferredSize: preferredSize);

  @override
  Widget build(BuildContext context) {
    var user = context.watch<UserCubit>().state.model;
    return Visibility(
      visible: context.read<AuthCubit>().state.authorized,
      child: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: MyColors.white,
        toolbarHeight: 90,
        title: Row(
          children: [
            CachedImage(
              height: 50,
              width: 50,
              url: user?.imgProfile ?? "",
              borderRadius: BorderRadius.circular(10),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      title: "${tr(context,"hello")} ${user?.userName}",
                      color: MyColors.black,
                      size: 12,
                    ),
                    MyText(
                      title: user?.phone ?? "",
                      color: MyColors.blackOpacity,
                      size: 11,
                      fontWeight: FontWeight.w600,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        actions: [
          OpenContainer(
            closedElevation: 0,
            openElevation: 0,
            closedColor: Colors.transparent,
            openColor: Colors.white,
            middleColor: Colors.transparent,
            transitionDuration: Duration(milliseconds: 800),
            transitionType: ContainerTransitionType.fadeThrough,
            openBuilder: (context, action) => ProfileSettings(),
            closedBuilder: (context, action) => Container(
              margin: const EdgeInsets.symmetric(horizontal: 5),
              child: Image.asset(Res.settings, width: 27),
            ),
          ),
          CartIcon(),
        ],
      ),
      replacement: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: MyColors.white,
        toolbarHeight: 90,
        centerTitle: false,
        title: MyText(
          title: tr(context,"mySetting"),
          color: MyColors.black,
          size: 16,
        ),
        /* actions: [
          IconButton(
            onPressed: () => AutoRouter.of(context).push(LoginRoute()),
            icon: Icon(
              Icons.login,
              size: 30,
            ),
          )
        ], */
      ),
    );
  }
}
