part of 'ProfileWidgetsImports.dart';

class BuildOrderItem extends StatelessWidget {
  final String image;
  final String title;
  final Widget? page;
  const BuildOrderItem({required this.image,required this.title, this.page});

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      openElevation: 0,
      closedColor: Colors.transparent,
      openColor: Colors.white,
      middleColor: Colors.transparent,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (context, action) =>page??Container(),
      closedBuilder: (context, action) => Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          children: [
            Image.asset(image, height: 25),
            MyText(
              title: title,
              color: MyColors.blackOpacity,
              size: 10,
            )
          ],
        ),
      ),
    );
  }
}
