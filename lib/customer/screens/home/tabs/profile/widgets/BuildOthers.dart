part of 'ProfileWidgetsImports.dart';

class BuildOthers extends StatelessWidget {
  final ProfileData profileData;

  const BuildOthers({required this.profileData});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: MyColors.white,
        boxShadow: [
          BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Visibility(
            visible: context.read<AuthCubit>().state.authorized,
            child: Column(
              children: [
                BuildOthersItem(
                  image: Res.location,
                  title: tr(context, "myAddresses"),
                  page: UserAddresses(),
                ),

              ],
            ),
          ),
          Visibility(
            visible: context.read<AuthCubit>().state.authorized,
            child: Column(
              children: [
                BuildOthersItem(
                  image: Res.wallet,
                  title: tr(context, "wallet"),
                  page: Wallet(),
                ),
                BuildOthersItem(
                  image: Res.invitation,
                  title: tr(context, "invitationCode"),
                  page: Invitation(),
                ),
              ],
            ),
          ),
          BuildOthersItem(
            image: Res.language,
            title: tr(context, "lang"),
            page: Languages(),
          ),
          BuildOthersItem(
            image: Res.about,
            title: tr(context, "about"),
            page: About(),
          ),
          BuildOthersItem(
            image: Res.rules,
            title: tr(context, "conditions"),
            page: Terms(),
          ),
          BuildOthersItem(
            image: Res.call,
            title: tr(context, "contactUs"),
            page: ContactUs(),
          ),
          BuildShare(profileData: profileData),
          context.read<AuthCubit>().state.authorized
              ? BuildOthersItemLogOut(
                  image: Res.logout,
                  showDivider: false,
                  title: tr(context, "logout"),
                  page: Login(),
                )
              : BuildOthersItemLogIn(
                  icon: Icon(Icons.login),
                  showDivider: false,
                  title: tr(context, "login"),
                  page: Login(),
                ),

        ],
      ),
    );
  }
}
