part of 'ProfileWidgetsImports.dart';

class BuildMyOrders extends StatelessWidget {
  const BuildMyOrders({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: context.read<AuthCubit>().state.authorized,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: MyColors.white,
          boxShadow: [
            BoxShadow(
                color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2),
          ],
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  title: tr(context,"orders"),
                  color: MyColors.black,
                  size: 12,
                ),
                InkWell(
                  onTap: () =>
                      AutoRouter.of(context).push(MyOrdersRoute(index: 0)),
                  child: MyText(
                    title:tr(context, "displayAll"),
                    color: MyColors.blackOpacity,
                    size: 11,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                BuildOrderItem(
                  title: tr(context, "new"),
                  image: Res.neworder,
                  page: MyOrders(index: 1),
                ),
                BuildOrderItem(
                  title: tr(context, "inProgress"),
                  image: Res.inprogressorder,
                  page: MyOrders(index: 2),
                ),
                BuildOrderItem(
                  title: tr(context, "onDelivery"),
                  image: Res.shippingorder,
                  page: MyOrders(index: 3),
                ),
                BuildOrderItem(
                  title: tr(context, "finished"),
                  image: Res.endorder,
                  page: MyOrders(index: 4),
                ),
                BuildOrderItem(
                  title: tr(context, "returned"),
                  image: Res.returns,
                  page: MyOrders(index: 5),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
