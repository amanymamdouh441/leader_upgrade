part of 'ProfileWidgetsImports.dart';

class BuildShare extends StatelessWidget {
  final ProfileData profileData;

  const BuildShare({required this.profileData});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => profileData.shareApp(context),
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
        child: Column(
          children: [
            Row(
              children: [
                Image.asset(Res.share, height: 20),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child: MyText(
                      title: tr(context, "shareApp"),
                      color: MyColors.black,
                      size: 12,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Icon(Icons.arrow_forward_ios, size: 15)
              ],
            ),
            Visibility(
              visible: true,
              child: Divider(thickness: 1),
            )
          ],
        ),
      ),
    );
  }
}
