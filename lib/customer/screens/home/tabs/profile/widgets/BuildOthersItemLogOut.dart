part of 'ProfileWidgetsImports.dart';

class BuildOthersItemLogOut extends StatelessWidget {
  final String image;
  final String title;
  final Widget? page;
  final bool? showDivider;

  const BuildOthersItemLogOut(
      {required this.image, required this.title, this.page, this.showDivider});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>GeneralRepository(context).logout(),
      child: Container(
          margin: const EdgeInsets.only(top: 10),
          padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 2),
          child: Column(
            children: [
              Row(
                children: [
                  Image.asset(image, height: 20),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: MyText(
                        title: title,
                        color: MyColors.black,
                        size: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Icon(Icons.arrow_forward_ios, size: 15)
                ],
              ),
              Visibility(
                visible: showDivider?? true,
                child: Divider(thickness: 1),
              )
            ],
          ),
        ),
    );
  }
}
