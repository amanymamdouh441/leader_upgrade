part of 'ProfileImports.dart';

class ProfileData {

  void shareApp(BuildContext context) {
    var share = context.read<ShareCubit>().state;

    if (Platform.isIOS) {
      Utils.shareApp(share.ios);
    } else {
      Utils.shareApp(share.android);
    }
  }
}