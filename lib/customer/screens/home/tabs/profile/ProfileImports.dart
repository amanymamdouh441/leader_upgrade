import 'dart:io';

import 'package:leader_upgrade/customer/screens/home/tabs/profile/widgets/ProfileWidgetsImports.dart';
import 'package:leader_upgrade/general/blocks/share_cubit/share_cubit.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'Profile.dart';
part 'ProfileData.dart';