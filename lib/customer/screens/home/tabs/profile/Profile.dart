part of 'ProfileImports.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final ProfileData profileData = ProfileData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: BuildProfileAppBar(),
      body: GenericListView(
        padding: const EdgeInsets.symmetric(vertical: 20),
        type: ListViewType.normal,
        children: [
          BuildOthers(profileData: profileData),
        ],
      ),
    );
  }
}
