part of 'LeaderImports.dart';

class Leader extends StatefulWidget {
  const Leader({Key? key}) : super(key: key);

  @override
  _LeaderState createState() => _LeaderState();
}

class _LeaderState extends State<Leader> {

  @override
  Widget build(BuildContext context) {
    var cats = context.watch<CatsCubit>().state.cats;
    return DefaultTabController(
      length: cats.length,
      child: Scaffold(
        appBar: BuildLeaderAppBar(),
        body: Column(
          children: [
            BuildLeaderTabs(cats: cats),
            Flexible(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  children: List.generate(
                    cats.length,
                    (index) => BuildLeaderTabView(
                      category: cats[index],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
