part of 'LeaderWidgetsImports.dart';

class BuildPrintItem extends StatelessWidget {
  final DropDownModel dropDownModel;

  const BuildPrintItem({required this.dropDownModel});

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      openElevation: 0,
      closedColor: Colors.transparent,
      openColor: Colors.white,
      middleColor: Colors.transparent,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (context, action) =>
          PrintDetails(model: dropDownModel),
      closedBuilder: (context, action) => Container(
        decoration: BoxDecoration(
            color: MyColors.secondary, borderRadius: BorderRadius.circular(3)),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        alignment: Alignment.center,
        child: MyText(
          title: dropDownModel.name,
          color: MyColors.black,
          size: 12,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
