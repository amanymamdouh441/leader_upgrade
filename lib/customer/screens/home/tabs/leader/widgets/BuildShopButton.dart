part of 'LeaderWidgetsImports.dart';

class BuildShopButton extends StatelessWidget {
  const BuildShopButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=> AutoRouter.of(context).push(HomeRoute(index: 1)),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primary,
            borderRadius: BorderRadius.circular(10)
        ),
        child: MyText(
          title: tr(context, "shopNow"),
          size: 10,
          color: MyColors.white,
        ),
      ),
    );
  }
}
