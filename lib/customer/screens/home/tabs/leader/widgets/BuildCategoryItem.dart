part of 'LeaderWidgetsImports.dart';

class BuildCategoryItem extends StatelessWidget {
  final SubCategory subCategory;

  const BuildCategoryItem({Key? key, required this.subCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      openElevation: 0,
      closedColor: Colors.transparent,
      openColor: Colors.white,
      middleColor: Colors.transparent,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (context, action) => CategoryDetails(model: subCategory),
      closedBuilder: (context, action) => Container(
        decoration: BoxDecoration(
            color: MyColors.secondary, borderRadius: BorderRadius.circular(3)),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CachedImage(
              url: subCategory.img,
              height: 25,
              width: 25,
              bgColor: Colors.transparent,
              fit: BoxFit.fill,
            ),
            SizedBox(width: 10),
            Expanded(
              child: MyText(
                title: subCategory.name,
                color: MyColors.black,
                overflow: TextOverflow.clip,
                size: 12,
                fontWeight: FontWeight.w600,
              ),
            )
          ],
        ),
      ),
    );
  }
}
