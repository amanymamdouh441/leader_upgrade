part of 'LeaderWidgetsImports.dart';

class BuildShopping extends StatelessWidget {
  final SubCategory model;
  const BuildShopping({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: CachedImage(
            height: 250,
            url: model.img,
            alignment: Alignment.bottomCenter,
            fit: BoxFit.cover,
            placeHolder: SvgPicture.asset(Res.noImage),
            child: Container(
              height: 75,
              width: MediaQuery.of(context).size.width,
              color: MyColors.secondary.withOpacity(0.8),
              child: Column(
                children: [
                  MyText(
                    title: model.name,
                    size: 12,
                    color: MyColors.black,
                  ),
                  BuildShopButton()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
