part of 'LeaderWidgetsImports.dart';

class BuildNewArrivals extends StatelessWidget {
  final List<ProductModel> products;

  const BuildNewArrivals({Key? key, required this.products}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MyText(title: tr(context, "newArrive"), color: MyColors.black, size: 13),
          Container(
            height: 180,
            margin: const EdgeInsets.symmetric(vertical: 10),
            alignment: Alignment.center,
            child: Visibility(
              visible: products.length > 0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: products.length,
                itemBuilder: (_, index) {
                  return BuildProductItem(
                    showButtons: false,
                    model: products[index],
                  );
                },
              ),
              replacement: MyText(
                title: tr(context,"waitAddingProds"),
                color: MyColors.primary,
                size: 14,
                alien: TextAlign.center,
              ),
            ),
          )
        ],
      ),
    );
  }
}
