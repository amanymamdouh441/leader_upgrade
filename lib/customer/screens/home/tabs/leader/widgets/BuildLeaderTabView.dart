part of 'LeaderWidgetsImports.dart';

class BuildLeaderTabView extends StatefulWidget {
  final Category category;

  const BuildLeaderTabView({required this.category});

  @override
  State<BuildLeaderTabView> createState() => _BuildLeaderTabViewState();
}

class _BuildLeaderTabViewState extends State<BuildLeaderTabView> {
  final LeaderData leaderData = LeaderData();

  @override
  void initState() {
    leaderData.fetchHomeData(context, widget.category.id, refresh: false);
    leaderData.fetchHomeData(context, widget.category.id);
    if (widget.category.id == 0) {
      leaderData.fetchPrintCategories(context, refresh: false);
      leaderData.fetchPrintCategories(context);
    } else {
      leaderData.fetchSubCategories(context, widget.category.id,
          refresh: false);
      leaderData.fetchSubCategories(context, widget.category.id);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<HomeModel?>, GenericState<HomeModel?>>(
      bloc: leaderData.homeCubit,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return Container(
            child: GenericListView(
              type: ListViewType.normal,
              padding: const EdgeInsets.symmetric(vertical: 10),
              children: [
                BuildSlider(
                  slider: state.data!.sliders,
                  leaderData: leaderData,
                ),
                // BuildNewArrivals(products: state.data!.products,),
                BuildLeaderCategories(
                  leaderData: leaderData,
                  category: widget.category,
                ),
              ],
            ),
          );
        }
        return LoadingDialog.showLoadingView();
      },
    );
  }
}
