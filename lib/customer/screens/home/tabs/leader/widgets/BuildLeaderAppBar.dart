part of 'LeaderWidgetsImports.dart';

class BuildLeaderAppBar extends PreferredSize {
  final Size preferredSize;

  BuildLeaderAppBar({
    this.preferredSize = const Size.fromHeight(kToolbarHeight + 15),
  }) : super(child: Container(), preferredSize: preferredSize);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Image.asset(Res.logo, height: 50),
      centerTitle: true,
      backgroundColor: MyColors.white,
      elevation: 0,
      toolbarHeight: 100,
      leadingWidth: 90,
      leading: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(width: 10),
          CartIcon(),
          SearchIcon(),
        ],
      ),
      automaticallyImplyLeading: false,
      actions: [
        NotifyIcon(),
      ],
    );
  }
}
