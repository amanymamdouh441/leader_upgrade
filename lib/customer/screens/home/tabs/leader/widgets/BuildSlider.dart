part of 'LeaderWidgetsImports.dart';

class BuildSlider extends StatefulWidget {
  final List<ImageModel> slider;
  final LeaderData leaderData;

  const BuildSlider({Key? key, required this.slider, required this.leaderData})
      : super(key: key);

  @override
  _BuildSliderState createState() => _BuildSliderState();
}

class _BuildSliderState extends State<BuildSlider> {
  @override
  Widget build(BuildContext context) {
    if (widget.slider.isEmpty) return Container();

    return Container(
      height: 200,
      child: Swiper(
        itemCount: widget.slider.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () => widget.leaderData
                .navigate(context, widget.slider[index].id, index),
            child: CachedImage(
              url: widget.slider[index].img,
              width: 100,
              height: 100,
              haveRadius: true,
              fit: BoxFit.cover,
            ),
          );
        },
        autoplay: true,
        scrollDirection: Axis.horizontal,
        pagination: SwiperPagination(alignment: Alignment.bottomCenter),
      ),
    );
  }
}
