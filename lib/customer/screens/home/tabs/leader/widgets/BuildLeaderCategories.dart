part of 'LeaderWidgetsImports.dart';

class BuildLeaderCategories extends StatelessWidget {
  final LeaderData leaderData;
  final Category category;

  const BuildLeaderCategories(
      {required this.leaderData, required this.category});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: MyText(
            title: tr(context, "cats"),
            size: 14,
            color: MyColors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        Visibility(
          visible: category.id == 0,
          child: BlocBuilder<GenericBloc<List<DropDownModel>>,
              GenericState<List<DropDownModel>>>(
            bloc: leaderData.printCategoriesCubit,
            builder: (_, state) {
              if (state is GenericUpdateState) {
                if (state.data.length > 0) {
                  return GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: state.data.length,
                    gridDelegate: GridFixedHeightDelegate(
                        crossAxisCount: 2,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        height: 40),
                    itemBuilder: (_, index) {
                      return BuildPrintItem(
                        dropDownModel: state.data[index],
                      );
                    },
                  );
                } else {
                  return MyText(
                    title: tr(context, "noSubCats"),
                    color: MyColors.primary,
                    size: 14,
                    alien: TextAlign.center,
                  );
                }
              } else {
                return Container();
              }
            },
          ),
          replacement: BlocBuilder<GenericBloc<List<SubCategory>>,
              GenericState<List<SubCategory>>>(
            bloc: leaderData.subCategoriesCubit,
            builder: (_, state) {
              if (state is GenericUpdateState) {
                if (state.data.length > 0) {
                  return GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: state.data.length,
                    gridDelegate: GridFixedHeightDelegate(
                        crossAxisCount: 2,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        height: 40),
                    itemBuilder: (_, index) {
                      return BuildCategoryItem(
                        subCategory: state.data[index],
                      );
                    },
                  );
                } else {
                  return MyText(
                    title: tr(context, "noSubCats"),
                    color: MyColors.primary,
                    size: 14,
                    alien: TextAlign.center,
                  );
                }
              } else {
                return Container();
              }
            },
          ),
        )
      ],
    );
  }
}
