part of 'LeaderImports.dart';

class LeaderData {
  final GenericBloc<int> pageCubit = GenericBloc(0);
  final GenericBloc<HomeModel?> homeCubit = GenericBloc(null);
  final GenericBloc<List<SubCategory>> subCategoriesCubit = GenericBloc([]);
  final GenericBloc<List<DropDownModel>> printCategoriesCubit = GenericBloc([]);

  Future<void> fetchHomeData(BuildContext context, int id,
      {bool refresh = true}) async {
    var data = await CustomerRepository(context).getHomeData(id, refresh);
    if (data != null) {
      if (data.countCart != 0) {
        context.read<CartCountCubit>().onUpdateCount(data.countCart);
      }
      context.read<NotifyCountCubit>().onUpdateCount(data.countNotify);
      homeCubit.onUpdateData(data);
    }
  }

  void fetchSubCategories(BuildContext context, int id,
      {bool refresh = true}) async {
    var data = await CustomerRepository(context).getSubCategories(id, refresh);
    subCategoriesCubit.onUpdateData(data);
  }

  void fetchPrintCategories(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getPrintCategories(refresh);
    printCategoriesCubit.onUpdateData(data);
  }

  void navigate(BuildContext context, int id, int index) {
    if (id == 0) {
      print("@@@@@@@@@@$index");
      AutoRouter.of(context).push(
          ImageZoomRoute(images: homeCubit.state.data!.sliders, index: index));
    } else {
      print("@@@@@@@@@@${homeCubit.state.data!.sliders[index].id}");
      AutoRouter.of(context).push(
          ProductDetailsRoute(id: homeCubit.state.data!.sliders[index].id));
    }
  }
}
