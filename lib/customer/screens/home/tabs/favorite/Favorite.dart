part of 'FavoriteImports.dart';

class Favorite extends StatefulWidget {
  const Favorite({Key? key}) : super(key: key);

  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: DefaultAppBar(
          title: tr(context,"fav"),
          leading: Container(),
          actions: [CartIcon()],
        ),
        body: Visibility(
          visible: context.read<AuthCubit>().state.authorized,
          child: Column(
            children: [
              BuildFavTabs(),
              Flexible(
                child: TabBarView(
                  children: [
                    FavProducts(),
                    RecentlyViewed(),
                  ],
                ),
              )
            ],
          ),
          replacement: Center(
            child: MyText(
              title: tr(context, "LoginFirst"),
              color: MyColors.primary,
              size: 15,
            ),
          ),
        ),
      ),
    );
  }
}
