part of 'FavoriteWidgetsImports.dart';

class BuildFavTabs extends StatelessWidget {
  const BuildFavTabs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: MyColors.white,
          boxShadow: [
            BoxShadow(
                color: MyColors.greyWhite,
                spreadRadius: 2,
                blurRadius: 2
            ),
          ]
      ),
      child: TabBar(
        unselectedLabelColor: MyColors.blackOpacity,
        labelColor: MyColors.black,
        indicatorColor: MyColors.black,
        indicatorSize: TabBarIndicatorSize.label,
        tabs: [
          Tab(
            text: tr(context,"favProds"),
          ),
          Tab(
            text: tr(context,"lastSeen"),
          ),
        ],
      ),
    );
  }
}
