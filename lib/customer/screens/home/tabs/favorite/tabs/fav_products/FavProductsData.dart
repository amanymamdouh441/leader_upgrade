part of 'FavProductsImports.dart';

class FavProductsData {
  GenericBloc<List<ProductModel>> productsCubit = GenericBloc([]);

  Future<void> fetchFavData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getFavProducts(refresh);
    productsCubit.onUpdateData(data);
  }

  onFavChanged(ProductModel? model) {
    if (model != null) {
      if (model.favorite == false) {
        int index =
            productsCubit.state.data.indexWhere((e) => e.id == model.id);
        productsCubit.state.data.removeAt(index);
        productsCubit.onUpdateData(productsCubit.state.data);
      }
    }
  }
}
