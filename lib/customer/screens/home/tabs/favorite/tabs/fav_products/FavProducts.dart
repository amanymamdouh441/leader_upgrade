part of 'FavProductsImports.dart';

class FavProducts extends StatefulWidget {
  const FavProducts({Key? key}) : super(key: key);

  @override
  _FavProductsState createState() => _FavProductsState();
}

class _FavProductsState extends State<FavProducts> {
  FavProductsData favProductsData = FavProductsData();

  @override
  Widget build(BuildContext context) {
    return GenericListView(
      type: ListViewType.gridApi,
      padding: EdgeInsets.all(10),
      cubit: favProductsData.productsCubit,
      onRefresh: favProductsData.fetchFavData,
      params: [context],
      emptyStr: tr(context, "noData"),
      gridCrossCount: 3,
      gridItemHeight: 180,
      itemBuilder: (_, index, item) {
        return BuildProductItem(
          model: item,
          onClosed: (model)=> favProductsData.onFavChanged(model),
        );
      },
    );
  }
}
