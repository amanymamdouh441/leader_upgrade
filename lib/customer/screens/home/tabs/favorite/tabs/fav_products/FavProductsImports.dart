import 'package:leader_upgrade/customer/models/product_model.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/customer/widgets/widgetsImports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'FavProducts.dart';
part 'FavProductsData.dart';