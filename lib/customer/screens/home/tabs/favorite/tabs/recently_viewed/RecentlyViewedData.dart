part of 'RecentlyViewedImports.dart';

class RecentlyViewedData {

  GenericBloc<List<ProductModel>> productsCubit = GenericBloc([]);

  Future<void> fetchViewedData(BuildContext context, {bool refresh = true})async{
    var data = await CustomerRepository(context).getVisitedProducts(refresh);
    productsCubit.onUpdateData(data);
  }

}