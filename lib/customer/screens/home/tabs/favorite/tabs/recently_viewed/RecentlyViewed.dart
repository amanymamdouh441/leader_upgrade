part of 'RecentlyViewedImports.dart';

class RecentlyViewed extends StatefulWidget {
  const RecentlyViewed({Key? key}) : super(key: key);

  @override
  _RecentlyViewedState createState() => _RecentlyViewedState();
}

class _RecentlyViewedState extends State<RecentlyViewed> {

  final RecentlyViewedData viewedData = RecentlyViewedData();

  @override
  Widget build(BuildContext context) {
    return GenericListView(
      type: ListViewType.gridApi,
      padding: EdgeInsets.all(5),
      cubit: viewedData.productsCubit,
      onRefresh: viewedData.fetchViewedData,
      params: [context],
      emptyStr: tr(context, "noData"),
      gridCrossCount: 3,
      gridItemHeight: 180,
      itemBuilder: (_,index, item){
        return BuildProductItem(model: item, showButtons: false,);
      },
    );
  }
}
