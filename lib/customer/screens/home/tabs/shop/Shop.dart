part of 'ShopImports.dart';

class Shop extends StatefulWidget {
  const Shop({Key? key}) : super(key: key);

  @override
  _ShopState createState() => _ShopState();
}

class _ShopState extends State<Shop> {
  ShopData shopData = ShopData();

  @override
  Widget build(BuildContext context) {
    var cats = context.watch<CatsCubit>().state.cats;
    return DefaultTabController(
      length: cats.length,
      child: Scaffold(
        appBar: DefaultAppBar(
          title: tr(context, "cats"),
          leading: Container(),
          actions: [
            CartIcon()
          ],
        ),
        body: Column(
          children: [
            BuildShopTabBar(cats: cats),
            BuildShopTabView(cats: cats),
          ],
        ),
      ),
    );
  }
}
