import 'package:leader_upgrade/customer/screens/home/tabs/shop/widgets/ShopWidgetsImports.dart';
import 'package:leader_upgrade/customer/widgets/widgetsImports.dart';
import 'package:leader_upgrade/general/blocks/cats_cubit/cats_cubit.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'Shop.dart';
part 'ShopData.dart';