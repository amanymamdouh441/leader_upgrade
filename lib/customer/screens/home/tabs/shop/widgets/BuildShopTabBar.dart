part of 'ShopWidgetsImports.dart';

class BuildShopTabBar extends StatelessWidget {
  final List<Category> cats;
  const BuildShopTabBar({Key? key, required this.cats}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: 40,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: MyColors.white,
        boxShadow: [
          BoxShadow(
            color: MyColors.greyWhite,
            spreadRadius: 2,
            blurRadius: 2
          ),
        ]
      ),
      child: TabBar(
        unselectedLabelColor: MyColors.blackOpacity,
        labelColor: MyColors.black,
        indicatorColor: MyColors.black,
        indicatorSize: TabBarIndicatorSize.label,
        isScrollable: true,
        tabs: List.generate( cats.length,
              (index) => Tab(text: cats[index].name),
        ),
      ),
    );
  }
}
