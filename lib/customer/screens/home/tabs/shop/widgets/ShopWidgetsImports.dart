import 'package:leader_upgrade/customer/models/category.dart';
import 'package:leader_upgrade/customer/screens/home/tabs/leader/widgets/LeaderWidgetsImports.dart';
import 'package:leader_upgrade/customer/widgets/grid_fixed_height_delegate.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'BuildShopTabBar.dart';
part 'BuildShopTabView.dart';