part of 'CartImports.dart';

class Cart extends StatefulWidget {
  const Cart({Key? key}) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  final CartData cartData = CartData();

  @override
  void initState() {
    var auth = context.read<AuthCubit>().state.authorized;
    if (auth) {
      // cartData.fetchData(context, refresh: false);
      cartData.fetchData(context);
    } else {
      cartData.getLocalCart(context);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(
        title: tr(context, "cart"),
        actions: [
          BlocBuilder<GenericBloc<CartModel?>, GenericState<CartModel?>>(
            bloc: cartData.cartCubit,
            builder: (context, state) {
              if (state.data == null) {
                return Container();
              }
              return Container(
                margin: const EdgeInsets.all(15),
                child: MyText(
                  title:
                      "${state.data!.cartItems.length} ${tr(context, "products")}",
                  color: MyColors.blackOpacity,
                  size: 13,
                ),
              );
            },
          )
        ],
      ),
      body: BlocBuilder<GenericBloc<CartModel?>, GenericState<CartModel?>>(
        bloc: cartData.cartCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if (state.data!.cartItems.isEmpty) {
              return Center(
                child: MyText(
                  title: tr(context, "noProducts"),
                  color: MyColors.primary,
                  size: 14,
                ),
              );
            }
            return Column(
              children: [
                BuildDeleteCart(cartData: cartData),
                Flexible(
                  child: RefreshIndicator(
                    onRefresh: () => cartData.refresh(context),
                    child: GenericListView(
                      type: ListViewType.normal,
                      children: List.generate(
                        state.data!.cartItems.length,
                        (index) => BuildCartItem(
                          cartData: cartData,
                          model: state.data!.cartItems[index],
                          index: index,
                        ),
                      ),
                    ),
                  ),
                ),
                BuildCartButton(
                  model: state.data!,
                  cartData: cartData,
                ),
              ],
            );
          }
          return LoadingDialog.showLoadingView();
        },
      ),
    );
  }
}
