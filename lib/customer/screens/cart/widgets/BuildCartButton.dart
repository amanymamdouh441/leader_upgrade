part of 'CartWidgetsImports.dart';

class BuildCartButton extends StatelessWidget {
  final CartModel model;
  final CartData cartData;
  const BuildCartButton({Key? key,required this.model, required this.cartData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
        color: MyColors.white,
        boxShadow: [
          BoxShadow(
            color: MyColors.greyWhite,
            spreadRadius: 2,
            blurRadius: 2,
          ),
        ],
      ),
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                MyText(
                  title: "${tr(context, "TotalWithTax")} :  ",
                  color: MyColors.blackOpacity,
                  size: 11.5,
                  fontWeight: FontWeight.w600,
                ),
                MyText(
                  title: " ${model.total.toStringAsFixed(2)} ${tr(context, "sr")} ",
                  color: MyColors.black,
                  size: 12,
                  fontWeight: FontWeight.bold,
                )
              ],
            ),
          ),
          DefaultButton(
            title: tr(context, "finishOrder"),
            onTap: () => cartData.completeAddOrder(context, model),
            borderRadius: BorderRadius.circular(5),
            margin: EdgeInsets.symmetric(vertical: 10),
          )
        ],
      ),
    );
  }
}
