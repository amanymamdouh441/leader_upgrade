part of 'CartWidgetsImports.dart';

class BuildCartItem extends StatelessWidget {
  final CartData cartData;
  final CartItemModel model;
  final int index;
  const BuildCartItem({required this.cartData, required this.model, required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.2,
        actions: [
          IconSlideAction(
            color: Colors.red[700],
            icon: Icons.delete,
            onTap: ()=> cartData.removeItemFromCard(context, model.id, index),
          ),
        ],
        child: Container(
          height: 150,
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.symmetric(horizontal: 5),
          padding: const EdgeInsets.all(10),
          color: MyColors.white,
          child: Row(
            children: [
              CachedImage(
                url: model.img,
                height: 130,
                width: 90,
                placeHolder: SvgPicture.asset(Res.noImage),
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: MyText(
                              title: model.productName,
                              color: MyColors.blackOpacity,
                              size: 11,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Bounce(
                            onPressed: ()=> cartData.removeItemFromCard(context, model.id, index),
                            duration: Duration(milliseconds: 120),
                            child: Icon(
                              Icons.close,
                              color: Colors.red,
                              size: 20,
                            ),
                          ),
                        ],
                      ),
                      MyText(
                        title: "${model.price} ${tr(context, "sr")}",
                        color: MyColors.black,
                        size: 12,
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: ()=> cartData.specificBottomSheet(context,model, index),
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                color: MyColors.secondary,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  MyText(
                                    title: tr(context, "specs"),
                                    color: MyColors.blackOpacity,
                                    size: 9,
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down,
                                    color: MyColors.black,
                                    size: 15,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border:
                                    Border.all(color: MyColors.blackOpacity)),
                            child: Row(
                              children: [
                                Bounce(
                                  onPressed: ()=> cartData.incrementCartItem(context, index),
                                  duration: Duration(milliseconds: 200),
                                  child: Icon(
                                    Icons.add,
                                    color: MyColors.black,
                                    size: 15,
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                  child: VerticalDivider(
                                    color: MyColors.blackOpacity,
                                    thickness: 0.5,
                                  ),
                                ),
                                MyText(
                                  title: "${model.qty}",
                                  color: MyColors.blackOpacity,
                                  size: 9,
                                  fontWeight: FontWeight.w600,
                                ),
                                SizedBox(
                                  height: 15,
                                  child: VerticalDivider(
                                    color: MyColors.blackOpacity,
                                    thickness: 0.5,
                                  ),
                                ),
                                Bounce(
                                  onPressed: ()=> cartData.decrementCartItem(context, index),
                                  duration: Duration(milliseconds: 200),
                                  child: Icon(
                                    MdiIcons.minus,
                                    color: MyColors.blackOpacity,
                                    size: 15,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
