part of 'CartWidgetsImports.dart';

class BuildSpecificationBottom extends StatelessWidget {
  final List<SpecificationModel> specs;
  final Function() onConfirm;
  final Function(SpecificationModel model, int id, int index, int position)
      onItemClick;

  const BuildSpecificationBottom({
    Key? key,
    required this.specs,
    required this.onItemClick,
    required this.onConfirm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.white,
      height: 350,
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          Flexible(
            child: ListView.separated(
              itemCount: specs.length,
              itemBuilder: (BuildContext context, int index) {
                return Visibility(
                  visible: specs[index].subSpecifications.length>0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      MyText(
                        title: specs[index].name,
                        color: MyColors.blackOpacity,
                        size: 11,
                      ),
                      Wrap(
                        spacing: 15,
                        runSpacing: 10,
                        children: List.generate(
                          specs[index].subSpecifications.length,
                          (position) {
                            return SizedBox(
                              height: 25,
                              width: 70,
                              child: BuildSpecItem(
                                model: specs[index].subSpecifications[position],
                                selected: specs[index].selected,
                                onClick: (id) => onItemClick(
                                    specs[index], id, index, position),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Divider(color: MyColors.greyWhite);
              },
            ),
          ),
          DefaultButton(
            title: tr(context, "update"),
            margin: EdgeInsets.all(20),
            onTap: onConfirm,
          )
        ],
      ),
    );
  }
}
