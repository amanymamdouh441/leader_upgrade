part of 'CartWidgetsImports.dart';

class BuildBottomSheet extends StatelessWidget {
  const BuildBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.white,
      height: 300,
      padding: const EdgeInsets.all(10),
      child: ListView(
        children: [
          MyText(
            title: tr(context, "color"),
            color: MyColors.blackOpacity,
            size: 11,
          ),
          GridView.builder(
            itemCount: 5,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(vertical: 10),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              mainAxisSpacing: 10,
              crossAxisSpacing: 15,
              childAspectRatio: 3.5,
            ),
            itemBuilder: (_, index) {
              return Container();
              // return BuildColorWidget();
            },
          ),
          MyText(
            title: tr(context, "size"),
            color: MyColors.blackOpacity,
            size: 11,
          ),
          GridView.builder(
            itemCount: 5,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(vertical: 10),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              mainAxisSpacing: 10,
              crossAxisSpacing: 15,
              childAspectRatio: 3.5,
            ),
            itemBuilder: (_, index) {
              return Container();
             // return BuildSizeWidget();
            },
          ),
        ],
      ),
    );
  }
}
