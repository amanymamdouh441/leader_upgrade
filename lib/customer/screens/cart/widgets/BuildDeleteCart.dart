part of 'CartWidgetsImports.dart';

class BuildDeleteCart extends StatelessWidget {
  final CartData cartData;
  const BuildDeleteCart({Key? key, required this.cartData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Bounce(
      onPressed: ()=> cartData.removeAllCard(context),
      duration: Duration(milliseconds: 150),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          children: [
            Icon(
              Icons.delete_outline,
              color: Colors.red,
              size: 25,
            ),
            SizedBox(width: 5),
            MyText(
              title: tr(context, "deleteAllCart"),
              color: Colors.red,
              size: 13,
            ),
          ],
        ),
      ),
    );
  }
}
