part of 'CartImports.dart';

class CartData {
  GenericBloc<CartModel?> cartCubit = new GenericBloc(null);
  final GenericBloc<List<SpecificationModel>> selectedSpecCubit =
      GenericBloc([]);

  fetchData(BuildContext context, {bool refresh = true}) async {
    var data = await CustomerRepository(context).getCartDetails(refresh);
    if (data != null) {
      cartCubit.onUpdateData(data);
    }
  }

  getLocalCart(BuildContext context) async {
    var db = context.read<MyDatabase>();
    var data = await db.allProductEntries;
    List<CartItemModel> cartItems = data.map((e) {
      var cats = List<SpecificationModel>.from(
          json.decode(e.cats).map((e) => SpecificationModel.fromJson(e)));
      return CartItemModel(
          id: e.proId,
          price: e.price,
          qty: e.qty,
          img: e.image,
          productName: e.name,
          cats: cats);
    }).toList();
    CartModel model = CartModel(
      cartItems: cartItems,
      total: cartItems.fold(0, (prev, e) => prev + (e.price * e.qty)),
      shippingFee: 20,
      commission: 20,
    );
    cartCubit.onUpdateData(model);
  }

  setSelectedItem(List<SpecificationModel> model) {
    var data = model.map((e) {
      e.selected = e.subSpecifications
          .where((s) => s.isChecked ?? false)
          .map((sub) => sub.id)
          .toList();
      return e;
    }).toList();
    selectedSpecCubit.onUpdateData(data);
  }

  specificBottomSheet(
      BuildContext context, CartItemModel model, int cartIndex) {
    setSelectedItem(model.cats);
    showModalBottomSheet(
      context: context,
      builder: (_) {
        return BlocBuilder<GenericBloc<List<SpecificationModel>>,
            GenericState<List<SpecificationModel>>>(
          bloc: selectedSpecCubit,
          builder: (context, state) {
            if (state.data.length > 0) {
              return BuildSpecificationBottom(
                specs: state.data,
                onConfirm: () =>
                    confirmSelectedSpec(context, model.id, cartIndex),
                onItemClick: (model, id, index, position) =>
                    setSelectedSpec(model, id, index, position),
              );
            } else {
              return Center(
                child: MyText(
                  title: tr(context, "noSpecs"),
                  color: MyColors.black,
                  size: 14,
                ),
              );
            }
          },
        );
      },
    );
  }

  setSelectedSpec(SpecificationModel model, int id, int index, int position) {
    if (!model.selected.any((element) => element == id)) {
      model.selected.clear();
      selectedSpecCubit.state.data[index].subSpecifications =
          selectedSpecCubit.state.data[index].subSpecifications.map((e) {
        e.isChecked = false;
        return e;
      }).toList();
      selectedSpecCubit
          .state.data[index].subSpecifications[position].isChecked = true;
      model.selected.add(id);
      selectedSpecCubit.state.data[index] = model;
      selectedSpecCubit.onUpdateData(selectedSpecCubit.state.data);
    }
  }

  confirmSelectedSpec(BuildContext context, int id, int index) async {
    var auth = context.read<AuthCubit>().state.authorized;
    if (auth) {
      var ids = selectedSpecCubit.state.data
          .map((e) => e.selected)
          .expand((element) => element)
          .toList();
      var result =
          await CustomerRepository(context).updateCartItemSpecs(id, ids);
      if (result != null) cartCubit.state.data!.cartItems[index] = result;
    } else {
      var db = context.read<MyDatabase>();
      var product =
          await db.productById(cartCubit.state.data!.cartItems[index].id);
      ProductTableData entry = ProductTableData(
          cats: json.encode(selectedSpecCubit.state.data),
          qty: product.qty,
          price: product.price.toDouble(),
          image: product.image,
          name: product.name,
          proId: product.proId,
          id: product.id);
      db.updateProduct(entry);
    }
    cartCubit.onUpdateData(cartCubit.state.data);
    Navigator.of(context).pop();
  }

  removeItemFromCard(BuildContext context, int id, int index) async {
    var auth = context.read<AuthCubit>().state.authorized;
    if (auth) {
      var data = await CustomerRepository(context).removeFromCart(id);
      cartCubit.state.data!.total = data;
    } else {
      var db = context.read<MyDatabase>();
      await db.deleteProduct(id);
      cartCubit.state.data!.total = cartCubit.state.data!.cartItems
          .fold(0, (prev, e) => prev + (e.price * e.qty));
    }
    cartCubit.state.data!.cartItems.removeAt(index);
    cartCubit.onUpdateData(cartCubit.state.data);
    var cubit = context.read<CartCountCubit>();
    cubit.onUpdateCount(cubit.state.count - 1);
  }

  removeAllCard(BuildContext context) async {
    var auth = context.read<AuthCubit>().state.authorized;
    if (auth) {
      await CustomerRepository(context).removeAllCart();
    } else {
      var db = context.read<MyDatabase>();
      await db.deleteAllProduct();
    }
    cartCubit.state.data!.cartItems.clear();
    cartCubit.state.data!.total = 0;
    cartCubit.onUpdateData(cartCubit.state.data);
    context.read<CartCountCubit>().onUpdateCount(0);
  }

  incrementCartItem(BuildContext context, int index) async {
    var auth = context.read<AuthCubit>().state.authorized;
    var item = cartCubit.state.data!.cartItems[index];
    var oldQty = item.qty;
    if (auth) {
      ++item.qty;
      var result =
          await CustomerRepository(context).changeCartQty(item.id, item.qty);
      cartCubit.state.data!.total = result;
      item.price = item.price / oldQty;
      item.price = item.price * item.qty;
    } else {
      ++item.qty;
      item.price = item.price / oldQty;
      item.price = item.price * item.qty;
      var db = context.read<MyDatabase>();
      var product = await db.productById(item.id);
      ProductTableData entry = ProductTableData(
          cats: product.cats,
          qty: item.qty,
          price: item.price.toDouble(),
          image: product.image,
          name: product.name,
          proId: product.proId,
          id: product.id);
      db.updateProduct(entry);
      cartCubit.state.data!.total = cartCubit.state.data!.cartItems
          .fold(0, (prev, e) => prev + (e.price * e.qty));
    }
    cartCubit.state.data!.cartItems[index] = item;
    cartCubit.onUpdateData(cartCubit.state.data);
  }

  decrementCartItem(BuildContext context, int index) async {
    var item = cartCubit.state.data!.cartItems[index];
    if (item.qty == 1) return;
    var auth = context.read<AuthCubit>().state.authorized;
    var oldQty = item.qty;
    if (auth) {
      --item.qty;
      item.price = item.price - (item.price / oldQty);
      var result =
          await CustomerRepository(context).changeCartQty(item.id, item.qty);
      cartCubit.state.data!.total = result;
    } else {
      --item.qty;
      item.price = item.price - (item.price / oldQty);
      var db = context.read<MyDatabase>();
      var product = await db.productById(item.id);
      ProductTableData entry = ProductTableData(
          cats: product.cats,
          qty: item.qty,
          price: item.price.toDouble(),
          image: product.image,
          name: product.name,
          proId: product.proId,
          id: product.id);
      db.updateProduct(entry);
      cartCubit.state.data!.total = cartCubit.state.data!.cartItems
          .fold(0, (prev, e) => prev + (e.price * e.qty));
    }
    cartCubit.state.data!.cartItems[index] = item;
    cartCubit.onUpdateData(cartCubit.state.data);
  }

  completeAddOrder(BuildContext context, CartModel model) async {
    var auth = context.read<AuthCubit>().state.authorized;
    if (!auth) {
      CustomToast.showAuthDialog(context: context);
      return;
    } else {
      var data = await CustomerRepository(context).checkCart();
      if (data) {
        AutoRouter.of(context).push(AddressRoute(model: model));
      }
    }
  }
   refresh(BuildContext context)async{
    var auth = context.read<AuthCubit>().state.authorized;
    if (auth) {
     await fetchData(context);
    } else{
      getLocalCart(context);
    }
  }
}
