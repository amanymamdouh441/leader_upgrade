part of 'CustomerRepoImports.dart';

class CustomerRepository {
  late BuildContext _context;
  late CustomerLogicMethods _logicMethods;

  CustomerRepository(BuildContext context) {
    _context = context;
    _logicMethods = new CustomerLogicMethods(_context);
  }

  Future<bool> checkActive(String phoneNumber) =>
      _logicMethods.checkActive(phoneNumber);

  Future<MainModel?> getCategories(bool refresh) =>
      _logicMethods.getCategories(refresh);

  Future<List<SubCategory>> getSubCategories(int id, bool refresh) =>
      _logicMethods.getSubCategories(id, refresh);

  Future<List<DropDownModel>> getPrintCategories(bool refresh) =>
      _logicMethods.getPrintCategories(refresh);

  Future<List<ProductModel>> getPrintDetails(
          int id, int currentPage, bool refresh) =>
      _logicMethods.getPrintDetails(id, currentPage, refresh);

  Future<bool> addPrintOrder(AddPrintOrderModel model) =>
      _logicMethods.addPrintOrder(model);

  Future<HomeModel?> getHomeData(int id, bool refresh) =>
      _logicMethods.getHomeData(id, refresh);

  Future<void> changePassword(String oldPass, String newPass) =>
      _logicMethods.changePassword(oldPass, newPass);

  Future<List<ProductModel>> getFavProducts(bool refresh) =>
      _logicMethods.getFavProducts(refresh);

  Future<List<ProductModel>> getVisitedProducts(bool refresh) =>
      _logicMethods.getVisitedProducts(refresh);

  Future<CatDetailsModel?> getCatSpecifications(int catId, bool refresh) =>
      _logicMethods.getCatSpecifications(catId, refresh);

  Future<List<SpecificationModel>> getProSpecifications(
          int proId, bool refresh) =>
      _logicMethods.getProSpecifications(proId, refresh);

  Future<ProductModel> getProductDetails(int proId, bool refresh) =>
      _logicMethods.getProductDetails(proId, refresh);

  Future<List<ProductModel>> getCatProducts(FilterModel model) =>
      _logicMethods.getCatProducts(model);

  Future<bool> addToFavourite(int id) => _logicMethods.addToFavourite(id);

  Future<bool> checkCart() => _logicMethods.checkCart();

  Future<int> getCatProductsCount(FilterModel model) =>
      _logicMethods.getCatProductsCount(model);

  Future<bool> addToCart(AddCartModel model) => _logicMethods.addToCart(model);

  Future<CartModel?> getCartDetails(bool refresh) =>
      _logicMethods.getCartDetails(refresh);

  Future<num> removeFromCart(int id) => _logicMethods.removeFromCart(id);

  Future<num> removeAllCart() => _logicMethods.removeAllCart();

  Future<num> changeCartQty(int id, int count) =>
      _logicMethods.changeCartQty(id, count);

  Future<CartItemModel?> updateCartItemSpecs(int id, List<int> specsId) =>
      _logicMethods.updateCartItemSpecs(id, specsId);

  Future<bool> addOrder(AddOrderModel model) => _logicMethods.addOrder(model);

  Future<List<OrderModel>> getAllOrders(bool refresh) =>
      _logicMethods.getAllOrders(refresh);

  Future<List<OrderModel>> getNewOrders(bool refresh) =>
      _logicMethods.getNewOrders(refresh);

  Future<List<OrderModel>> getProgressOrders(bool refresh) =>
      _logicMethods.getProgressOrders(refresh);

  Future<List<OrderModel>> getShippingOrders(bool refresh) =>
      _logicMethods.getShippingOrders(refresh);

  Future<List<OrderModel>> getFinishedOrders(bool refresh) =>
      _logicMethods.getFinishedOrders(refresh);

  Future<List<OrderModel>> getReturnOrders(bool refresh) =>
      _logicMethods.getReturnOrders(refresh);

  Future<OrderDetailsModel> getOrderDetails(bool refresh, int orderId) =>
      _logicMethods.getOrderDetails(refresh, orderId);

  Future<List<DropDownModel>> getReturnReasons(bool refresh) =>
      _logicMethods.getReturnReasons(refresh);

  Future<bool> addReturn(AddReturnModel model) =>
      _logicMethods.addReturn(model);

  Future<List<NotifyModel>> getNotify(bool refresh) =>
      _logicMethods.getNotify(refresh);

  Future<num> getWallet(bool refresh) => _logicMethods.getWallet(refresh);

  Future<CouponModel?> useCoupon(String coupon, num total) =>
      _logicMethods.useCoupon(coupon, total);

  Future<SettingModel> getOrderSetting(bool refresh) =>
      _logicMethods.getOrderSetting(refresh);

  Future<List<String>> getListAramexCities(String nameStartsWith) =>
      _logicMethods.getListAramexCities(nameStartsWith);

  /// Address
  Future<List<UserAddressesModel>> getUserAddresses(bool refresh) =>
      _logicMethods.getUserAddresses(refresh);

  Future<UserAddressesModel?> addUserAddress(LocationModel model) =>
      _logicMethods.addUserAddress(model);

  Future<UserAddressesModel?> updateUserAddress(
          LocationModel model, int addressId) =>
      _logicMethods.updateUserAddress(model, addressId);

  Future<bool> deleteAddress(int addressId) =>
      _logicMethods.deleteAddress(addressId);
}
