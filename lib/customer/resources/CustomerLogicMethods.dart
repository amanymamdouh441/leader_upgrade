part of 'CustomerRepoImports.dart';

class CustomerLogicMethods {
  final BuildContext context;

  CustomerLogicMethods(this.context);

  Future<bool> checkActive(String phoneNumber) async {
    Map<String, dynamic> body = {"phoneNumber": phoneNumber};
    bool? data = await PrevGenericHttp<bool>(context).callApi(
      name: ApiNames.checkActive,
      json: body,
      returnType: ReturnType.Type,
      showLoader: false,
      returnDataFun: (data) => data["data"]["active"],
      methodType: MethodType.Get,
    );
    return data ?? true;
  }

  Future<MainModel?> getCategories(bool refresh) async {
    MainModel? data = await PrevGenericHttp<MainModel>(context).callApi(
      returnType: ReturnType.Model,
      methodType: MethodType.Get,
      name: ApiNames.categories,
      refresh: refresh,
      showLoader: false,
      returnDataFun: (data) => data["data"],
      toJsonFunc: (json) => MainModel.fromJson(json),
    );

    if (data != null) {
      data.cats.add(Category(id: 0, img: "", name: tr(context, "print")));
      print(
          ">>>>>>>>>>>>>>>>${data.cats.where((element) => element.id == 0).map((e) => e.toJson())}");
      context.read<CatsCubit>().onUpdateData(data.cats);
      context.read<ShareCubit>().onUpdateData(data.android, data.ios);
    }
    return null;
  }

  Future<List<SubCategory>> getSubCategories(int id, bool refresh) async {
    return await PrevGenericHttp<SubCategory>(context).callApi(
        returnType: ReturnType.List,
        methodType: MethodType.Get,
        name: ApiNames.subCategories,
        json: {"categoryId": id},
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => SubCategory.fromJson(json)) as List<SubCategory>;
  }

  Future<List<DropDownModel>> getPrintCategories(bool refresh) async {
    var data = await PrevGenericHttp<DropDownModel>(context).callApi(
        returnType: ReturnType.List,
        methodType: MethodType.Get,
        name: ApiNames.printCategories,
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => DropDownModel.fromJson(json));
    return data ?? [];
  }

  Future<bool> addPrintOrder(AddPrintOrderModel model) async {
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.addPrintOrder,
      json: model.toJson(),
      returnType: ReturnType.Type,
      methodType: MethodType.UploadFile,
      showLoader: true,
    );
    return data != null;
  }

  Future<List<ProductModel>> getPrintDetails(
      int id, int currentPage, bool refresh) async {
    return await PrevGenericHttp<ProductModel>(context).callApi(
            returnType: ReturnType.List,
            methodType: MethodType.Get,
            name: ApiNames.printProducts,
            json: {"printCategoryId": id, "currentPage": currentPage},
            refresh: refresh,
            returnDataFun: (data) => data["data"]["products"],
            toJsonFunc: (json) => ProductModel.fromJson(json))
        as List<ProductModel>;
  }

  Future<HomeModel?> getHomeData(int id, bool refresh) async {
    return await PrevGenericHttp<HomeModel>(context).callApi(
        returnType: ReturnType.Model,
        methodType: MethodType.Get,
        name: ApiNames.home,
        json: {"categoryId": id},
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => HomeModel.fromJson(json));
  }

  Future<void> changePassword(String oldPass, String newPass) async {
    Map<String, dynamic> body = {
      "lang": context.read<LangCubit>().state.locale.languageCode,
      "oldPassword": oldPass,
      "newPassword": newPass,
    };

    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.changePass,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      showLoader: false,
    );
    if (data != null) {
      CustomToast.showSimpleToast(msg: tr(context, "passwordSuccess"));
      Navigator.of(context).pop();
    }
  }

  Future<List<ProductModel>> getFavProducts(bool refresh) async {
    var data = await PrevGenericHttp<ProductModel>(context).callApi(
        returnType: ReturnType.List,
        methodType: MethodType.Get,
        name: ApiNames.favProducts,
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => ProductModel.fromJson(json));
    return data ?? [];
  }

  Future<List<ProductModel>> getVisitedProducts(bool refresh) async {
    var data = await PrevGenericHttp<ProductModel>(context).callApi(
        returnType: ReturnType.List,
        methodType: MethodType.Get,
        name: ApiNames.visitedProducts,
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => ProductModel.fromJson(json));
    return data ?? [];
  }

  Future<CatDetailsModel?> getCatSpecifications(int catId, bool refresh) async {
    var data = await PrevGenericHttp<CatDetailsModel>(context).callApi(
        returnType: ReturnType.Model,
        methodType: MethodType.Get,
        name: ApiNames.getListSpecification,
        json: {"categoryId": catId},
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => CatDetailsModel.fromJson(json));
    return data;
  }

  Future<List<SpecificationModel>> getProSpecifications(
      int proId, bool refresh) async {
    var data = await PrevGenericHttp<SpecificationModel>(context).callApi(
        returnType: ReturnType.List,
        methodType: MethodType.Get,
        name: ApiNames.getProSpecification,
        json: {"productId": proId},
        refresh: refresh,
        returnDataFun: (data) => data["data"]["specifaction"],
        toJsonFunc: (json) => SpecificationModel.fromJson(json));
    return data ?? [];
  }

  Future<ProductModel> getProductDetails(int proId, bool refresh) async {
    return await PrevGenericHttp<ProductModel>(context).callApi(
        returnType: ReturnType.Model,
        methodType: MethodType.Get,
        json: {"productId": proId},
        name: ApiNames.getProSpecification,
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => ProductModel.fromJson(json)) as ProductModel;
  }

  Future<List<ProductModel>> getCatProducts(FilterModel model) async {
    var data = await PrevGenericHttp<ProductModel>(context).callApi(
        returnType: ReturnType.List,
        methodType: MethodType.Get,
        name: ApiNames.getCatProducts,
        json: model.toJson(),
        refresh: model.refresh ?? true,
        returnDataFun: (data) => data["data"]["products"],
        toJsonFunc: (json) => ProductModel.fromJson(json));
    return data ?? [];
  }

  Future<int> getCatProductsCount(FilterModel model) async {
    var data = await PrevGenericHttp<int>(context).callApi(
        returnType: ReturnType.Type,
        methodType: MethodType.Get,
        name: ApiNames.getCatProductsCount,
        json: model.toJson(),
        refresh: model.refresh ?? true,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => json);
    return data ?? 0;
  }

  Future<bool> addToFavourite(int id) async {
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      name: ApiNames.addFavourite,
      json: {"productId": id},
      showLoader: true,
    );
    return data != null;
  }

  Future<bool> checkCart() async {
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      returnType: ReturnType.Type,
      methodType: MethodType.Get,
      name: ApiNames.checkCart,
      showLoader: false,
    );
    return data != null;
  }

  Future<bool> addToCart(AddCartModel model) async {
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      name: ApiNames.addToCartAsync,
      json: model.toJson(),
      showLoader: true,
    );
    return data != null;
  }

  Future<num> removeFromCart(int id) async {
    var data = await PrevGenericHttp<num>(context).callApi(
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      name: ApiNames.removeItemFromCart,
      json: {"cartId": id},
      returnDataFun: (data) => data["data"],
      showLoader: true,
    );
    return data != null ? data : 0;
  }

  Future<num> removeAllCart() async {
    var data = await PrevGenericHttp<num>(context).callApi(
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      name: ApiNames.removeAllCart,
      returnDataFun: (data) => data["data"],
      showLoader: true,
    );
    return data != null ? data : 0;
  }

  Future<num> changeCartQty(int id, int count) async {
    var data = await PrevGenericHttp<num>(context).callApi(
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      name: ApiNames.changeCartQty,
      json: {"cartId": id, "qty": count},
      returnDataFun: (data) => data["data"],
      showLoader: true,
    );
    return data != null ? data : 0;
  }

  Future<CartItemModel?> updateCartItemSpecs(int id, List<int> specsId) async {
    var data = await PrevGenericHttp<CartItemModel>(context).callApi(
      returnType: ReturnType.Model,
      methodType: MethodType.Post,
      name: ApiNames.changeCartSpecifications,
      json: {"cartId": id, "SubSpecificationIds": json.encode(specsId)},
      returnDataFun: (data) => data["data"],
      toJsonFunc: (json) => CartItemModel.fromJson(json),
      showLoader: true,
    );
    return data;
  }

  Future<CartModel?> getCartDetails(bool refresh) async {
    var data = await PrevGenericHttp<CartModel>(context).callApi(
        returnType: ReturnType.Model,
        methodType: MethodType.Get,
        name: ApiNames.listCart,
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => CartModel.fromJson(json));
    return data;
  }

  Future<bool> addOrder(AddOrderModel model) async {
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.addOrder,
      json: model.toJson(),
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      showLoader: true,
    );
    return data != null;
  }

  Future<List<OrderModel>> getAllOrders(bool refresh) async {
    return await PrevGenericHttp<OrderModel>(context).callApi(
      name: ApiNames.getAllOrders,
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      toJsonFunc: (json) => OrderModel.fromJson(json),
      returnDataFun: (data) {
        if (data != null) {
          return data["data"];
        }
      },
      showLoader: false,
      refresh: refresh,
    ) as List<OrderModel>;
  }

  Future<List<OrderModel>> getNewOrders(bool refresh) async {
    return await PrevGenericHttp<OrderModel>(context).callApi(
      name: ApiNames.getNewOrders,
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      toJsonFunc: (json) => OrderModel.fromJson(json),
      returnDataFun: (data) => data["data"],
      showLoader: false,
      refresh: refresh,
    ) as List<OrderModel>;
  }

  Future<List<OrderModel>> getProgressOrders(bool refresh) async {
    return await PrevGenericHttp<OrderModel>(context).callApi(
      name: ApiNames.getProgressOrders,
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      toJsonFunc: (json) => OrderModel.fromJson(json),
      returnDataFun: (data) => data["data"],
      showLoader: false,
      refresh: refresh,
    ) as List<OrderModel>;
  }

  Future<List<OrderModel>> getShippingOrders(bool refresh) async {
    return await PrevGenericHttp<OrderModel>(context).callApi(
      name: ApiNames.getShippingOrders,
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      toJsonFunc: (json) => OrderModel.fromJson(json),
      returnDataFun: (data) => data["data"],
      showLoader: false,
      refresh: refresh,
    ) as List<OrderModel>;
  }

  Future<List<OrderModel>> getFinishedOrders(bool refresh) async {
    return await PrevGenericHttp<OrderModel>(context).callApi(
      name: ApiNames.getFinishedOrders,
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      toJsonFunc: (json) => OrderModel.fromJson(json),
      returnDataFun: (data) => data["data"],
      showLoader: false,
      refresh: refresh,
    ) as List<OrderModel>;
  }

  Future<List<OrderModel>> getReturnOrders(bool refresh) async {
    return await PrevGenericHttp<OrderModel>(context).callApi(
      name: ApiNames.getReturnOrders,
      returnType: ReturnType.List,
      methodType: MethodType.Get,
      toJsonFunc: (json) => OrderModel.fromJson(json),
      returnDataFun: (data) => data["data"],
      showLoader: false,
      refresh: refresh,
    ) as List<OrderModel>;
  }

  Future<OrderDetailsModel> getOrderDetails(bool refresh, int orderId) async {
    Map<String, dynamic> body = {
      "orderId": orderId,
    };
    return await PrevGenericHttp<OrderDetailsModel>(context).callApi(
      name: ApiNames.getOrderDetails,
      returnType: ReturnType.Model,
      methodType: MethodType.Get,
      json: body,
      toJsonFunc: (json) => OrderDetailsModel.fromJson(json),
      returnDataFun: (data) => data["data"],
      showLoader: false,
      refresh: refresh,
    ) as OrderDetailsModel;
  }

  Future<List<DropDownModel>> getReturnReasons(bool refresh) async {
    var data = await PrevGenericHttp<DropDownModel>(context).callApi(
        returnType: ReturnType.List,
        methodType: MethodType.Get,
        name: ApiNames.getReturnReason,
        refresh: refresh,
        returnDataFun: (data) => data["data"],
        toJsonFunc: (json) => DropDownModel.fromJson(json));
    return data ?? [];
  }

  Future<bool> addReturn(AddReturnModel model) async {
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.addReturn,
      json: model.toJson(),
      returnType: ReturnType.Type,
      methodType: MethodType.UploadFile,
      showLoader: true,
    );
    return data != null;
  }

  Future<List<NotifyModel>> getNotify(bool refresh) async {
    return await PrevGenericHttp<NotifyModel>(context).callApi(
      name: ApiNames.listOfNotify,
      returnType: ReturnType.List,
      showLoader: false,
      refresh: refresh,
      returnDataFun: (data) => data["data"],
      methodType: MethodType.Get,
      toJsonFunc: (json) => NotifyModel.fromJson(json),
    ) as List<NotifyModel>;
  }

  Future<num> getWallet(bool refresh) async {
    var data = await PrevGenericHttp<int>(context).callApi(
        returnType: ReturnType.Type,
        methodType: MethodType.Get,
        name: ApiNames.wallet,
        refresh: refresh,
        returnDataFun: (data) {
          if (data["key"] ==1) {
            // CustomToast.showSimpleToast(msg: data['msg']);

            return data['data'];
          } else {
            CustomToast.showSimpleToast(msg: data['msg']);
          }
        },
        toJsonFunc: (json) => json);
    return data ?? 0;
  }

  Future<CouponModel> useCoupon(String coupon, num total) async {
    return await PrevGenericHttp<CouponModel>(context).callApi(
      name: ApiNames.useCoupon,
      json: {
        "copon": coupon,
        "total": total,
      },
      returnType: ReturnType.Model,
      methodType: MethodType.Post,
      toJsonFunc: (json) => CouponModel.fromJson(json),
      returnDataFun: (data) => data["data"],
      showLoader: true,
    ) as CouponModel;
  }

  Future<SettingModel> getOrderSetting(bool refresh) async {
    return await PrevGenericHttp<SettingModel>(context).callApi(
      returnType: ReturnType.Model,
      methodType: MethodType.Get,
      name: ApiNames.getSetting,
      refresh: refresh,
      returnDataFun: (data) => data["data"],
      toJsonFunc: (json) => SettingModel.fromJson(json),
    ) as SettingModel;
  }

  Future<List<UserAddressesModel>> getUserAddresses(bool refresh) async {
    var data = await PrevGenericHttp<UserAddressesModel>(context).callApi(
      name: ApiNames.listOfUserAddresses,
      returnType: ReturnType.List,
      showLoader: false,
      refresh: refresh,
      returnDataFun: (data) => data['data'],
      methodType: MethodType.Get,
      toJsonFunc: (json) =>
          json == null ? {} : UserAddressesModel.fromJson(json),
    ) as List<UserAddressesModel>;
    print("dataAddress===========>${data.map((e) => e.toJson())}");
    return data;
  }

  Future<List<String>> getListAramexCities(String nameStartsWith) async {
    var data = await PrevGenericHttp<String>(context).callApi(
      name: ApiNames.listAramexCities,
      json: {'NameStartsWith': nameStartsWith},
      returnType: ReturnType.List,
      returnDataFun: (data) {
        if(data["key"]==1){
          return data['data'];
        }
      },
      toJsonFunc: (json) => json.toString(),
      methodType: MethodType.Get,
      showLoader: false,
    ) as List<String>;
    return data;
  }

  /// address
  Future<UserAddressesModel?> addUserAddress(LocationModel model) async {
    print(model.address.toString() + 'modelAddress');
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.addUserAddress,
      json: {
        'Lat': model.lat,
        'Lng': model.lng,
        'Address': model.address,
        'lang': context.read<LangCubit>().state.locale.languageCode,
      },
      returnType: ReturnType.Model,
      methodType: MethodType.Post,
      toJsonFunc: (json) =>
          json == null ? {} : UserAddressesModel.fromJson(json["data"]),
      showLoader: true,
    );
    print("object===>$data");
    return data;
  }

  Future<UserAddressesModel?> updateUserAddress(
      LocationModel model, int addressId) async {
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.updateUserAddress,
      json: {
        'Lat': model.lat,
        'Lng': model.lng,
        'Address': model.address,
        'lang': context.read<LangCubit>().state.locale.languageCode,
        "Id": addressId
      },
      returnType: ReturnType.Model,
      toJsonFunc: (json) =>
          json == null ? {} : UserAddressesModel.fromJson(json["data"]),
      methodType: MethodType.Post,
      showLoader: true,
    );
    log("updated model =====>${data}");
    return data;
  }

  Future<bool> deleteAddress(int id) async {
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      name: ApiNames.deleteAddress,
      json: {
        "UserAddressId": id,
        'lang': context.read<LangCubit>().state.locale.languageCode,
      },
      returnDataFun: (data) => data,
      showLoader: true,
    );
    return data != null;
  }
}
