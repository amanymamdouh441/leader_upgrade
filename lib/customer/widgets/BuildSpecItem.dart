part of 'widgetsImports.dart';

class BuildSpecItem extends StatelessWidget {
  final SubSpecificationModel model;
  final List<int> selected;
  final Function(int id) onClick;

  const BuildSpecItem({
    Key? key,
    required this.model,
    required this.selected,
    required this.onClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool current = selected.contains(model.id);
    return Bounce(
      onPressed: () => onClick(model.id),
      duration: Duration(milliseconds: 200),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(
                color: current ? MyColors.primary : MyColors.greyWhite,
                width: current ? 2 : 1),
            borderRadius: BorderRadius.circular(4)),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Offstage(
              offstage: model.code == null,
              child: Container(
                height: 10,
                width: 10,
                color: Color(int.parse(
                    model.code?.replaceFirst("#", "0xff") ?? "0xff000")),
              ),
            ),
            SizedBox(width: 5),
            MyText(
              title: model.name,
              color: current ? MyColors.primary : MyColors.black,
              size: 10,
              fontWeight: FontWeight.w700,
            ),
          ],
        ),
      ),
    );
  }
}
