part of 'widgetsImports.dart';

class BuildLocationItem extends StatelessWidget {
  final LocationModel locationCubit;
final AddressData addressData;
  const BuildLocationItem({required this.locationCubit,required this.addressData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: MyColors.greyWhite,
            spreadRadius: 2,
            blurRadius: 2,
          )
        ],
      ),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(Res.location, height: 15),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 5),
                child: MyText(
                  title: tr(context, "selectFromMap"),
                  color: MyColors.blackOpacity,
                  size: 9,
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: MyText(
                    title: locationCubit.address,
                    color: MyColors.blackOpacity,
                    size: 9,
                  ),
                ),
              ),
              InkWell(
                onTap: () => addressData.onLocationClick(context),
                child: Container(
                  height: 70,
                  width: 70,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: AssetImage(Res.map),
                      fit: BoxFit.fill,
                    ),
                  ),
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 20,
                    width: 70,
                    decoration: BoxDecoration(
                      color: MyColors.blackOpacity,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: MyText(
                      title: tr(context, "update"),
                      size: 8,
                      color: MyColors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
