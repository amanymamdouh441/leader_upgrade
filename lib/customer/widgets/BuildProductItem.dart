part of 'widgetsImports.dart';

class BuildProductItem extends StatelessWidget {
  final ProductModel model;
  final bool? showButtons;
  final Function()? onTapFav;
  final Function()? onTapCart;
  final Function(ProductModel? model)? onClosed;
  const BuildProductItem({
    this.showButtons,
    this.onTapFav,
    this.onTapCart,
    this.onClosed,
    required this.model,
  });

  @override
  Widget build(BuildContext context) {
    return OpenContainer<ProductModel>(
      closedElevation: 0,
      openElevation: 0,
      closedColor: Colors.transparent,
      openColor: Colors.white,
      middleColor: Colors.transparent,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      onClosed: onClosed,
      openBuilder: (context, action) => ProductDetails(id: model.id),
      closedBuilder: (context, action) => Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        height: 180,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CachedImage(
              height: 150,
              width: MediaQuery.of(context).size.width * 0.4,
              url: model.mainImg,
              bgColor: MyColors.secondary,
              fit: BoxFit.cover,
              placeHolder: SvgPicture.asset(Res.noImage),
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Visibility(
                  visible: showButtons ?? true,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: onTapFav,
                        child: Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: MyColors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Visibility(
                            visible: model.favorite,
                            child: Icon(
                              Icons.favorite,
                              color: Colors.red,
                              size: 15,
                            ),
                            replacement: Icon(
                              Icons.favorite_border,
                              color: MyColors.black,
                              size: 15,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: onTapCart,
                            child: Image.asset(
                              Res.addtocart,
                              height: 30,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
            MyText(
              title: "${model.price} ${tr(context, "sar")}",
              color: MyColors.black,
              size: 10,
            )
          ],
        ),
      ),
    );
  }
}
