part of 'widgetsImports.dart';

class IndicatorDots extends StatelessWidget {
  final double index;
  final int len;
  const IndicatorDots({required this.index, required this.len});

  @override
  Widget build(BuildContext context) {
    if (len<=0) {
      return Container();
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        DotsIndicator(
            position: index,
            dotsCount: len,
            mainAxisAlignment: MainAxisAlignment.center,
            decorator: DotsDecorator(
              activeColor: MyColors.primary.withOpacity(.7),
              color: MyColors.grey,
              size: const Size.square(8.0),
              activeSize: const Size(15.0, 7.0),
              activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
            )),
      ],
    );
    // return BlocBuilder<WelcomeCubit,WelcomeState>(
    //   cubit: welcomeCubit,
    //   builder: (_,state){
    //     return DotsIndicator(
    //       position: state.index.toDouble(),
    //       dotsCount: 3,
    //       decorator: DotsDecorator(
    //         activeColor: MyColors.primary.withOpacity(.7),
    //         color: MyColors.grey,
    //         size: const Size.square(8.0),
    //         activeSize: const Size(15.0, 7.0),
    //         activeShape: RoundedRectangleBorder(
    //           borderRadius: BorderRadius.circular(5.0),
    //         ),
    //       ),
    //     );
    //   },
    // );
  }
}
