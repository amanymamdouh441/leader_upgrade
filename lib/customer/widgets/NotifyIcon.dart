part of 'widgetsImports.dart';

class NotifyIcon extends StatelessWidget {
  const NotifyIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: context.read<AuthCubit>().state.authorized,
      child: BlocBuilder<NotifyCountCubit, NotifyCountState>(
        builder: (context, state) {
          return Stack(
            alignment: Alignment.center,
            children: [
              IconButton(
                icon: Icon(
                  Icons.notifications_none,
                  color: MyColors.black,
                  size: 30,
                ),
                onPressed: () =>
                    AutoRouter.of(context).push(NotificationsRoute()),
              ),
              Positioned(
                top: 25,
                right: 8,
                child: Offstage(
                  offstage: state.count == 0,
                  child: Container(
                    padding: const EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        color: Colors.red.withOpacity(0.9),
                        shape: BoxShape.circle),
                    alignment: Alignment.center,
                    child: MyText(
                      title: "${state.count}",
                      color: MyColors.white,
                      size: 7,
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
