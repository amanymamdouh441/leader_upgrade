
import 'package:flutter/material.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'not_data.dart';

class BuildNoItemFound extends StatelessWidget {
  final String title;
  final String message;

  const BuildNoItemFound({required this.title, required this.message});
  @override
  Widget build(BuildContext context) {
    return NoData(
      title: tr(context,title),
      message: tr(context, message),
    );
  }
}

