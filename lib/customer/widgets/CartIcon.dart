part of 'widgetsImports.dart';

class CartIcon extends StatelessWidget {
  const CartIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      openElevation: 0,
      closedColor: Colors.transparent,
      openColor: Colors.white,
      middleColor: Colors.transparent,
      transitionDuration: Duration(milliseconds: 800),
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (context, action) => Cart(),
      closedBuilder: (context, action) =>
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                height: 30,
                child: Image.asset(Res.cart, width: 25),
              ),
              Positioned(
                top: 10, right: 8,
                child: BlocBuilder<CartCountCubit, CartCountState>(
                  builder: (context, state) {
                    return Visibility(
                      visible: state.count!=0,
                      child: Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: Colors.red.withOpacity(0.9),
                            shape: BoxShape.circle
                        ),
                        alignment: Alignment.center,
                        child: MyText(
                          title: "${state.count}",
                          color: MyColors.white,
                          size: 7,
                        ),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
    );
  }
}
