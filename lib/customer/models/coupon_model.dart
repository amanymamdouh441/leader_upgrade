import 'package:json_annotation/json_annotation.dart';

part 'coupon_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class CouponModel {
  @JsonKey(name: 'coponId')
  int coponId;
  @JsonKey(name: 'discount')
  int discount;
  @JsonKey(name: 'lasttotal')
  num lasttotal;

  CouponModel({
    required this.coponId,
    required this.discount,
    required this.lasttotal,
  });

  factory CouponModel.fromJson(Map<String, dynamic> json) =>
      _$CouponModelFromJson(json);

  Map<String, dynamic> toJson() => _$CouponModelToJson(this);
}
