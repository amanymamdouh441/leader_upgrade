// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sub_specification_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubSpecificationModel _$SubSpecificationModelFromJson(
        Map<String, dynamic> json) =>
    SubSpecificationModel(
      id: json['id'] as int,
      name: json['name'] as String,
      code: json['code'] as String?,
    )..isChecked = json['isChecked'] as bool?;

Map<String, dynamic> _$SubSpecificationModelToJson(
        SubSpecificationModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'code': instance.code,
      'isChecked': instance.isChecked,
    };
