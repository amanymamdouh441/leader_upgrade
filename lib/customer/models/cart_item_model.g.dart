// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartItemModel _$CartItemModelFromJson(Map<String, dynamic> json) =>
    CartItemModel(
      id: json['id'] as int,
      qty: json['qty'] as int,
      productName: json['productName'] as String,
      cats: (json['specifaction'] as List<dynamic>)
          .map((e) => SpecificationModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      price: json['price'] as num,
      img: json['img'] as String,
    );

Map<String, dynamic> _$CartItemModelToJson(CartItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'qty': instance.qty,
      'productName': instance.productName,
      'specifaction': instance.cats,
      'price': instance.price,
      'img': instance.img,
    };
