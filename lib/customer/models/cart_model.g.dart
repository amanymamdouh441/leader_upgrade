// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartModel _$CartModelFromJson(Map<String, dynamic> json) => CartModel(
      cartItems: (json['cartItems'] as List<dynamic>)
          .map((e) => CartItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      total: json['total'] as num,
      shippingFee: json['shippingFee'] as num,
      commission: json['commission'] as num,
    );

Map<String, dynamic> _$CartModelToJson(CartModel instance) => <String, dynamic>{
      'cartItems': instance.cartItems,
      'total': instance.total,
      'shippingFee': instance.shippingFee,
      'commission': instance.commission,
    };
