import 'package:leader_upgrade/customer/models/specification_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cart_item_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class CartItemModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'qty')
  int qty;
  @JsonKey(name: 'productName')
  String productName;
  @JsonKey(name: 'specifaction')
  List<SpecificationModel> cats;
  @JsonKey(name: 'price')
  num price;
  @JsonKey(name: 'img')
  String img;

  CartItemModel({
    required this.id,
    required this.qty,
    required this.productName,
    required this.cats,
    required this.price,
    required this.img,
  });

  factory CartItemModel.fromJson(Map<String, dynamic> json) => _$CartItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$CartItemModelToJson(this);
}
