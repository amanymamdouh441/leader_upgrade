import 'package:leader_upgrade/customer/models/cart_item_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cart_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class CartModel {
  @JsonKey(name: 'cartItems')
  List<CartItemModel> cartItems;
  @JsonKey(name: 'total')
  num total;
  @JsonKey(name: 'shippingFee')
  num shippingFee;
  num commission;

  CartModel({
    required this.cartItems,
    required this.total,
    required this.shippingFee,
    required this.commission
  });

  factory CartModel.fromJson(Map<String, dynamic> json) => _$CartModelFromJson(json);

  Map<String, dynamic> toJson() => _$CartModelToJson(this);
}
