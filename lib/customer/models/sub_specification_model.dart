import 'package:json_annotation/json_annotation.dart'; 

part 'sub_specification_model.g.dart'; 

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class SubSpecificationModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'code')
  String? code;
  @JsonKey(name: 'isChecked')
  bool? isChecked;

  SubSpecificationModel({required this.id, required this.name, required this.code});

  factory SubSpecificationModel.fromJson(Map<String, dynamic> json) => _$SubSpecificationModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubSpecificationModelToJson(this);
}

