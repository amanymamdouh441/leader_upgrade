class UserAddressesModel {
  int ?id  ;
  String? lat;
  String? lng;
  String? name;

  UserAddressesModel({ this.id, this.lat, this.lng, this.name});

  UserAddressesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    lat = json['lat'];
    lng = json['lng'];
    name = json['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['address'] = this.name;
    return data;
  }
  @override
  String toString() {
    // TODO: implement toString
    return name??"";
  }
}
