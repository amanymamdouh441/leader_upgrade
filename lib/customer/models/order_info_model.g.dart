// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderInfoModel _$OrderInfoModelFromJson(Map<String, dynamic> json) =>
    OrderInfoModel(
      name: json['name'] as String,
      fileName: json['fileName'] as String,
      price: json['price'] as num,
      qty: json['qty'] as num,
      subSpecs: (json['subspecifications'] as List<dynamic>)
          .map((e) => SubSpecificationModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      printImage: json['printImage'] as String,
    );

Map<String, dynamic> _$OrderInfoModelToJson(OrderInfoModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'fileName': instance.fileName,
      'price': instance.price,
      'qty': instance.qty,
      'printImage': instance.printImage,
      'subspecifications': instance.subSpecs,
    };
