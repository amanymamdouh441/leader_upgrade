import 'package:leader_upgrade/customer/models/sub_specification_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'specification_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class SpecificationModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'selected', defaultValue: [])
  List<int> selected;
  @JsonKey(name: 'opened', defaultValue: false)
  bool opened;
  @JsonKey(name: 'subSpecifaction')
  List<SubSpecificationModel> subSpecifications;

  SpecificationModel({
    required this.id,
    required this.name,
    required this.selected,
    required this.opened,
    required this.subSpecifications,
  });

  factory SpecificationModel.fromJson(Map<String, dynamic> json) =>
      _$SpecificationModelFromJson(json);

  Map<String, dynamic> toJson() => _$SpecificationModelToJson(this);
}
