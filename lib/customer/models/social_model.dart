import 'package:json_annotation/json_annotation.dart'; 

part 'social_model.g.dart'; 

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class SocialModel {
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'img')
  String img;
  @JsonKey(name: 'url')
  String url;

  SocialModel({required this.name, required this.img, required this.url});

  factory SocialModel.fromJson(Map<String, dynamic> json) => _$SocialModelFromJson(json);

  Map<String, dynamic> toJson() => _$SocialModelToJson(this);
}

