import 'package:leader_upgrade/customer/models/sub_category.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

@JsonSerializable(explicitToJson: true, ignoreUnannotated: false)
class Category {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'img')
  String img;
  // @JsonKey(name: 'categories')
  // List<SubCategory> categories;

  Category({
    required this.id,
    required this.name,
    required this.img,
    // required this.categories,
  });

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
