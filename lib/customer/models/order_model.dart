import 'package:json_annotation/json_annotation.dart';

part 'order_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class OrderModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'total')
  num total;
  @JsonKey(name: 'shippingFee')
  num shippingFee;
  @JsonKey(name: 'countProduct')
  int countProduct;
  @JsonKey(name: 'status')
  String status;
  List<String> imgs;

  OrderModel(
      {required this.id,
      required this.total,
      required this.shippingFee,
      required this.countProduct,
       required this.status,
      required this.imgs});

  factory OrderModel.fromJson(Map<String, dynamic> json) =>
      _$OrderModelFromJson(json);

  Map<String, dynamic> toJson() => _$OrderModelToJson(this);
}
