// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SettingModel _$SettingModelFromJson(Map<String, dynamic> json) => SettingModel(
      phone: json['phone'] as String,
      aboutUs: json['aboutUs'] as String,
      condtions: json['condtions'] as String,
      shippingFee: json['shippingFee'] as num,
      commission: json['commission'] as num,
    );

Map<String, dynamic> _$SettingModelToJson(SettingModel instance) =>
    <String, dynamic>{
      'phone': instance.phone,
      'aboutUs': instance.aboutUs,
      'condtions': instance.condtions,
      'shippingFee': instance.shippingFee,
      'commission': instance.commission,
    };
