import 'package:json_annotation/json_annotation.dart';

part 'sub_category.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class SubCategory {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'img')
  String img;
  @JsonKey(includeIfNull: true,ignore: true,defaultValue: 0,)

  int? parentId;

  SubCategory({
    required this.id,
    required this.name,
    required this.img,
     this.parentId,
  });

  factory SubCategory.fromJson(Map<String, dynamic> json) => _$SubCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$SubCategoryToJson(this);
}
