import 'package:leader_upgrade/customer/models/order_info_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'order_details_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class OrderDetailsModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'total')
  num total;
  @JsonKey(name: 'shippingFee')
  num shippingFee;
  @JsonKey(name: 'commission')
  num commission;
  @JsonKey(name: 'status')
  String status;
  @JsonKey(name: "statusId")
  int statusId;
  @JsonKey(name: 'typePay')
  String typePay;
  @JsonKey(name: 'date')
  String date;
  @JsonKey(name: 'time')
  String time;
  @JsonKey(name: 'info')
  List<OrderInfoModel> orderInfo;
  @JsonKey(name: 'reason')
  String? reason;
  @JsonKey(name: 'invoice')
  String? invoice;
  @JsonKey(name: 'taxNumber')
  String? taxNumber;
  bool canReturned;

  @JsonKey(name: 'canReturned')
  OrderDetailsModel(
      {required this.id,
      required this.total,
      required this.shippingFee,
      required this.commission,
      required this.status,
      required this.statusId,
      required this.typePay,
      required this.date,
      required this.time,
      required this.orderInfo,
      required this.canReturned,
      required this.taxNumber,
      this.reason,
      this.invoice});

  factory OrderDetailsModel.fromJson(Map<String, dynamic> json) =>
      _$OrderDetailsModelFromJson(json);

  Map<String, dynamic> toJson() => _$OrderDetailsModelToJson(this);
}
