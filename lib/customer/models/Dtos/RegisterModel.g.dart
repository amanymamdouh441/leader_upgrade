// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RegisterModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterModel _$RegisterModelFromJson(Map<String, dynamic> json) =>
    RegisterModel(
      userName: json['userName'] as String?,
      email: json['email'] as String?,
      phone: json['phone'] as String?,
      password: json['password'] as String?,
      deviceId: json['deviceId'] as String?,
      InvitationCode: json['InvitationCode'] as String?,
      deviceType: json['deviceType'] as String?,
      projectName: json['projectName'] as String?,
    );

Map<String, dynamic> _$RegisterModelToJson(RegisterModel instance) =>
    <String, dynamic>{
      'userName': instance.userName,
      'email': instance.email,
      'phone': instance.phone,
      'password': instance.password,
      'InvitationCode': instance.InvitationCode,
      'deviceId': instance.deviceId,
      'deviceType': instance.deviceType,
      'projectName': instance.projectName,
    };
