import 'dart:io';

class UpdateUserModel{

  String? userName;
  String? email;
  String? phone;
  File? imgProfile;


  UpdateUserModel({
    this.userName,
    this.email,
    this.phone,
    this.imgProfile,
  });

  Map<String, dynamic> toJson()=>{
    "userName": userName,
    "email": email,
    "phone": phone,
    "imgProfile": imgProfile,
  };

}