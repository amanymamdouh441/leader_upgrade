class AddOrderModel {
  String? lat;
  String? lng;
  String? location;
  int? userAddressId;
  num? total;
  int? typePay;
  num? shippingFee;
  num? commission;
  String? addressDetails;
  String? notes;
  String? userId;
  int? coponId;
  String? cityName;

  AddOrderModel(
      {this.location,
      this.addressDetails,
      this.typePay,
      this.lat,
      this.commission,
      this.userAddressId,
        this.cityName,
      this.userId,
      this.lng,
      this.notes,
      this.shippingFee,
      this.total,
      this.coponId});

  Map<String, dynamic> toJson() => {
        "location": location,
        "addressDetails": addressDetails,
        "typePay": typePay,
        "lat": lat,
        "userId": userId,
        "UserAddressId": userAddressId,
        "lng": lng,
        "notes": notes,
        "shippingFee": shippingFee,
        "total": total,
        "coponId": coponId,
        "commission": commission,
    "CityName":cityName,
      };
}
