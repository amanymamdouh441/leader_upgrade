import 'dart:convert';

class AddCartModel {
  int productId;
  int qty;
  List<int> subIds;

  AddCartModel({
    required this.productId,
    required this.qty,
    required this.subIds,
  });

  Map<String, dynamic> toJson()=>{
    "productId":productId,
    "qty":qty,
    "SubSpecificationIds":json.encode(subIds),
  };

}
