import 'package:flutter/material.dart';

class PriceRangeModel{
  RangeValues initial;
  RangeValues value;

  PriceRangeModel({required this.initial, required this.value});
}