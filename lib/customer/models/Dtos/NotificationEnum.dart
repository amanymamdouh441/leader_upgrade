enum NotificationType {
  BlockUser,
  Order,
  NotiyFromDashBord,
}

extension NotifyValue on NotificationType {
  int getValue() {
    switch (this) {
      case NotificationType.Order:
        return 2;
      case NotificationType.NotiyFromDashBord:
        return 7;
      default:
        return -1;
    }
  }
}
