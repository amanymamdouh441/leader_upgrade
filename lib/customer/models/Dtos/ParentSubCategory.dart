import 'package:leader_upgrade/customer/models/sub_category.dart';

class ParentSubCategory{
  List<SubCategory> subCategory;
  int selectedId;

  ParentSubCategory({required this.subCategory,required this.selectedId});
}