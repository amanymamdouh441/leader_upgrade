import 'dart:io';

class AddPrintOrderModel {
  int? productId;
  num? productPrice;
  String? productName;
  String? productImage;
  String? printNotes;
  String? printProductName;
  String? printName;
  String? printNumber;
  bool? isSpecialPrint;
  bool? isNormalPrint;
  int? qty;
  String? subSpecificationIds;
  String? subSpecificationNames;
  String? lat;
  String? lng;
  String? location;
  num? total;
  int? typePay;
  num? shippingFee;
  String? addressDetails;
  String? notes;
  int? coponId;
  File? printImage;
  String? cityName;

  AddPrintOrderModel({
    this.productId,
    this.productPrice,
    this.productName,
    this.productImage,
    this.printNotes,
    this.printProductName,
    this.printName,
    this.printNumber,
    this.isSpecialPrint,
    this.isNormalPrint,
    this.qty,
    this.subSpecificationIds,
    this.subSpecificationNames,
    this.lat,
    this.lng,
    this.location,
    this.total,
    this.typePay,
    this.shippingFee,
    this.addressDetails,
    this.notes,
    this.coponId,
    this.printImage,
    this.cityName,
  });

  Map<String, dynamic> toJson() => {
        "productId": productId,
        "productPrice": productPrice,
        "productName": productName,
        "printNotes": printNotes,
        "printName": printName,
        "CityName": cityName,
        "printProductName": printProductName,
        "printNumber": printNumber,
        "isSpecialPrint": isSpecialPrint,
        "isNormalPrint": isNormalPrint,
        "qty": qty,
        "SubSpecificationIds": subSpecificationIds,
        "lat": lat,
        "lng": lng,
        "location": location,
        "total": total,
        "typePay": typePay,
        "shippingFee": shippingFee,
        "addressDetails": addressDetails,
        "notes": notes,
        "coponId": coponId,
        "printImage": printImage,
      };
}
