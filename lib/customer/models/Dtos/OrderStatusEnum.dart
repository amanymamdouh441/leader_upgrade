enum OrderStatus {
  New_order,
  Beingprocessed,
  Shipping,
  Finished,
  ReturnRequest,
  Delivered,
}

extension StatusValue on OrderStatus {
  int getValue() {
    switch (this) {
      case OrderStatus.New_order:
        return 1;
      case OrderStatus.Beingprocessed:
        return 2;
      case OrderStatus.Shipping:
        return 3;
      case OrderStatus.Finished:
        return 4;
      case OrderStatus.ReturnRequest:
        return 5;
      case OrderStatus.Delivered:
        return 6;
      default:
        return -1;
    }
  }
}
