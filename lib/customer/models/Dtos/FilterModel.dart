import 'dart:convert';

class FilterModel{
  final int? categoryId;
  final num? priceForm;
  final num? priceTo;
  final List<int> subIds;
  final String? searchTxt;
  final int? typePrice;
  final int? currentPage;
  final bool? refresh;

  FilterModel({
    this.categoryId,
    this.priceTo,
    this.priceForm,
    required this.subIds,
    this.searchTxt,
    this.typePrice,
    this.currentPage,
    this.refresh,
  });


  Map<String, dynamic> toJson()=>{
    "categoryId":categoryId,
    "priceTo":priceTo,
    "priceForm":priceForm,
    "subSpecifactionIds": subIds,
    "searchTxt":searchTxt,
    "typePrice":typePrice,
    "currentPage":currentPage,
  };

}