import 'dart:io';

class AddReturnModel {
  int? orderId;
  int? returnReasonId;
  String? note;
  File? payImg;
  List<File>? productImg;

  AddReturnModel(
      {this.orderId,
      this.returnReasonId,
      this.note,
      this.payImg,
      this.productImg});

  Map<String, dynamic> toJson() => {
        "orderId": orderId,
        "returnReasonId": returnReasonId,
        "note": note,
        "payImg": payImg,
        "prouctImg": productImg,
      };
}
