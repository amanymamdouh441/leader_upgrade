import 'package:json_annotation/json_annotation.dart';

part 'RegisterModel.g.dart';

@JsonSerializable()
class RegisterModel {
  String? userName;
  String? email;
  String? phone;
  String? password;
  String? InvitationCode;
  String? deviceId;
  String? deviceType;
  String? projectName;

  RegisterModel({
    this.userName,
    this.email,
    this.phone,
    this.password,
    this.deviceId,
    this.InvitationCode,
    this.deviceType,
    this.projectName,
  });

  factory RegisterModel.fromJson(Map<String, dynamic> json) =>
      _$RegisterModelFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterModelToJson(this);
}
