// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponModel _$CouponModelFromJson(Map<String, dynamic> json) => CouponModel(
      coponId: json['coponId'] as int,
      discount: json['discount'] as int,
      lasttotal: json['lasttotal'] as num,
    );

Map<String, dynamic> _$CouponModelToJson(CouponModel instance) =>
    <String, dynamic>{
      'coponId': instance.coponId,
      'discount': instance.discount,
      'lasttotal': instance.lasttotal,
    };
