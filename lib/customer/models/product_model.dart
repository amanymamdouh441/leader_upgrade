import 'package:leader_upgrade/customer/models/specification_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'ImageModel.dart';

part 'product_model.g.dart';

@JsonSerializable(explicitToJson: true, ignoreUnannotated: false)
class ProductModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'mainImg')
  String mainImg;
  @JsonKey(name: 'imgs')
  List<ImageModel>? images;
  @JsonKey(name: 'price')
  num price;
  @JsonKey(name: 'favorite')
  bool favorite;
  @JsonKey(name: 'categoryId')
  int categoryId;
  @JsonKey(name: 'typePrint')
  String typePrint;
  @JsonKey(name: 'printCategoryId')
  int printCategoryId;
  @JsonKey(name: 'specifaction',includeIfNull: true,defaultValue: [])
  List<SpecificationModel>?specification;

  ProductModel({
    required this.id,
    required this.name,
    required this.mainImg,
    required this.images,
    required this.price,
    required this.favorite,
    required this.categoryId,
    required this.printCategoryId,
    required this.typePrint,
     this.specification
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) =>
      _$ProductModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductModelToJson(this);
}
