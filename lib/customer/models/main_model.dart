import 'package:leader_upgrade/customer/models/category.dart';
import 'package:json_annotation/json_annotation.dart';

part 'main_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class MainModel {
  @JsonKey(name: 'android')
  String android;
  @JsonKey(name: 'ios')
  String ios;
  @JsonKey(name: "categoryDtos")
  List<Category> cats;

  MainModel({required this.android, required this.ios, required this.cats});

  factory MainModel.fromJson(Map<String, dynamic> json) =>
      _$MainModelFromJson(json);

  Map<String, dynamic> toJson() => _$MainModelToJson(this);
}
