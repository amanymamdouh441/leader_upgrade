// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderModel _$OrderModelFromJson(Map<String, dynamic> json) => OrderModel(
      id: json['id'] as int,
      total: json['total'] as num,
      shippingFee: json['shippingFee'] as num,
      countProduct: json['countProduct'] as int,
      status: json['status'] as String,
      imgs: (json['imgs'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$OrderModelToJson(OrderModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'total': instance.total,
      'shippingFee': instance.shippingFee,
      'countProduct': instance.countProduct,
      'status': instance.status,
      'imgs': instance.imgs,
    };
