import 'package:leader_upgrade/customer/models/sub_specification_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'order_info_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class OrderInfoModel {
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'fileName')
  String fileName;
  @JsonKey(name: 'price')
  num price;
  @JsonKey(name: 'qty')
  num qty;
  String printImage;
  @JsonKey(name: 'subspecifications')
  List<SubSpecificationModel> subSpecs;

  OrderInfoModel({
    required this.name,
    required this.fileName,
    required this.price,
    required this.qty,
    required this.subSpecs,
    required this.printImage
  });

  factory OrderInfoModel.fromJson(Map<String, dynamic> json) =>
      _$OrderInfoModelFromJson(json);

  Map<String, dynamic> toJson() => _$OrderInfoModelToJson(this);
}
