// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductModel _$ProductModelFromJson(Map<String, dynamic> json) => ProductModel(
      id: json['id'] as int,
      name: json['name'] as String,
      mainImg: json['mainImg'] as String,
      images: (json['imgs'] as List<dynamic>?)
          ?.map((e) => ImageModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      price: json['price'] as num,
      favorite: json['favorite'] as bool,
      categoryId: json['categoryId'] as int,
      printCategoryId: json['printCategoryId'] as int,
      typePrint: json['typePrint'] as String,
      specification: (json['specifaction'] as List<dynamic>?)
              ?.map(
                  (e) => SpecificationModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
    );

Map<String, dynamic> _$ProductModelToJson(ProductModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'mainImg': instance.mainImg,
      'imgs': instance.images?.map((e) => e.toJson()).toList(),
      'price': instance.price,
      'favorite': instance.favorite,
      'categoryId': instance.categoryId,
      'typePrint': instance.typePrint,
      'printCategoryId': instance.printCategoryId,
      'specifaction': instance.specification?.map((e) => e.toJson()).toList(),
    };
