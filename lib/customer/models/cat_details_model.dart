import 'package:leader_upgrade/customer/models/specification_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cat_details_model.g.dart'; 

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class CatDetailsModel {
  @JsonKey(name: 'maxPrice')
  num maxPrice;
  @JsonKey(name: 'minPrice')
  num minPrice;
  @JsonKey(name: 'specifaction')
  List<SpecificationModel> specifications;

  CatDetailsModel({required this.maxPrice, required this.minPrice, required this.specifications});

  factory CatDetailsModel.fromJson(Map<String, dynamic> json) => _$CatDetailsModelFromJson(json);

  Map<String, dynamic> toJson() => _$CatDetailsModelToJson(this);
}

