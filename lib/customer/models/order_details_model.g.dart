// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderDetailsModel _$OrderDetailsModelFromJson(Map<String, dynamic> json) =>
    OrderDetailsModel(
      id: json['id'] as int,
      total: json['total'] as num,
      shippingFee: json['shippingFee'] as num,
      commission: json['commission'] as num,
      status: json['status'] as String,
      statusId: json['statusId'] as int,
      typePay: json['typePay'] as String,
      date: json['date'] as String,
      time: json['time'] as String,
      orderInfo: (json['info'] as List<dynamic>)
          .map((e) => OrderInfoModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      canReturned: json['canReturned'] as bool,
      taxNumber: json['taxNumber'] as String?,
      reason: json['reason'] as String?,
      invoice: json['invoice'] as String?,
    );

Map<String, dynamic> _$OrderDetailsModelToJson(OrderDetailsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'total': instance.total,
      'shippingFee': instance.shippingFee,
      'commission': instance.commission,
      'status': instance.status,
      'statusId': instance.statusId,
      'typePay': instance.typePay,
      'date': instance.date,
      'time': instance.time,
      'info': instance.orderInfo,
      'reason': instance.reason,
      'invoice': instance.invoice,
      'taxNumber': instance.taxNumber,
      'canReturned': instance.canReturned,
    };
