// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'specification_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpecificationModel _$SpecificationModelFromJson(Map<String, dynamic> json) =>
    SpecificationModel(
      id: json['id'] as int,
      name: json['name'] as String,
      selected:
          (json['selected'] as List<dynamic>?)?.map((e) => e as int).toList() ??
              [],
      opened: json['opened'] as bool? ?? false,
      subSpecifications: (json['subSpecifaction'] as List<dynamic>)
          .map((e) => SubSpecificationModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SpecificationModelToJson(SpecificationModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'selected': instance.selected,
      'opened': instance.opened,
      'subSpecifaction': instance.subSpecifications,
    };
