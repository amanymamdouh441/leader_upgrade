import 'package:json_annotation/json_annotation.dart'; 

part 'setting_model.g.dart'; 

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class SettingModel {
  @JsonKey(name: 'phone')
  String phone;
  @JsonKey(name: 'aboutUs')
  String aboutUs;
  @JsonKey(name: 'condtions')
  String condtions;
  @JsonKey(name: 'shippingFee')
  num shippingFee;
  @JsonKey(name: 'commission')
  num commission;

  SettingModel({required this.phone,required this.aboutUs,required this.condtions,required this.shippingFee,required this.commission});

   factory SettingModel.fromJson(Map<String, dynamic> json) => _$SettingModelFromJson(json);

   Map<String, dynamic> toJson() => _$SettingModelToJson(this);
}

