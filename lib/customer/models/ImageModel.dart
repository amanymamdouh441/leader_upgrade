import 'package:json_annotation/json_annotation.dart';

part 'ImageModel.g.dart';

@JsonSerializable()
class ImageModel {

  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'img')
  String img;

  ImageModel({required this.id, required this.img});

  factory ImageModel.fromJson(Map<String, dynamic> json) => _$ImageModelFromJson(json);

  Map<String, dynamic> toJson() => _$ImageModelToJson(this);
}