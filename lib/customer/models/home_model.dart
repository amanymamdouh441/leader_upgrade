import 'package:leader_upgrade/customer/models/ImageModel.dart';
import 'package:leader_upgrade/customer/models/product_model.dart';
import 'package:leader_upgrade/customer/models/sub_category.dart';
import 'package:json_annotation/json_annotation.dart';

part 'home_model.g.dart';

@JsonSerializable(explicitToJson: true, ignoreUnannotated: false)
class HomeModel {
  @JsonKey(name: 'sliders')
  List<ImageModel> sliders;
  @JsonKey(name: 'products')
  List<ProductModel> products;
  @JsonKey(name: 'baners')
  List<SubCategory> banars;
  @JsonKey(name: 'countCart')
  int countCart;
  @JsonKey(name: 'countNotify')
  int countNotify;

  HomeModel({
    required this.sliders,
    required this.products,
    required this.banars,
    required this.countCart,
    required this.countNotify,
  });

  factory HomeModel.fromJson(Map<String, dynamic> json) => _$HomeModelFromJson(json);

  Map<String, dynamic> toJson() => _$HomeModelToJson(this);
}
