// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CatDetailsModel _$CatDetailsModelFromJson(Map<String, dynamic> json) =>
    CatDetailsModel(
      maxPrice: json['maxPrice'] as num,
      minPrice: json['minPrice'] as num,
      specifications: (json['specifaction'] as List<dynamic>)
          .map((e) => SpecificationModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CatDetailsModelToJson(CatDetailsModel instance) =>
    <String, dynamic>{
      'maxPrice': instance.maxPrice,
      'minPrice': instance.minPrice,
      'specifaction': instance.specifications,
    };
