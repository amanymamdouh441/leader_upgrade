part of 'share_cubit.dart';

abstract class ShareState extends Equatable {
  final String android;
  final String ios;

  const ShareState(this.android, this.ios);
}

class ShareInitial extends ShareState {
  ShareInitial() : super("", "");

  @override
  List<Object?> get props => [android, ios];
}

class ShareUpdate extends ShareState {
  ShareUpdate(String android, String ios) : super(android, ios);

  @override
  List<Object?> get props => [android,ios];

}
