part of 'sub_cats_cubit.dart';

abstract class SubCatsState extends Equatable {
  final List<SubCategory> subCats;
  final bool changed;
  const SubCatsState(this.subCats,this.changed);
}

class SubCatsInitial extends SubCatsState {
  SubCatsInitial() : super([], false);

  @override
  List<Object> get props => [];
}

class SubCatsUpdated extends SubCatsState{
  SubCatsUpdated(List<SubCategory> subCats, bool changed) : super(subCats, changed);

  @override
  // TODO: implement props
  List<Object?> get props => [subCats,changed];

}