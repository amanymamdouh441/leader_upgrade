import 'package:leader_upgrade/customer/models/sub_category.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'sub_cats_state.dart';

class SubCatsCubit extends Cubit<SubCatsState> {
  SubCatsCubit() : super(SubCatsInitial());

  onUpdateData(List<SubCategory> cats) {
    emit(SubCatsUpdated(cats, !state.changed));
  }
}
