part of 'cart_count_cubit.dart';

abstract class CartCountState extends Equatable {
  final int count;
  const CartCountState(this.count);
}

class CartCountInitial extends CartCountState {
  CartCountInitial() : super(0);

  @override
  List<Object> get props => [count];
}
class CartCountUpdate extends CartCountState {
  CartCountUpdate(int count) : super(count);

  @override
  List<Object> get props => [count];
}
