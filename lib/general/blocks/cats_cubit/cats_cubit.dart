import 'package:leader_upgrade/customer/models/category.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'cats_state.dart';

class CatsCubit extends Cubit<CatsState> {
  CatsCubit() : super(CatsInitial());

  onUpdateData(List<Category> cats){
    emit(CatsUpdateState(cats, !state.changed));
  }

}
