
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomInputTextStyle extends TextStyle{

  final String lang;
  final Color? textColor;

  CustomInputTextStyle({required this.lang,this.textColor});


  @override
  String get fontFamily => lang =="ar"?GoogleFonts.cairo().fontFamily!:GoogleFonts.almarai().fontFamily!;

  @override
  // TODO: implement fontSize
  double get fontSize => lang =="ar"?16:20;

  @override
  // TODO: implement color
  Color get color => textColor??MyColors.black;

}