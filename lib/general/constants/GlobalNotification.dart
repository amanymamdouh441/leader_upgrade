import 'dart:async';
import 'dart:convert';
import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/models/Dtos/NotificationEnum.dart';
import 'package:leader_upgrade/general/blocks/notify_count_cubit/notify_count_cubit.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

class GlobalNotification {
  static StreamController<Map<String, dynamic>> _onMessageStreamController =
  StreamController.broadcast();

  static late FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;
  static late BuildContext context;
  static GlobalNotification instance = new GlobalNotification._();
  static FirebaseMessaging messaging = FirebaseMessaging.instance;

  GlobalNotification._();

  GlobalNotification();

  setupNotification(BuildContext cxt) async {
    context = cxt;
    _flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings("@mipmap/launcher_icon");
    var ios = new DarwinInitializationSettings(
    );
    var initSettings = new InitializationSettings(android: android, iOS: ios);
    _flutterLocalNotificationsPlugin.initialize(
      initSettings,
      onDidReceiveNotificationResponse:(response)=> flutterNotificationClick(response.payload),
      onDidReceiveBackgroundNotificationResponse:(response)=> flutterNotificationClick(response.payload),
    );
    NotificationSettings settings =
    await messaging.requestPermission(provisional: true);
    print('User granted permission: ${settings.authorizationStatus}');
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      messaging.getToken().then((token) {
        print(token);
      });
      messaging.setForegroundNotificationPresentationOptions(
          alert: false, badge: false, sound: false);
      // messaging.getInitialMessage().then((message) => _showLocalNotification(message));
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        print("_____________________Message data:${message.data}");
        print(
            "_____________________notification:${message.notification?.title}");
        _showLocalNotification(message);
        _onMessageStreamController.add(message.data);
        if (int.parse(message.data["type"] ?? "0") == -1) {
          Utils.clearSavedData();
          Phoenix.rebirth(context);
        }
      });
      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        print('A new onMessageOpenedApp event was published!');
        flutterNotificationClick(json.encode(message.data));
      });
      FirebaseMessaging.onBackgroundMessage(
          _firebaseMessagingBackgroundHandler);
    }
  }

  static Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    print("Handling a background message: ${message.data}");
     _showLocalAndroidNotification(message.data);
    //_showLocalNotification(message);
    flutterNotificationClick(json.encode(message.data));
  }
  StreamController<Map<String, dynamic>> get notificationSubject {
    return _onMessageStreamController;
  }

  static _showLocalAndroidNotification(Map<String, dynamic> data) async {
    // if (data == null) return;
    print("===================> $data");
    _flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = AndroidNotificationDetails(
      "${DateTime.now()}",
      "Default",
      priority: Priority.high,
      importance: Importance.max,
      playSound: true,
      shortcutId: DateTime.now().toIso8601String(),
      icon: "@mipmap/launcher_icon",
    );
    var ios = const DarwinNotificationDetails();
    var _platform = NotificationDetails(android: android, iOS: ios);
    _flutterLocalNotificationsPlugin.show(DateTime.now().microsecond,
        "${data["title"]}", "${data["body"]}", _platform,
        payload: json.encode(data));
  }

  // StreamController<Map<String, dynamic>> get notificationSubject {
  //   return _onMessageStreamController;
  // }

  static _showLocalNotification(RemoteMessage? message) async {
    if (message == null) return;
    int count = context.read<NotifyCountCubit>().state.count + 1;
    context.read<NotifyCountCubit>().onUpdateCount(count);
    BigPictureStyleInformation? bigPictureStyleInformation;
    var android = AndroidNotificationDetails(
      "${DateTime.now()}",
      "Default",
      priority: Priority.high,
      importance: Importance.max,
      playSound: true,
      shortcutId: DateTime.now().toIso8601String(),
      icon: "@mipmap/launcher_icon",
      styleInformation: bigPictureStyleInformation,
    );
    var ios = DarwinNotificationDetails();
    var _platform = NotificationDetails(android: android, iOS: ios);
    _flutterLocalNotificationsPlugin.show(
        DateTime.now().microsecond,
        "${message.notification?.title}",
        "${message.notification?.body}",
        _platform,
        payload: json.encode(message.data));
  }

  static Future flutterNotificationClick(String? payload) async {
    print("tttttttttt $payload");
    var _data = json.decode("$payload");
    int _type = int.parse(_data["type"] ?? "7");
    int _orderId = int.parse(_data["orderId"] ?? "0");
    // if(_orderId != 0)
    navigate(context, _type, _orderId);
  }

  static navigate(BuildContext context, int type, int orderId) {
    if (type == NotificationType.Order.getValue()) {
      AutoRouter.of(context).push(OrderDetailsRoute(orderId: orderId, ));
    }
    if (type == NotificationType.NotiyFromDashBord.getValue()) {
      AutoRouter.of(context).push(NotificationsRoute());
    }
    else {
      print("=================navigate================");
    }
  }
}




// import 'dart:async';
// import 'dart:convert';
// import 'dart:developer';
// import 'package:auto_route/auto_route.dart';
// import 'package:dio_helper/dio_helper.dart';
// import 'package:leader_upgrade/customer/models/Dtos/NotificationEnum.dart';
// import 'package:leader_upgrade/general/blocks/notify_count_cubit/notify_count_cubit.dart';
// import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
// import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_phoenix/flutter_phoenix.dart';
//
// class GlobalNotification {
//   static StreamController<Map<String, dynamic>> _onMessageStreamController =
//   StreamController.broadcast();
//
//   static late FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;
//   static late BuildContext context;
//   static GlobalNotification instance = new GlobalNotification._();
//   static FirebaseMessaging messaging = FirebaseMessaging.instance;
//
//   GlobalNotification._();
//
//   GlobalNotification();
//
//   setupNotification(BuildContext cxt) async {
//     context = cxt;
//     _flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
//     var android = new AndroidInitializationSettings("@mipmap/launcher_icon");
//     var ios = DarwinInitializationSettings();
//     var initSettings = InitializationSettings(android: android, iOS: ios);
//     _flutterLocalNotificationsPlugin.initialize(
//       initSettings,
//
//       onDidReceiveNotificationResponse: (response){
//         print("onDidReceiveNotificationResponse");
//         print(response.payload);
//         flutterNotificationClick(response.payload);
//       },
//       onDidReceiveBackgroundNotificationResponse:(response){
//         print("onDidReceiveBackgroundNotificationResponse");
//         print(response.payload);
//         flutterNotificationClick(response.payload);
//       },
//     );
//     //var details = await _flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
//     // if (details!.didNotificationLaunchApp ) {
//     //   print("didnoti ${details.notificationResponse!.payload}");
//     //   flutterNotificationClick(details.notificationResponse!.payload);
//     // }
//
//     NotificationSettings settings =
//     await messaging.requestPermission(provisional: true);
//     print('User granted permission: ${settings.authorizationStatus}');
//     if (settings.authorizationStatus == AuthorizationStatus.authorized) {
//       messaging.getToken().then((token) {
//         print(token);
//       });
//       messaging.setForegroundNotificationPresentationOptions(
//           alert: true, badge: true, sound: true);
//       messaging.getInitialMessage().then((message) => _showLocalNotification(message));
//       FirebaseMessaging.onMessage.listen((RemoteMessage message) {
//         print("_____________________Message data:${message.data}");
//         print(
//             "_____________________notification:${message.notification?.title}");
//         _showLocalNotification(message);
//         _onMessageStreamController.add(message.data);
//         if (int.parse(message.data["type"] ?? "0") == -1) {
//           Utils.clearSavedData();
//           Phoenix.rebirth(context);
//         }
//       });
//
//
//       FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
//         print('A new onMessageOpenedApp event was published!');
//         flutterNotificationClick(json.encode(message.data));
//       });
//       FirebaseMessaging.onBackgroundMessage(
//           _firebaseMessagingBackgroundHandler);
//     }
//   }
//
//   static Future<void> _firebaseMessagingBackgroundHandler(
//       RemoteMessage message) async {
//     print("Handling a background message: ${message.data}");
//     // _showLocalAndroidNotification(message.data);
//   }
//
//   static _showLocalAndroidNotification(Map<String, dynamic> data) async {
//     // if (data == null) return;
//     print("===================> $data");
//     _flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
//     var android = AndroidNotificationDetails(
//       "${DateTime.now()}",
//       "Default",
//       priority: Priority.high,
//       importance: Importance.max,
//       playSound: true,
//       shortcutId: DateTime.now().toIso8601String(),
//     );
//     var ios = const DarwinNotificationDetails();
//     var _platform = NotificationDetails(android: android, iOS: ios);
//     _flutterLocalNotificationsPlugin.show(DateTime.now().microsecond,
//         "${data["title"]}", "${data["body"]}", _platform,
//         payload: json.encode({}));
//   }
//
//   StreamController<Map<String, dynamic>> get notificationSubject {
//     return _onMessageStreamController;
//   }
//
//   _showLocalNotification(RemoteMessage? message) async {
//     if (message == null) {
//       CustomToast.showSimpleToast(msg: "erooooor");
//       return;
//     }
//
//     int count = context
//         .read<NotifyCountCubit>()
//         .state
//         .count + 1;
//     context.read<NotifyCountCubit>().onUpdateCount(count);
//     var android = AndroidNotificationDetails(
//       "${DateTime.now()}",
//       // "Default",
//       "${message.notification?.title}",
//       priority: Priority.high,
//       importance: Importance.max,
//       playSound: true,
//       shortcutId: DateTime.now().toIso8601String(),
//     );
//     var ios = DarwinNotificationDetails();
//     var _platform = NotificationDetails(android: android, iOS: ios);
//       _flutterLocalNotificationsPlugin.show(
//           DateTime
//               .now()
//               .microsecond,
//           "${message.notification?.title}",
//           "${message.notification?.body}",
//           _platform,
//           payload: json.encode(message.data));
//     }
//
//
//
//   // static Future<void> flutterNotificationClick(NotificationResponse? response) async {
//   //   if (context == null || response?.payload == null) return;
//   //   print("sssssssss${response!.payload.toString()}");
//   //   var _data = convertPayload(response!.payload!) ;
//   //   print("notification data $_data");
//   //   print("notification id ${_data["orderId"]}");
//   //   int _type = int.parse(_data["type"] ?? "2");
//   //     int _orderId = int.parse(_data["orderId"] ?? "0");
//   //     print("dddddddddd${_orderId}");
//   //      if(_orderId != 0){
//   //        navigate(context, _type, _orderId);
//   //      }else{
//   //        AutoRouter.of(context).push(HomeRoute());
//   //      }
//   //
//   //   // if (_data.containsKey()){
//   //   //   await Navigator.of(context!).push();
//   //   // }
//   // }
//   //
//   // static dynamic convertPayload(String payload){
//   //   final String _payload = payload.substring(1, payload.length - 1);
//   //   List<String> _split = [];
//   //   _payload.split(",")..forEach((String s) => _split.addAll(s.split(":")));
//   //   var _mapped = {};
//   //   for (int i = 0; i < _split.length + 1; i++) {
//   //     if (i % 2 == 1) _mapped.addAll({_split[i-1].trim().toString(): _split[i].trim()});
//   //   }
//   //   return _mapped;
//   // }
//   static Future flutterNotificationClick(String? response) async {
//     print("tttttttttt ${response}");
//     var _data = json.decode("${response!}");
//     int _type = int.parse(_data["type"] ?? "7" );
//     int _orderId = int.parse(_data["orderId"] ?? "0");
//     print("iddddddddd${_orderId}");
//     // if(_orderId != 0)
//     navigate(context, _type, _orderId);
//   }
//
//   static navigate(BuildContext context, int type, int orderId) {
//     log("jjbhbhbhbhbh$type");
//     log("kkknjnj$orderId");
//     if (type == NotificationType.Order.getValue()) {
//
//       AutoRouter.of(context).push(OrderDetailsRoute(orderId: orderId, ));
//     } else if (type == NotificationType.NotiyFromDashBord.getValue()) {
//       AutoRouter.of(context).push(NotificationsRoute());
//     } else {
//       print("=================navigate================");
//     }
//   }
// }


