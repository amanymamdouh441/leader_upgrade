import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/models/Dtos/AddCartModel.dart';
import 'package:leader_upgrade/customer/models/specification_model.dart';
import 'package:leader_upgrade/general/blocks/auth_cubit/auth_cubit.dart';
import 'package:leader_upgrade/general/utilities/db/db.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/widgets/AuthScaffold.dart';
import 'package:leader_upgrade/general/widgets/HeaderLogo.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';
import 'package:dio_helper/utils/DioUtils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tf_custom_widgets/utils/CustomButtonAnimation.dart';
import 'package:leader_upgrade/general/resources/GeneralRepoImports.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'widgets/LoginWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'LoginData.dart';
part 'Login.dart';
