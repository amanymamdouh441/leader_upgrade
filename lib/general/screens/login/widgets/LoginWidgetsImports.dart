import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/res.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/tf_validator.dart';
import '../LoginImports.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'BuildFormInputs.dart';

part 'BuildForgetText.dart';

part 'BuildLoginButton.dart';

part 'BuildNewRegister.dart';

part 'BuildText.dart';

part 'BuildRegisterButton.dart';

part 'BuildVisitor.dart';
