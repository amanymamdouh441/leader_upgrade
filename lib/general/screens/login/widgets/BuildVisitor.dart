part of 'LoginWidgetsImports.dart';

class BuildVisitor extends StatelessWidget {
final LoginData loginData;

  const BuildVisitor({required this.loginData}) ;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>loginData.enterAsVisitor(context),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        child: MyText(
          title: tr(context,"enterAsVisitor"),
          color: MyColors.primary,
          size: 16,
          decoration: TextDecoration.underline,
          alien: TextAlign.center,
        ),
      ),
    );
  }
}
