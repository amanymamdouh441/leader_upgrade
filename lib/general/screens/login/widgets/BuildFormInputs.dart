part of 'LoginWidgetsImports.dart';

class BuildFormInputs extends StatelessWidget {
  final LoginData loginData;

  const BuildFormInputs({required this.loginData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: loginData.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GenericTextField(
            suffixIcon: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MyText(title: "966+", color: MyColors.black, size: 14),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 7),
                  padding: const EdgeInsets.only(top: 3),
                  child: Image.asset(Res.arabic, scale: 3),
                ),
              ],
            ),
            fieldTypes: FieldTypes.normal,
            label: tr(context, "phone"),
            controller: loginData.phone,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.phone,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey,
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: loginData.showPassword,
            builder: (context, state) {
              return GenericTextField(
                enableBorderColor: MyColors.grey,
                suffixIcon: IconButton(
                  icon: Icon(
                    state.data ? Icons.visibility_off : Icons.visibility,
                  ),
                  onPressed: () =>
                      loginData.showPassword.onUpdateData(!state.data),
                ),
                fieldTypes:
                    state.data ? FieldTypes.password : FieldTypes.normal,
                label: tr(context, "password"),
                controller: loginData.password,
                margin: const EdgeInsets.symmetric(vertical: 10),
                validate: (value) => value!.validateEmpty(context),
                type: TextInputType.text,
                action: TextInputAction.done,
                onSubmit: () => loginData.userLogin(context),
              );
            },
          ),
        ],
      ),
    );
  }
}
