part of 'LoginImports.dart';

class LoginData {
  GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<CustomButtonState> btnKey =
      new GlobalKey<CustomButtonState>();

  final TextEditingController password = new TextEditingController();
  final TextEditingController phone = new TextEditingController();
  final GenericBloc<bool> showPassword = new GenericBloc(true);

  void userLogin(BuildContext context) async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (formKey.currentState!.validate()) {
      String phoneEn = Utils.convertDigitsToLatin(phone.text);
      if (phoneEn.length != 9) {
        Fluttertoast.showToast(
            msg: tr(context, "phoneValidation"),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: DioUtils.primaryColor,
            textColor: Colors.white,
            fontSize: 16.0);
        return;
      }
      btnKey.currentState!.animateForward();

      var cart = await getLocalCart(context);
      await GeneralRepository(context)
          .setUserLogin(phoneEn, password.text, cart);
      var db = context.read<MyDatabase>();
      db.deleteAllProduct();
      btnKey.currentState!.animateReverse();
    }
  }

  Future<String> getLocalCart(BuildContext context) async {
    var db = context.read<MyDatabase>();
    var data = await db.allProductEntries;
    List<AddCartModel> cartItems = data.map((e) {
      var cats = List<SpecificationModel>.from(
          json.decode(e.cats).map((e) => SpecificationModel.fromJson(e)));
      var ids = cats
          .where((e) => e.selected.isNotEmpty)
          .map((e) => e.selected)
          .toList();
      return AddCartModel(
        productId: e.proId,
        qty: e.qty,
        subIds: ids.expand((element) => element).toList(),
      );
    }).toList();
    return json.encode(cartItems);
  }

  void enterAsVisitor(BuildContext context) {
    context.read<AuthCubit>().onUpdateAuth(false);
    AutoRouter.of(context).push(HomeRoute(index: 0));
  }
}
