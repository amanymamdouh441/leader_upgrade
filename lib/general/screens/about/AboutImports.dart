import 'package:leader_upgrade/customer/widgets/widgetsImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:leader_upgrade/general/resources/GeneralRepoImports.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'widgets/AboutWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'About.dart';

part 'AboutData.dart';
