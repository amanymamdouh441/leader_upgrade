import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';

import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/screens/change_password/ChangePasswordImports.dart';
import 'package:flutter/material.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildChangeForm.dart';

part 'BuildSaveButton.dart';
