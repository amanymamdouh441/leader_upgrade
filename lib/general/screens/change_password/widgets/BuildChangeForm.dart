part of 'ChangePassWidgetsImports.dart';

class BuildChangeForm extends StatelessWidget {
  final ChangePasswordData changePasswordData;

  const BuildChangeForm({required this.changePasswordData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: changePasswordData.formKey,
      child: Column(
        children: [
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: changePasswordData.showOldPassword,
            builder: (context, state) {
              return GenericTextField(
                suffixIcon: IconButton(
                  icon: Icon(
                    state.data ? Icons.visibility_off : Icons.visibility,
                  ),
                  onPressed: () => changePasswordData.showOldPassword
                      .onUpdateData(!state.data),
                ),
                fieldTypes:
                    state.data ? FieldTypes.password : FieldTypes.normal,
                label: tr(context, "oldPassword"),
                margin: const EdgeInsets.symmetric(vertical: 10),
                controller: changePasswordData.oldPassword,
                validate: (value) => value!.validateEmpty(context),
                type: TextInputType.text,
                enableBorderColor: MyColors.grey,
                action: TextInputAction.next,
              );
            },
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: changePasswordData.showPassword,
            builder: (context, state) {
              return GenericTextField(
                suffixIcon: IconButton(
                  icon: Icon(
                    state.data ? Icons.visibility_off : Icons.visibility,
                  ),
                  onPressed: () =>
                      changePasswordData.showPassword.onUpdateData(!state.data),
                ),
                fieldTypes:
                    state.data ? FieldTypes.password : FieldTypes.normal,
                label: tr(context, "newPassword"),
                margin: const EdgeInsets.symmetric(vertical: 10),
                controller: changePasswordData.newPassword,
                validate: (value) => value!.validatePassword(context),
                type: TextInputType.text,
                enableBorderColor: MyColors.grey,
                action: TextInputAction.next,
              );
            },
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: changePasswordData.showConfirmPassword,
            builder: (context, state) {
              return GenericTextField(
                margin: const EdgeInsets.symmetric(vertical: 10),
                suffixIcon: IconButton(
                  icon: Icon(
                    state.data ? Icons.visibility_off : Icons.visibility,
                  ),
                  onPressed: () => changePasswordData.showConfirmPassword
                      .onUpdateData(!state.data),
                ),
                fieldTypes:
                    state.data ? FieldTypes.password : FieldTypes.normal,
                label: tr(context, "confirmPassword"),
                controller: changePasswordData.confirmNewPassword,
                validate: (value) => value!.validatePasswordConfirm(context,
                    pass: changePasswordData.newPassword.text),
                type: TextInputType.text,
                enableBorderColor: MyColors.grey,
                action: TextInputAction.done,
              );
            },
          ),
        ],
      ),
    );
  }
}
