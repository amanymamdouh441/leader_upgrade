import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:tf_custom_widgets/utils/CustomButtonAnimation.dart';
import 'package:leader_upgrade/general/screens/change_password/widgets/ChangePassWidgetsImports.dart';
import 'package:leader_upgrade/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'ChangePassword.dart';

part 'ChangePasswordData.dart';
