import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/general/screens/login/LoginImports.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';
import 'package:tf_custom_widgets/utils/CustomButtonAnimation.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/resources/GeneralRepoImports.dart';
import 'package:leader_upgrade/general/screens/reset_password/reset_password_cubit/reset_password_cubit.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/widgets/AuthScaffold.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/general/widgets/HeaderLogo.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'widgets/ResetPasswordWidgetsImports.dart';


part 'ResetPasswordData.dart';

part 'ResetPassword.dart';
