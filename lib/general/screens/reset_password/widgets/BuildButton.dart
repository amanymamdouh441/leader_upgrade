part of 'ResetPasswordWidgetsImports.dart';

class BuildButton extends StatelessWidget {
  final ResetPasswordData resetPasswordData;
  final String userId;

  const BuildButton({required this.resetPasswordData, required this.userId});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        LoadingButton(
          btnKey: resetPasswordData.btnKey,
          title: tr(context,"confirm"),
          onTap: () => resetPasswordData.onResetPassword(context, userId),
          color: MyColors.primary,
          margin: const EdgeInsets.symmetric( vertical: 30),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              title:tr(context,"noReceiveCode"),
              size: 13,
              color: MyColors.grey,
            ),
            InkWell(
              onTap: () => resetPasswordData.onResendCode(context, userId),
              child: MyText(
                  title:tr(context,"sendCode"),
                  size: 13,
                  color: MyColors.primary,
                  decoration: TextDecoration.underline),
            ),
          ],
        ),
      ],
    );
  }
}
