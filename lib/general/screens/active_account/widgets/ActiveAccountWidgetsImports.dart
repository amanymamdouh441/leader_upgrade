import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/screens/active_account/ActiveAccountImports.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:google_fonts/google_fonts.dart';
part 'BuildButtonList.dart';

part 'BuildFormInputs.dart';

part 'BuildText.dart';
