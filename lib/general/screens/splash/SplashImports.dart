import 'package:leader_upgrade/general/constants/GlobalNotification.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../widgets/AnimationContainer.dart';
part 'Splash.dart';
