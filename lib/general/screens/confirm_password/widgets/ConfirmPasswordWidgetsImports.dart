import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/screens/confirm_password/ConfirmPasswordImports.dart';
import 'package:tf_validator/localization/SetLocalization.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tf_validator/tf_validator.dart';

part 'BuildText.dart';

part 'BuildFormInputs.dart';

part 'BuildButtonList.dart';
