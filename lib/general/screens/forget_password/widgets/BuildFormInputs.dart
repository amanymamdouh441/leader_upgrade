part of 'ForgetPasswordWidgetsImports.dart';

class BuildFormInputs extends StatelessWidget {
  final ForgerPasswordData forgerPasswordData;

  const BuildFormInputs({required this.forgerPasswordData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: forgerPasswordData.formKey,
      child: GenericTextField(
        fieldTypes: FieldTypes.normal,
        hint: tr(context, "phone"),
        prefixIcon: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 7),
              padding: const EdgeInsets.only(top: 3),
              child: Image.asset(Res.arabic, scale: 3),
            ),
            MyText(title: "966+", color: MyColors.black, size: 14),
          ],
        ),
        controller: forgerPasswordData.phone,
        action: TextInputAction.done,
        type: TextInputType.number,
        validate: (value) => value!.validatePhone(context),
        onSubmit: () => forgerPasswordData.onForgetPassword(context),
      ),
    );
  }
}
