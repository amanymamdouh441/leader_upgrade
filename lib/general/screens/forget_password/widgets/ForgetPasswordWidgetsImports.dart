import 'package:leader_upgrade/res.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';

import 'package:flutter/material.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/screens/forget_password/ForgetPasswordImports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/tf_validator.dart';

part 'BuildButton.dart';

part 'BuildFormInputs.dart';

part 'BuildText.dart';
