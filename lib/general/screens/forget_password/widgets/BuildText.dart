part of 'ForgetPasswordWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, bottom: 35),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                  title: tr(context, "forgetPassword"),
                  size: 18,
                  color: MyColors.primary,
                ),
                MyText(
                  title: tr(context, "insertPhone"),
                  size: 13,
                  color: MyColors.black.withOpacity(.6),
                ),
              ],
            ),
          ),
          IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.arrow_forward_ios_rounded,
              )),
        ],
      ),
    );
  }
}
