import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:dio_helper/modals/LoadingDialog.dart';
import 'package:tf_custom_widgets/utils/CustomButtonAnimation.dart';
import 'package:leader_upgrade/general/resources/GeneralRepoImports.dart';
import 'package:leader_upgrade/general/widgets/AuthScaffold.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'widgets/ForgetPasswordWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/general/widgets/HeaderLogo.dart';

part 'ForgetPasswordData.dart';
part 'ForgetPassword.dart';
