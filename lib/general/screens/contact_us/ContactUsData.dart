part of 'ContactUsImports.dart';

class ContactUsData {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GenericBloc<List<SocialModel>> socialCubit = GenericBloc([]);
  final TextEditingController name = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController msg = TextEditingController();
  final GenericBloc<String> phoneCubit = GenericBloc("");

  void getPhoneNumber(BuildContext context, {bool refresh = true}) async {
    var data = await GeneralRepository(context).getSetting(refresh);
    phoneCubit.onUpdateData(data.phone);
  }

  fetchSocialData(BuildContext context, {bool refresh = true}) async {
    var data = await GeneralRepository(context).getSocialMedia(refresh);
    socialCubit.onUpdateData(data);
  }
  void callWhatsApp() {
    var phone = phoneCubit.state.data;
    print("_________$phone");
    Utils.callPhone(phone: phone);
  }
  sendContactMsg(BuildContext context) async {
    if (formKey.currentState!.validate()) {
      await GeneralRepository(context).sendMessage(
        name: name.text,
        mail: email.text,
        message: msg.text,
      );
      name.text = "";
      email.text = "";
      msg.text = "";
    }
  }
}
