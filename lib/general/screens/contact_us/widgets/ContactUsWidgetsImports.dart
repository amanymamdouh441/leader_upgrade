import 'package:leader_upgrade/customer/models/social_model.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:leader_upgrade/general/screens/contact_us/ContactUsImports.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/LoadingDialog.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:leader_upgrade/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';

part 'BuildContactUsForm.dart';
part 'BuildSocial.dart';
part 'BuildFloatButton.dart';