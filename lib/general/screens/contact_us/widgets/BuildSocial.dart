part of 'ContactUsWidgetsImports.dart';

class BuildSocial extends StatelessWidget {
  final ContactUsData contactUsData;

  const BuildSocial({Key? key, required this.contactUsData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 30),
      child: Column(
        children: [
          MyText(
            title: tr(context, "orVia"),
            color: MyColors.black,
            size: 12,
          ),
          SizedBox(height: 15),
          BlocBuilder<GenericBloc<List<SocialModel>>,
              GenericState<List<SocialModel>>>(
            bloc: contactUsData.socialCubit,
            builder: (context, state) {
              if (state is GenericUpdateState) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(state.data.length, (index) {
                    return InkWell(
                      onTap: ()=> Utils.launchURL(url: state.data[index].url),
                      child: Container(
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        child: CachedImage(
                          url: state.data[index].img,
                          height: 30,
                          width: 30,
                        ),
                      ),
                    );
                  }),
                );
              }
              return Container(
                height: 80,
                child: LoadingDialog.showLoadingView(),
              );
            },
          )
        ],
      ),
    );
  }
}
