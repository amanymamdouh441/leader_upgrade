part of 'ContactUsWidgetsImports.dart';
class BuildFloatButton extends StatelessWidget {
final ContactUsData contactUsData;

  const BuildFloatButton({required this.contactUsData}) ;
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: Colors.green,
      child: Icon(Icons.phone,color: MyColors.white),
      onPressed: () => contactUsData.callWhatsApp(),
    );
  }
}
