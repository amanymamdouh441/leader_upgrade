part of 'ContactUsImports.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final ContactUsData contactUsData = ContactUsData();

  @override
  void initState() {
    contactUsData.fetchSocialData(context, refresh: false);
    contactUsData.fetchSocialData(context);
    contactUsData.getPhoneNumber(context, refresh: false);
    contactUsData.getPhoneNumber(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(
        title: tr(context, "contactUs"),
        actions: [NotifyIcon()],
      ),
      body: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        children: [
          HeaderLogo(),
          BuildContactUsForm(contactUsData: contactUsData),
          DefaultButton(
            title: tr(context, "send"),
            onTap: () => contactUsData.sendContactMsg(context),
            borderRadius: BorderRadius.circular(30),
          ),
          BuildSocial(
            contactUsData: contactUsData,
          )
        ],
      ),
      floatingActionButton: BuildFloatButton(contactUsData: contactUsData),
    );
  }
}
