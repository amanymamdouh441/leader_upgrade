part of 'SelectLangImports.dart';
class SelectLangData{
  GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();

  void setUserLang(BuildContext context, String lang) async {
    await Utils.changeLanguage(lang, context);
    await CustomerRepository(context).getCategories(true);
    AutoRouter.of(context).push(HomeRoute());
  }

  Future<bool> onBackPressed() async {
    SystemNavigator.pop();
    return true;
  }

}