import 'package:auto_route/auto_route.dart';
import 'package:leader_upgrade/customer/resources/CustomerRepoImports.dart';
import 'package:leader_upgrade/general/utilities/routers/RouterImports.gr.dart';
import 'package:leader_upgrade/general/utilities/utils_functions/UtilsImports.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'widgets/SelectLangWidgetsImports.dart';
import 'package:leader_upgrade/general/widgets/AuthScaffold.dart';
import 'package:leader_upgrade/general/widgets/HeaderLogo.dart';

part 'SelectLangData.dart';

part 'SelectLang.dart';
