import 'package:tf_validator/localization/SetLocalization.dart';
import 'package:flutter/material.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/tf_validator.dart';
import '../SelectLangImports.dart';

part 'BuildButtonList.dart';
part 'BuildLangText.dart';