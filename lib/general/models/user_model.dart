import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable(nullable: false, ignoreUnannotated: false)
class UserModel {
  @JsonKey(name: 'id')
  String id;
  @JsonKey(name: 'userName')
  String userName;
  @JsonKey(name: 'email')
  String email;
  @JsonKey(name: 'phone')
  String phone;
  @JsonKey(name: 'lang')
  String lang;
  @JsonKey(name: 'closeNotify')
  bool closeNotify;
  @JsonKey(name: 'status')
  bool status;
  @JsonKey(name: 'imgProfile')
  String imgProfile;
  @JsonKey(name: 'token')
  String token;
  @JsonKey(name: 'expiration')
  String expiration;
  @JsonKey(name: 'typeUser')
  int typeUser;
  @JsonKey(name: 'code')
  int code;
  @JsonKey(name: 'invitationCode')
  String? invitationCode;

  UserModel({
    required this.id,
    required this.userName,
    required this.email,
    required this.phone,
    required this.lang,
    required this.closeNotify,
    required this.status,
    required this.imgProfile,
    required this.token,
    required this.expiration,
    required this.typeUser,
    required this.code,
    this.invitationCode,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
