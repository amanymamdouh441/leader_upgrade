// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      id: json['id'] as String,
      userName: json['userName'] as String,
      email: json['email'] as String,
      phone: json['phone'] as String,
      lang: json['lang'] as String,
      closeNotify: json['closeNotify'] as bool,
      status: json['status'] as bool,
      imgProfile: json['imgProfile'] as String,
      token: json['token'] as String,
      expiration: json['expiration'] as String,
      typeUser: json['typeUser'] as int,
      code: json['code'] as int,
      invitationCode: json['invitationCode'] as String?,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'id': instance.id,
      'userName': instance.userName,
      'email': instance.email,
      'phone': instance.phone,
      'lang': instance.lang,
      'closeNotify': instance.closeNotify,
      'status': instance.status,
      'imgProfile': instance.imgProfile,
      'token': instance.token,
      'expiration': instance.expiration,
      'typeUser': instance.typeUser,
      'code': instance.code,
      'invitationCode': instance.invitationCode,
    };
