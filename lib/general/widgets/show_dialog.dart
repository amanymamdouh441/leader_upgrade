import 'dart:io';

import 'package:leader_upgrade/customer/screens/return/ReturnImports.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

Future showAlertDialog({context, required ReturnData returnData}) async {
  if (!Platform.isIOS) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextButton(
                  child: Text(tr(context, 'camera')),
                  onPressed: () {
                    returnData.setBillImage(
                      ImageSource.camera,
                    );
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: Text(tr(context, 'gallery')),
                  onPressed: () {
                    returnData.setBillImage(
                      ImageSource.gallery,
                    );
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(tr(context, 'cancel')),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  } else {
    print('object');
    showCupertinoDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  TextButton(
                    child: Text(tr(context, 'camera')),
                    onPressed: () {
                      returnData.setBillImage(
                        ImageSource.camera,
                      );
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    child: Text(tr(context, 'gallery')),
                    onPressed: () {
                      returnData.setBillImage(
                        ImageSource.gallery,
                      );
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text(tr(context, 'cancel')),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
}
