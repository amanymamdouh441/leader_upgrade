import 'package:flutter/material.dart';
import 'package:leader_upgrade/res.dart';

class HeaderLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30),
      child: Image(
        height: 160,
        image: AssetImage(Res.logo),
        fit: BoxFit.contain,
      ),
    );
  }
}
