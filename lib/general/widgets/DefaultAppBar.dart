import 'package:flutter/material.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class DefaultAppBar extends PreferredSize {
  final String title;
  final Widget? leading;
  final List<Widget> actions;
  final Size preferredSize ;

  DefaultAppBar({
    required this.title,
    this.actions = const [],
    this.leading,
    this.preferredSize = const Size.fromHeight(kToolbarHeight + 5),
  }) : super(child: Container(),preferredSize: preferredSize);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: MyText(
        title: "$title",
        size: 14,
        color: MyColors.black,
        fontWeight: FontWeight.bold,
      ),
      centerTitle: false,
      backgroundColor: MyColors.white,
      leadingWidth: leading == null ? 50 : 10,
      elevation: 1,
      leading: leading ??
          IconButton(
            icon: Icon(
              Icons.arrow_back_ios_sharp,
              size: 23,
              color: MyColors.black,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
      actions: actions,
    );
  }
}
