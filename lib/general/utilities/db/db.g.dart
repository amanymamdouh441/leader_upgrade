// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'db.dart';

// ignore_for_file: type=lint
class $ProductTableTable extends ProductTable
    with TableInfo<$ProductTableTable, ProductTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ProductTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, true,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _proIdMeta = const VerificationMeta('proId');
  @override
  late final GeneratedColumn<int> proId = GeneratedColumn<int>(
      'pro_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _catsMeta = const VerificationMeta('cats');
  @override
  late final GeneratedColumn<String> cats = GeneratedColumn<String>(
      'cats', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _qtyMeta = const VerificationMeta('qty');
  @override
  late final GeneratedColumn<int> qty = GeneratedColumn<int>(
      'qty', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _imageMeta = const VerificationMeta('image');
  @override
  late final GeneratedColumn<String> image = GeneratedColumn<String>(
      'image', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _priceMeta = const VerificationMeta('price');
  @override
  late final GeneratedColumn<double> price = GeneratedColumn<double>(
      'price', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, proId, name, cats, qty, image, price];
  @override
  String get aliasedName => _alias ?? 'product_table';
  @override
  String get actualTableName => 'product_table';
  @override
  VerificationContext validateIntegrity(Insertable<ProductTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('pro_id')) {
      context.handle(
          _proIdMeta, proId.isAcceptableOrUnknown(data['pro_id']!, _proIdMeta));
    } else if (isInserting) {
      context.missing(_proIdMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('cats')) {
      context.handle(
          _catsMeta, cats.isAcceptableOrUnknown(data['cats']!, _catsMeta));
    } else if (isInserting) {
      context.missing(_catsMeta);
    }
    if (data.containsKey('qty')) {
      context.handle(
          _qtyMeta, qty.isAcceptableOrUnknown(data['qty']!, _qtyMeta));
    } else if (isInserting) {
      context.missing(_qtyMeta);
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image']!, _imageMeta));
    } else if (isInserting) {
      context.missing(_imageMeta);
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price']!, _priceMeta));
    } else if (isInserting) {
      context.missing(_priceMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ProductTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ProductTableData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id']),
      proId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}pro_id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      cats: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}cats'])!,
      qty: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}qty'])!,
      image: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}image'])!,
      price: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}price'])!,
    );
  }

  @override
  $ProductTableTable createAlias(String alias) {
    return $ProductTableTable(attachedDatabase, alias);
  }
}

class ProductTableData extends DataClass
    implements Insertable<ProductTableData> {
  final int? id;
  final int proId;
  final String name;
  final String cats;
  final int qty;
  final String image;
  final double price;
  const ProductTableData(
      {this.id,
      required this.proId,
      required this.name,
      required this.cats,
      required this.qty,
      required this.image,
      required this.price});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    map['pro_id'] = Variable<int>(proId);
    map['name'] = Variable<String>(name);
    map['cats'] = Variable<String>(cats);
    map['qty'] = Variable<int>(qty);
    map['image'] = Variable<String>(image);
    map['price'] = Variable<double>(price);
    return map;
  }

  ProductTableCompanion toCompanion(bool nullToAbsent) {
    return ProductTableCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      proId: Value(proId),
      name: Value(name),
      cats: Value(cats),
      qty: Value(qty),
      image: Value(image),
      price: Value(price),
    );
  }

  factory ProductTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ProductTableData(
      id: serializer.fromJson<int?>(json['id']),
      proId: serializer.fromJson<int>(json['proId']),
      name: serializer.fromJson<String>(json['name']),
      cats: serializer.fromJson<String>(json['cats']),
      qty: serializer.fromJson<int>(json['qty']),
      image: serializer.fromJson<String>(json['image']),
      price: serializer.fromJson<double>(json['price']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int?>(id),
      'proId': serializer.toJson<int>(proId),
      'name': serializer.toJson<String>(name),
      'cats': serializer.toJson<String>(cats),
      'qty': serializer.toJson<int>(qty),
      'image': serializer.toJson<String>(image),
      'price': serializer.toJson<double>(price),
    };
  }

  ProductTableData copyWith(
          {Value<int?> id = const Value.absent(),
          int? proId,
          String? name,
          String? cats,
          int? qty,
          String? image,
          double? price}) =>
      ProductTableData(
        id: id.present ? id.value : this.id,
        proId: proId ?? this.proId,
        name: name ?? this.name,
        cats: cats ?? this.cats,
        qty: qty ?? this.qty,
        image: image ?? this.image,
        price: price ?? this.price,
      );
  @override
  String toString() {
    return (StringBuffer('ProductTableData(')
          ..write('id: $id, ')
          ..write('proId: $proId, ')
          ..write('name: $name, ')
          ..write('cats: $cats, ')
          ..write('qty: $qty, ')
          ..write('image: $image, ')
          ..write('price: $price')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, proId, name, cats, qty, image, price);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProductTableData &&
          other.id == this.id &&
          other.proId == this.proId &&
          other.name == this.name &&
          other.cats == this.cats &&
          other.qty == this.qty &&
          other.image == this.image &&
          other.price == this.price);
}

class ProductTableCompanion extends UpdateCompanion<ProductTableData> {
  final Value<int?> id;
  final Value<int> proId;
  final Value<String> name;
  final Value<String> cats;
  final Value<int> qty;
  final Value<String> image;
  final Value<double> price;
  const ProductTableCompanion({
    this.id = const Value.absent(),
    this.proId = const Value.absent(),
    this.name = const Value.absent(),
    this.cats = const Value.absent(),
    this.qty = const Value.absent(),
    this.image = const Value.absent(),
    this.price = const Value.absent(),
  });
  ProductTableCompanion.insert({
    this.id = const Value.absent(),
    required int proId,
    required String name,
    required String cats,
    required int qty,
    required String image,
    required double price,
  })  : proId = Value(proId),
        name = Value(name),
        cats = Value(cats),
        qty = Value(qty),
        image = Value(image),
        price = Value(price);
  static Insertable<ProductTableData> custom({
    Expression<int>? id,
    Expression<int>? proId,
    Expression<String>? name,
    Expression<String>? cats,
    Expression<int>? qty,
    Expression<String>? image,
    Expression<double>? price,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (proId != null) 'pro_id': proId,
      if (name != null) 'name': name,
      if (cats != null) 'cats': cats,
      if (qty != null) 'qty': qty,
      if (image != null) 'image': image,
      if (price != null) 'price': price,
    });
  }

  ProductTableCompanion copyWith(
      {Value<int?>? id,
      Value<int>? proId,
      Value<String>? name,
      Value<String>? cats,
      Value<int>? qty,
      Value<String>? image,
      Value<double>? price}) {
    return ProductTableCompanion(
      id: id ?? this.id,
      proId: proId ?? this.proId,
      name: name ?? this.name,
      cats: cats ?? this.cats,
      qty: qty ?? this.qty,
      image: image ?? this.image,
      price: price ?? this.price,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (proId.present) {
      map['pro_id'] = Variable<int>(proId.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (cats.present) {
      map['cats'] = Variable<String>(cats.value);
    }
    if (qty.present) {
      map['qty'] = Variable<int>(qty.value);
    }
    if (image.present) {
      map['image'] = Variable<String>(image.value);
    }
    if (price.present) {
      map['price'] = Variable<double>(price.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ProductTableCompanion(')
          ..write('id: $id, ')
          ..write('proId: $proId, ')
          ..write('name: $name, ')
          ..write('cats: $cats, ')
          ..write('qty: $qty, ')
          ..write('image: $image, ')
          ..write('price: $price')
          ..write(')'))
        .toString();
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(e);
  late final $ProductTableTable productTable = $ProductTableTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [productTable];
}
