import 'package:drift/drift.dart';

class ProductTable extends Table {
  IntColumn get id => integer().nullable().autoIncrement()();
  IntColumn get proId => integer()();
  TextColumn get name => text()();
  TextColumn get cats => text()();
  IntColumn get qty => integer()();
  TextColumn get image => text()();
  RealColumn get price => real()();

}