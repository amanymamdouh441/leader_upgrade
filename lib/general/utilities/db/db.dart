import 'package:leader_upgrade/general/utilities/db/ProductTable.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:drift/drift.dart';
import 'dart:io';

part 'db.g.dart';


LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return NativeDatabase(file);
  });
}

@DriftDatabase(tables: [ProductTable])
class MyDatabase extends _$MyDatabase {
  // we tell the database where to store the data with this constructor
  MyDatabase() : super(_openConnection());

  Future<List<ProductTableData>> get allProductEntries => select(productTable).get();

  Future<ProductTableData> productById(int id) {
    return (select(productTable)..where((t) => t.proId.equals(id))).getSingle();
  }

  Future updateProduct(ProductTableData entry) {
    return update(productTable).replace(entry);
  }

  Future deleteProduct(int id) {
    return (delete(productTable)..where((t) => t.proId.equals(id))).go();
  }
  Future deleteAllProduct() {
    return (delete(productTable)).go();
  }

  Future<int> addProduct(ProductTableCompanion entry) {
    return into(productTable).insert(entry);
  }

  @override
  int get schemaVersion => 1;
}