class ApiNames {
  static const String baseUrl = "https://matjarlidar.ip4s.com/api/v1/";
  static const String branch = "1";

  static const String aboutApp = "AboutUs";
  static const String terms = "Condtions";
  static const String login = "login";
  static const String logout = "logout";
  static const String register = "RegisterClient";
  static const String deleteAccount = "RemoveAccount";
  static const String sendCode = "ConfirmCodeRegister";
  static const String resendCode = "ResendCode";
  static const String repeatedQuestions = "FrequentlyAskedQuestions";
  static const String socials = "ListSocialMedia";
  static const String switchNotify = "SwitchNotify";
  static const String forgetPassword = "ForgetPassword";
  static const String resetPassword = "ChangePasswordByCode";
  static const String contactUs = "Addcomplaints";
  static const String updateProfile = "UpdateDataUser";
  static const String changeLang = "ChangeLanguage";
  static const String checkActive = "CheckActiveUser";
  static const String useCoupon = "UseCopon";

  static const String changePass = "ChangePassward";
  static const String categories = "ListCategories";
  static const String subCategories = "ListSubCategories";
  static const String printCategories = "ListPrintCategories";
  static const String printProducts = "ListPrintProductbyCategory";
  static const String addPrintOrder = "AddSpecialPrintOrderAsync";

  static const String home = "Index";
  static const String favProducts = "ListFavourite";
  static const String visitedProducts = "ListShown";
  static const String getListSpecification = "GetListSpecification";
  static const String getProSpecification = "GetProductDetails";
  static const String getCatProducts = "ListProductbyCategory";
  static const String getCatProductsCount = "CountProductsbyCategory";
  static const String addFavourite = "AddFavourite";
  static const String checkCart = "CheckOrderAsync";
  static const String wallet = "Wallet";

  static const String addToCartAsync = "AddToCartAsync";
  static const String removeItemFromCart = "RemoveItemFromCart";
  static const String removeAllCart = "RemoveAllItemFromCart";
  static const String changeCartQty = "ChangeCartItemQtyAsync";
  static const String changeCartSpecifications = "ChangeCartSpecificationAsync";
  static const String listCart = "ListCart";
  static const String addOrder = "AddOrderAsync";

  static const String getAllOrders = "ListAllOrders";
  static const String getNewOrders = "ListNewOrders";
  static const String getProgressOrders = "ListBeingprocessedOrders";
  static const String getShippingOrders = "ListShippingOrders";
  static const String getFinishedOrders = "ListFinishedOrders";
  static const String getReturnOrders = "ListReturnsOrders";
  static const String getOrderDetails = "getOrderDetails";
  static const String getReturnReason = "ListReturnReasons";
  static const String addReturn = "AddReturnRequestAsync";
  static const String listOfNotify = "ListOfNotify";

  static const String getSetting = "GetSetting";

  static const String listOfUserAddresses = "ListUserAddress";
  static const String addUserAddress = "AddUserAddress";

  static const String updateUserAddress = "EditUserAddress";
  static const String deleteAddress = "DeleteUserAddress";

  static const String listAramexCities = "ListAramexCities";
}
