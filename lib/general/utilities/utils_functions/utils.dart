part of 'UtilsImports.dart';

class Utils {
  static Future<void> manipulateSplashData(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var lang = prefs.getString("lang");
    print(prefs.getString('user').toString() + 'uuuuu');
    initDio(context, lang: lang ?? "ar");
    initCustomWidgets(language: lang ?? "ar");
    await CustomerRepository(context).getCategories(true);
    var strUser = prefs.get("user");
    if (strUser != null) {
      UserModel data = UserModel.fromJson(json.decode("$strUser"));
      GlobalState.instance.set("token", data.token);
      changeLanguage(lang ?? "ar", context);
      var active = await CustomerRepository(context).checkActive(data.phone);
      if (active) {
        setCurrentUserData(data, context);
      } else {
        navigateToiVisitor(context, lang);
      }
    } else {
      navigateToiVisitor(context, lang);
    }
  }

  static navigateToiVisitor(BuildContext context, String? lang) async {
    context.read<AuthCubit>().onUpdateAuth(false);
    changeLanguage(lang ?? "ar", context);
    var db = context.read<MyDatabase>();
    var data = await db.allProductEntries;
    context.read<CartCountCubit>().onUpdateCount(data.length);
    if (lang == "null" || lang == null) {
      AutoRouter.of(context).push(SelectLangRoute());
    } else {
      AutoRouter.of(context).push(HomeRoute());
    }
  }

  static initDio(BuildContext context, {required String lang}) {
    DioUtils.init(
        baseUrl: ApiNames.baseUrl,
        style: CustomInputTextStyle(lang: lang),
        primary: MyColors.primary,
        authLink: LoginRoute.name,
        language: lang,
        dismissFunc: EasyLoading.dismiss,
        branchKey: "branchId",
        authBack: tr(context, "back"),
        authConfirm: tr(context, "enter"),
        authSentence: tr(context, "loginFirst"),
        branch: ApiNames.branch,
        showLoadingFunc: LoadingDialog.showLoadingDialog,
        authClick: () => AutoRouter.of(context).popAndPush(LoginRoute()));
  }

  static initCustomWidgets({required String language}) {
    WidgetUtils.init(
      style: CustomInputTextStyle(lang: language),
      primary: MyColors.primary,
      language: language,
      inputStyle: (
          {enableColor,
            fillColor,
            focusBorderColor,
            hint,
            hintColor,
            hintSize,
            label,
            padding,
            prefixIcon,
            prefixWidget,
            radius,
            suffixIcon,
            suffixWidget}) =>
          CustomInputDecoration(
              focusColor: focusBorderColor,
              hintColor: hintColor,
              lang: language,
              labelTxt: label,
              hint: hint,
              prefixIcon: prefixIcon,
              suffixIcon: suffixIcon,
              enableColor: enableColor,
              customFillColor: fillColor,
              padding: padding),
    );
  }

  static Future<bool> manipulateLoginData(
      BuildContext context, dynamic data, String token) async {
    if (data != null && data?["status"] != null) {
      if (data["status"]) {
        await Utils.setDeviceId("$token");
        UserModel user = UserModel.fromJson(data);
        user.token = data["token"];
        user.lang = context.read<LangCubit>().state.locale.languageCode;
        GlobalState.instance.set("token", user.token);
        await Utils.saveUserData(user);
        context.read<UserCubit>().onUpdateUserData(user);
        context.read<AuthCubit>().onUpdateAuth(true);
        AutoRouter.of(context).push(HomeRoute(index: 0));
      } else {
        AutoRouter.of(context).push(ActiveAccountRoute(userId: data["id"]));
      }
      return true;
    }
    return false;
  }

  static void setCurrentUserData(UserModel model, BuildContext context) async {
    context.read<UserCubit>().onUpdateUserData(model);
    context.read<AuthCubit>().onUpdateAuth(true);
    AutoRouter.of(context).push(HomeRoute(index: 0));
  }

  static Future<void> saveUserData(UserModel model) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("user", json.encode(model.toJson()));
  }

  static Future<void> changeLanguage(String lang, BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("lang", lang);
    initCustomWidgets(language: lang);
    initDio(context, lang: lang);
    DioUtils.lang = lang;
    WidgetUtils.lang = lang;
    context.read<LangCubit>().onUpdateLanguage(lang);
  }

  static UserModel getSavedUser({required BuildContext context}) {
    return context.read<UserCubit>().state.model!;
  }

  static Future<String?> getDeviceId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("deviceId");
  }

  static Future<void> setDeviceId(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("deviceId", token);
  }

  static Future<void> clearSavedData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  static String getCurrentUserId({required BuildContext context}) {
    var provider = context.watch<UserCubit>().state.model;
    return provider!.id;
  }

  static void launchURL({required String url}) async {
    if (!url.toString().startsWith("https")) {
      url = "https://" + url;
    }
    await launch(url);
  }

  static void launchWhatsApp(phone) async {
    String message = 'مرحبا بك';
    if (phone.startsWith("00966")) {
      phone = phone.substring(5);
    }
    var _whatsAppUrl = "whatsapp://send?phone=+966$phone&text=$message";
    print(_whatsAppUrl);
    if (await canLaunch(_whatsAppUrl)) {
      await launch(_whatsAppUrl);
    } else {
      throw 'حدث خطأ ما';
    }
  }

  static void launchYoutube({required String url}) async {
    if (Platform.isIOS) {
      if (await canLaunch('$url')) {
        await launch('$url', forceSafariVC: false);
      } else {
        if (await canLaunch('$url')) {
          await launch('$url');
        } else {
          throw 'Could not launch $url';
        }
      }
    } else {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }
  }

  static void callPhone({phone}) async {
    await launch("tel:$phone");
  }

  static void sendMail(mail) async {
    await launch("mailto:$mail");
  }

  static void shareApp(url) {
    LoadingDialog.showLoadingDialog();
    Share.share(url).whenComplete(() {
      EasyLoading.dismiss();
    });
  }

  static Future<File?> getImage() async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      File imageFile = File(image.path);
      return imageFile;
    }
    return null;
  }

  static Future<File?> getReturnImage(
    ImageSource imageSource,
  ) async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(source: imageSource);
    // Navigator.pop(context);
    if (image != null) {
      File imageFile = File(image.path);
      return imageFile;
    }
    return null;
  }

  static Future<List<File>> getImages() async {
    final ImagePicker _picker = ImagePicker();
    final List<XFile>? result = await _picker.pickMultiImage();
    if (result != null) {
      List<File> files = result.map((e) => File(e.path)).toList();
      return files;
    } else {
      return [];
    }
  }

  static Future<File?> getVideo() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? video = await _picker.pickVideo(source: ImageSource.gallery);
    if (video != null) {
      File imageFile = File(video.path);
      return imageFile;
    }
    return null;
  }

  static void copToClipboard(
      {required String text, required GlobalKey<ScaffoldState> scaffold}) {
    if (text.trim().isEmpty) {
      CustomToast.showToastNotification("لا يوجد بيانات للنسخ");
      return;
    } else {
      Clipboard.setData(ClipboardData(text: "$text")).then((value) {
        CustomToast.showToastNotification("تم النسخ بنجاح");
      });
    }
  }

  static Future<Position?> getCurrentLocation() async {
    bool serviceEnabled;
    LocationPermission permission;
    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      CustomToast.showSimpleToast(msg: 'Location services are disabled');
      return null;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        CustomToast.showSimpleToast(msg: 'Location permissions are denied');
        return null;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      CustomToast.showSimpleToast(
          msg:
              'Location permissions are permanently denied, we cannot request permissions');
      return null;
    }

    return await Geolocator.getCurrentPosition();
  }

  static void navigateToMapWithDirection(
      {required String lat,
      required String lng,
      required BuildContext context}) async {
    if (lat == "0") return;
    try {
      final coords = Coords(double.parse(lat), double.parse(lng));
      final title = "Destination";
      final availableMaps = await MapLauncher.installedMaps;

      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Container(
                child: Wrap(
                  children: <Widget>[
                    for (var map in availableMaps)
                      ListTile(
                        onTap: () => map.showMarker(
                          coords: coords,
                          title: title,
                        ),
                        title: Text(map.mapName),
                        leading: SvgPicture.asset(
                          map.icon,
                          height: 30.0,
                          width: 30.0,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    } catch (e) {
      CustomToast.showSimpleToast(msg: "$e");
    }
  }

  static Future<String> getAddress(LatLng latLng, BuildContext context) async {
    try {
      List<Placemark> address =
          await placemarkFromCoordinates(latLng.latitude, latLng.longitude);
      var data =
          "${address[0].country ?? ""}  ${address[0].locality ?? ""}  ${address[0].subLocality ?? ""}  ${address[0].street ?? ""}";
      print(data);
      return data;
    } catch (e) {
      return "";
    }
  }

  static String convertDigitsToLatin(String s) {
    var sb = new StringBuffer();
    for (int i = 0; i < s.length; i++) {
      switch (s[i]) {
        //Arabic digits
        case '\u0660':
          sb.write('0');
          break;
        case '\u0661':
          sb.write('1');
          break;
        case '\u0662':
          sb.write('2');
          break;
        case '\u0663':
          sb.write('3');
          break;
        case '\u0664':
          sb.write('4');
          break;
        case '\u0665':
          sb.write('5');
          break;
        case '\u0666':
          sb.write('6');
          break;
        case '\u0667':
          sb.write('7');
          break;
        case '\u0668':
          sb.write('8');
          break;
        case '\u0669':
          sb.write('9');
          break;
        default:
          sb.write(s[i]);
          break;
      }
    }
    return sb.toString();
  }
}
