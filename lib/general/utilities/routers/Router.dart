part of 'RouterImports.dart';


@AdaptiveAutoRouter(
  routes: <AutoRoute>[
    //general routes
    AdaptiveRoute(page: Splash, initial: true,),
    CustomRoute(page: Login,),
    AdaptiveRoute(page: ForgetPassword),
    AdaptiveRoute(page: ActiveAccount),
    AdaptiveRoute(page: ResetPassword),
    AdaptiveRoute(page: SelectLang),
    AdaptiveRoute(page: Terms),
    AdaptiveRoute(page: About),
    AdaptiveRoute(page: ContactUs),
    CustomRoute(page: SelectUser,transitionsBuilder: TransitionsBuilders.fadeIn,durationInMilliseconds: 1500),
    AdaptiveRoute(page: ConfirmPassword),
    AdaptiveRoute(page: ChangePassword),
    AdaptiveRoute(page: ImageZoom),

    // customer routes
    AdaptiveRoute(page: Register),
    AdaptiveRoute(page: Home),
    AdaptiveRoute(page: CategoryDetails),
    AdaptiveRoute(page: Cart),
    AdaptiveRoute(page: MyOrders),
    AdaptiveRoute(page: OrderDetails),
    AdaptiveRoute<ProductModel?>(page: ProductDetails),
    AdaptiveRoute(page: ProfileSettings),
    AdaptiveRoute(page: Notifications),
    AdaptiveRoute(page: UserAddresses),
    AdaptiveRoute(page: AddUpdateAddress),

    AdaptiveRoute(page: Return),
    AdaptiveRoute(page: Success),
    AdaptiveRoute(page: Address),
    AdaptiveRoute(page: CompleteOrder),
    AdaptiveRoute(page: Locations),
    AdaptiveRoute(page: Languages),
    AdaptiveRoute(page: Search),
    AdaptiveRoute(page: PrintCompleteOrder),
    AdaptiveRoute(page: PrintAddress),

  ],
)
class $AppRouter {}