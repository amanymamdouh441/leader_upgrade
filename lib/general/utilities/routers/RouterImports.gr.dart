// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i34;
import 'package:leader_upgrade/customer/models/cart_model.dart' as _i39;
import 'package:leader_upgrade/customer/models/Dtos/AddOrderModel.dart' as _i40;
import 'package:leader_upgrade/customer/models/Dtos/AddPrintOrderModel.dart'
    as _i41;
import 'package:leader_upgrade/customer/models/ImageModel.dart' as _i37;
import 'package:leader_upgrade/customer/models/product_model.dart' as _i36;
import 'package:leader_upgrade/customer/models/sub_category.dart' as _i38;
import 'package:leader_upgrade/customer/screens/add_update_address/AddAddressesImports.dart'
    as _i24;
import 'package:leader_upgrade/customer/screens/address/AddressImports.dart'
    as _i27;
import 'package:leader_upgrade/customer/screens/cart/CartImports.dart' as _i17;
import 'package:leader_upgrade/customer/screens/category_details/CategoryDetailsImports.dart'
    as _i16;
import 'package:leader_upgrade/customer/screens/complete_order/CompleteOrderImports.dart'
    as _i28;
import 'package:leader_upgrade/customer/screens/home/HomeImports.dart' as _i15;
import 'package:leader_upgrade/customer/screens/languages/LanguagesImports.dart'
    as _i30;
import 'package:leader_upgrade/customer/screens/locations/LocationsImports.dart'
    as _i29;
import 'package:leader_upgrade/customer/screens/my_orders/MyOrdersImports.dart'
    as _i18;
import 'package:leader_upgrade/customer/screens/notifications/NotificationsImports.dart'
    as _i22;
import 'package:leader_upgrade/customer/screens/order_details/OrderDetailsImports.dart'
    as _i19;
import 'package:leader_upgrade/customer/screens/print_address/PrintAddressImports.dart'
    as _i33;
import 'package:leader_upgrade/customer/screens/print_complete_order/PrintCompleteOrderImports.dart'
    as _i32;
import 'package:leader_upgrade/customer/screens/product_details/ProductDetailsImports.dart'
    as _i20;
import 'package:leader_upgrade/customer/screens/profile_settings/ProfileSettingsImports.dart'
    as _i21;
import 'package:leader_upgrade/customer/screens/register/RegisterImports.dart'
    as _i14;
import 'package:leader_upgrade/customer/screens/return/ReturnImports.dart'
    as _i25;
import 'package:leader_upgrade/customer/screens/search/SearchImports.dart'
    as _i31;
import 'package:leader_upgrade/customer/screens/success/SuccessImports.dart'
    as _i26;
import 'package:leader_upgrade/customer/screens/user_addresses/UserAddressesImports.dart'
    as _i23;
import 'package:leader_upgrade/general/screens/about/AboutImports.dart' as _i8;
import 'package:leader_upgrade/general/screens/active_account/ActiveAccountImports.dart'
    as _i4;
import 'package:leader_upgrade/general/screens/change_password/ChangePasswordImports.dart'
    as _i12;
import 'package:leader_upgrade/general/screens/confirm_password/ConfirmPasswordImports.dart'
    as _i11;
import 'package:leader_upgrade/general/screens/contact_us/ContactUsImports.dart'
    as _i9;
import 'package:leader_upgrade/general/screens/forget_password/ForgetPasswordImports.dart'
    as _i3;
import 'package:leader_upgrade/general/screens/image_zoom/ImageZoom.dart' as _i13;
import 'package:leader_upgrade/general/screens/login/LoginImports.dart' as _i2;
import 'package:leader_upgrade/general/screens/reset_password/ResetPasswordImports.dart'
    as _i5;
import 'package:leader_upgrade/general/screens/select_lang/SelectLangImports.dart'
    as _i6;
import 'package:leader_upgrade/general/screens/select_user/SelectUserImports.dart'
    as _i10;
import 'package:leader_upgrade/general/screens/splash/SplashImports.dart' as _i1;
import 'package:leader_upgrade/general/screens/terms/TermsImports.dart' as _i7;
import 'package:flutter/material.dart' as _i35;

class AppRouter extends _i34.RootStackRouter {
  AppRouter([_i35.GlobalKey<_i35.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i34.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) {
      final args = routeData.argsAs<SplashRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i1.Splash(navigatorKey: args.navigatorKey));
    },
    LoginRoute.name: (routeData) {
      return _i34.CustomPage<dynamic>(
          routeData: routeData,
          child: _i2.Login(),
          opaque: true,
          barrierDismissible: false);
    },
    ForgetPasswordRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i3.ForgetPassword());
    },
    ActiveAccountRoute.name: (routeData) {
      final args = routeData.argsAs<ActiveAccountRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i4.ActiveAccount(userId: args.userId));
    },
    ResetPasswordRoute.name: (routeData) {
      final args = routeData.argsAs<ResetPasswordRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i5.ResetPassword(userId: args.userId, code: args.code));
    },
    SelectLangRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i6.SelectLang());
    },
    TermsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i7.Terms());
    },
    AboutRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i8.About());
    },
    ContactUsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i9.ContactUs());
    },
    SelectUserRoute.name: (routeData) {
      return _i34.CustomPage<dynamic>(
          routeData: routeData,
          child: _i10.SelectUser(),
          transitionsBuilder: _i34.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 1500,
          opaque: true,
          barrierDismissible: false);
    },
    ConfirmPasswordRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i11.ConfirmPassword());
    },
    ChangePasswordRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i12.ChangePassword());
    },
    ImageZoomRoute.name: (routeData) {
      final args = routeData.argsAs<ImageZoomRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i13.ImageZoom(images: args.images, index: args.index));
    },
    RegisterRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i14.Register());
    },
    HomeRoute.name: (routeData) {
      final args =
          routeData.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i15.Home(index: args.index));
    },
    CategoryDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<CategoryDetailsRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i16.CategoryDetails(key: args.key, model: args.model));
    },
    CartRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i17.Cart());
    },
    MyOrdersRoute.name: (routeData) {
      final args = routeData.argsAs<MyOrdersRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i18.MyOrders(index: args.index));
    },
    OrderDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<OrderDetailsRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i19.OrderDetails(orderId: args.orderId));
    },
    ProductDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<ProductDetailsRouteArgs>();
      return _i34.AdaptivePage<_i36.ProductModel?>(
          routeData: routeData,
          child: _i20.ProductDetails(
              key: args.key,
              id: args.id,
              fromWhere: args.fromWhere,
              fromNormal: args.fromNormal));
    },
    ProfileSettingsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i21.ProfileSettings());
    },
    NotificationsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i22.Notifications());
    },
    UserAddressesRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i23.UserAddresses());
    },
    AddUpdateAddressRoute.name: (routeData) {
      final args = routeData.argsAs<AddUpdateAddressRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i24.AddUpdateAddress(
              key: args.key,
              bottomButtonName: args.bottomButtonName,
              appBarTitle: args.appBarTitle,
              id: args.id,
              lng: args.lng,
              lat: args.lat,
              addAddress: args.addAddress));
    },
    ReturnRoute.name: (routeData) {
      final args = routeData.argsAs<ReturnRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i25.Return(orderId: args.orderId));
    },
    SuccessRoute.name: (routeData) {
      final args = routeData.argsAs<SuccessRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i26.Success(payment: args.payment));
    },
    AddressRoute.name: (routeData) {
      final args = routeData.argsAs<AddressRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i27.Address(key: args.key, model: args.model));
    },
    CompleteOrderRoute.name: (routeData) {
      final args = routeData.argsAs<CompleteOrderRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i28.CompleteOrder(
              cartModel: args.cartModel,
              addOrderModel: args.addOrderModel,
              cityName: args.cityName));
    },
    LocationsRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i29.Locations());
    },
    LanguagesRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i30.Languages());
    },
    SearchRoute.name: (routeData) {
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData, child: _i31.Search());
    },
    PrintCompleteOrderRoute.name: (routeData) {
      final args = routeData.argsAs<PrintCompleteOrderRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i32.PrintCompleteOrder(
              addPrintOrderModel: args.addPrintOrderModel,
              printAddressData: args.printAddressData));
    },
    PrintAddressRoute.name: (routeData) {
      final args = routeData.argsAs<PrintAddressRouteArgs>();
      return _i34.AdaptivePage<dynamic>(
          routeData: routeData,
          child:
              _i33.PrintAddress(addPrintOrderModel: args.addPrintOrderModel));
    }
  };

  @override
  List<_i34.RouteConfig> get routes => [
        _i34.RouteConfig(SplashRoute.name, path: '/'),
        _i34.RouteConfig(LoginRoute.name, path: '/Login'),
        _i34.RouteConfig(ForgetPasswordRoute.name, path: '/forget-password'),
        _i34.RouteConfig(ActiveAccountRoute.name, path: '/active-account'),
        _i34.RouteConfig(ResetPasswordRoute.name, path: '/reset-password'),
        _i34.RouteConfig(SelectLangRoute.name, path: '/select-lang'),
        _i34.RouteConfig(TermsRoute.name, path: '/Terms'),
        _i34.RouteConfig(AboutRoute.name, path: '/About'),
        _i34.RouteConfig(ContactUsRoute.name, path: '/contact-us'),
        _i34.RouteConfig(SelectUserRoute.name, path: '/select-user'),
        _i34.RouteConfig(ConfirmPasswordRoute.name, path: '/confirm-password'),
        _i34.RouteConfig(ChangePasswordRoute.name, path: '/change-password'),
        _i34.RouteConfig(ImageZoomRoute.name, path: '/image-zoom'),
        _i34.RouteConfig(RegisterRoute.name, path: '/Register'),
        _i34.RouteConfig(HomeRoute.name, path: '/Home'),
        _i34.RouteConfig(CategoryDetailsRoute.name, path: '/category-details'),
        _i34.RouteConfig(CartRoute.name, path: '/Cart'),
        _i34.RouteConfig(MyOrdersRoute.name, path: '/my-orders'),
        _i34.RouteConfig(OrderDetailsRoute.name, path: '/order-details'),
        _i34.RouteConfig(ProductDetailsRoute.name, path: '/product-details'),
        _i34.RouteConfig(ProfileSettingsRoute.name, path: '/profile-settings'),
        _i34.RouteConfig(NotificationsRoute.name, path: '/Notifications'),
        _i34.RouteConfig(UserAddressesRoute.name, path: '/user-addresses'),
        _i34.RouteConfig(AddUpdateAddressRoute.name,
            path: '/add-update-address'),
        _i34.RouteConfig(ReturnRoute.name, path: '/Return'),
        _i34.RouteConfig(SuccessRoute.name, path: '/Success'),
        _i34.RouteConfig(AddressRoute.name, path: '/Address'),
        _i34.RouteConfig(CompleteOrderRoute.name, path: '/complete-order'),
        _i34.RouteConfig(LocationsRoute.name, path: '/Locations'),
        _i34.RouteConfig(LanguagesRoute.name, path: '/Languages'),
        _i34.RouteConfig(SearchRoute.name, path: '/Search'),
        _i34.RouteConfig(PrintCompleteOrderRoute.name,
            path: '/print-complete-order'),
        _i34.RouteConfig(PrintAddressRoute.name, path: '/print-address')
      ];
}

/// generated route for [_i1.Splash]
class SplashRoute extends _i34.PageRouteInfo<SplashRouteArgs> {
  SplashRoute({required _i35.GlobalKey<_i35.NavigatorState> navigatorKey})
      : super(name,
            path: '/', args: SplashRouteArgs(navigatorKey: navigatorKey));

  static const String name = 'SplashRoute';
}

class SplashRouteArgs {
  const SplashRouteArgs({required this.navigatorKey});

  final _i35.GlobalKey<_i35.NavigatorState> navigatorKey;
}

/// generated route for [_i2.Login]
class LoginRoute extends _i34.PageRouteInfo<void> {
  const LoginRoute() : super(name, path: '/Login');

  static const String name = 'LoginRoute';
}

/// generated route for [_i3.ForgetPassword]
class ForgetPasswordRoute extends _i34.PageRouteInfo<void> {
  const ForgetPasswordRoute() : super(name, path: '/forget-password');

  static const String name = 'ForgetPasswordRoute';
}

/// generated route for [_i4.ActiveAccount]
class ActiveAccountRoute extends _i34.PageRouteInfo<ActiveAccountRouteArgs> {
  ActiveAccountRoute({required String userId})
      : super(name,
            path: '/active-account',
            args: ActiveAccountRouteArgs(userId: userId));

  static const String name = 'ActiveAccountRoute';
}

class ActiveAccountRouteArgs {
  const ActiveAccountRouteArgs({required this.userId});

  final String userId;
}

/// generated route for [_i5.ResetPassword]
class ResetPasswordRoute extends _i34.PageRouteInfo<ResetPasswordRouteArgs> {
  ResetPasswordRoute({required String userId, required int code})
      : super(name,
            path: '/reset-password',
            args: ResetPasswordRouteArgs(userId: userId, code: code));

  static const String name = 'ResetPasswordRoute';
}

class ResetPasswordRouteArgs {
  const ResetPasswordRouteArgs({required this.userId, required this.code});

  final String userId;

  final int code;
}

/// generated route for [_i6.SelectLang]
class SelectLangRoute extends _i34.PageRouteInfo<void> {
  const SelectLangRoute() : super(name, path: '/select-lang');

  static const String name = 'SelectLangRoute';
}

/// generated route for [_i7.Terms]
class TermsRoute extends _i34.PageRouteInfo<void> {
  const TermsRoute() : super(name, path: '/Terms');

  static const String name = 'TermsRoute';
}

/// generated route for [_i8.About]
class AboutRoute extends _i34.PageRouteInfo<void> {
  const AboutRoute() : super(name, path: '/About');

  static const String name = 'AboutRoute';
}

/// generated route for [_i9.ContactUs]
class ContactUsRoute extends _i34.PageRouteInfo<void> {
  const ContactUsRoute() : super(name, path: '/contact-us');

  static const String name = 'ContactUsRoute';
}

/// generated route for [_i10.SelectUser]
class SelectUserRoute extends _i34.PageRouteInfo<void> {
  const SelectUserRoute() : super(name, path: '/select-user');

  static const String name = 'SelectUserRoute';
}

/// generated route for [_i11.ConfirmPassword]
class ConfirmPasswordRoute extends _i34.PageRouteInfo<void> {
  const ConfirmPasswordRoute() : super(name, path: '/confirm-password');

  static const String name = 'ConfirmPasswordRoute';
}

/// generated route for [_i12.ChangePassword]
class ChangePasswordRoute extends _i34.PageRouteInfo<void> {
  const ChangePasswordRoute() : super(name, path: '/change-password');

  static const String name = 'ChangePasswordRoute';
}

/// generated route for [_i13.ImageZoom]
class ImageZoomRoute extends _i34.PageRouteInfo<ImageZoomRouteArgs> {
  ImageZoomRoute({required List<_i37.ImageModel> images, required int index})
      : super(name,
            path: '/image-zoom',
            args: ImageZoomRouteArgs(images: images, index: index));

  static const String name = 'ImageZoomRoute';
}

class ImageZoomRouteArgs {
  const ImageZoomRouteArgs({required this.images, required this.index});

  final List<_i37.ImageModel> images;

  final int index;
}

/// generated route for [_i14.Register]
class RegisterRoute extends _i34.PageRouteInfo<void> {
  const RegisterRoute() : super(name, path: '/Register');

  static const String name = 'RegisterRoute';
}

/// generated route for [_i15.Home]
class HomeRoute extends _i34.PageRouteInfo<HomeRouteArgs> {
  HomeRoute({int? index})
      : super(name, path: '/Home', args: HomeRouteArgs(index: index));

  static const String name = 'HomeRoute';
}

class HomeRouteArgs {
  const HomeRouteArgs({this.index});

  final int? index;
}

/// generated route for [_i16.CategoryDetails]
class CategoryDetailsRoute
    extends _i34.PageRouteInfo<CategoryDetailsRouteArgs> {
  CategoryDetailsRoute({_i35.Key? key, required _i38.SubCategory model})
      : super(name,
            path: '/category-details',
            args: CategoryDetailsRouteArgs(key: key, model: model));

  static const String name = 'CategoryDetailsRoute';
}

class CategoryDetailsRouteArgs {
  const CategoryDetailsRouteArgs({this.key, required this.model});

  final _i35.Key? key;

  final _i38.SubCategory model;
}

/// generated route for [_i17.Cart]
class CartRoute extends _i34.PageRouteInfo<void> {
  const CartRoute() : super(name, path: '/Cart');

  static const String name = 'CartRoute';
}

/// generated route for [_i18.MyOrders]
class MyOrdersRoute extends _i34.PageRouteInfo<MyOrdersRouteArgs> {
  MyOrdersRoute({required int index})
      : super(name, path: '/my-orders', args: MyOrdersRouteArgs(index: index));

  static const String name = 'MyOrdersRoute';
}

class MyOrdersRouteArgs {
  const MyOrdersRouteArgs({required this.index});

  final int index;
}

/// generated route for [_i19.OrderDetails]
class OrderDetailsRoute extends _i34.PageRouteInfo<OrderDetailsRouteArgs> {
  OrderDetailsRoute({required int orderId})
      : super(name,
            path: '/order-details',
            args: OrderDetailsRouteArgs(orderId: orderId));

  static const String name = 'OrderDetailsRoute';
}

class OrderDetailsRouteArgs {
  const OrderDetailsRouteArgs({required this.orderId});

  final int orderId;
}

/// generated route for [_i20.ProductDetails]
class ProductDetailsRoute extends _i34.PageRouteInfo<ProductDetailsRouteArgs> {
  ProductDetailsRoute(
      {_i35.Key? key,
      required int id,
      String fromWhere = "anyWhere",
      String fromNormal = "normal"})
      : super(name,
            path: '/product-details',
            args: ProductDetailsRouteArgs(
                key: key,
                id: id,
                fromWhere: fromWhere,
                fromNormal: fromNormal));

  static const String name = 'ProductDetailsRoute';
}

class ProductDetailsRouteArgs {
  const ProductDetailsRouteArgs(
      {this.key,
      required this.id,
      this.fromWhere = "anyWhere",
      this.fromNormal = "normal"});

  final _i35.Key? key;

  final int id;

  final String fromWhere;

  final String fromNormal;
}

/// generated route for [_i21.ProfileSettings]
class ProfileSettingsRoute extends _i34.PageRouteInfo<void> {
  const ProfileSettingsRoute() : super(name, path: '/profile-settings');

  static const String name = 'ProfileSettingsRoute';
}

/// generated route for [_i22.Notifications]
class NotificationsRoute extends _i34.PageRouteInfo<void> {
  const NotificationsRoute() : super(name, path: '/Notifications');

  static const String name = 'NotificationsRoute';
}

/// generated route for [_i23.UserAddresses]
class UserAddressesRoute extends _i34.PageRouteInfo<void> {
  const UserAddressesRoute() : super(name, path: '/user-addresses');

  static const String name = 'UserAddressesRoute';
}

/// generated route for [_i24.AddUpdateAddress]
class AddUpdateAddressRoute
    extends _i34.PageRouteInfo<AddUpdateAddressRouteArgs> {
  AddUpdateAddressRoute(
      {_i35.Key? key,
      String? bottomButtonName,
      String? appBarTitle,
      int? id,
      double? lng,
      double? lat,
      required bool addAddress})
      : super(name,
            path: '/add-update-address',
            args: AddUpdateAddressRouteArgs(
                key: key,
                bottomButtonName: bottomButtonName,
                appBarTitle: appBarTitle,
                id: id,
                lng: lng,
                lat: lat,
                addAddress: addAddress));

  static const String name = 'AddUpdateAddressRoute';
}

class AddUpdateAddressRouteArgs {
  const AddUpdateAddressRouteArgs(
      {this.key,
      this.bottomButtonName,
      this.appBarTitle,
      this.id,
      this.lng,
      this.lat,
      required this.addAddress});

  final _i35.Key? key;

  final String? bottomButtonName;

  final String? appBarTitle;

  final int? id;

  final double? lng;

  final double? lat;

  final bool addAddress;
}

/// generated route for [_i25.Return]
class ReturnRoute extends _i34.PageRouteInfo<ReturnRouteArgs> {
  ReturnRoute({required int orderId})
      : super(name, path: '/Return', args: ReturnRouteArgs(orderId: orderId));

  static const String name = 'ReturnRoute';
}

class ReturnRouteArgs {
  const ReturnRouteArgs({required this.orderId});

  final int orderId;
}

/// generated route for [_i26.Success]
class SuccessRoute extends _i34.PageRouteInfo<SuccessRouteArgs> {
  SuccessRoute({required bool payment})
      : super(name, path: '/Success', args: SuccessRouteArgs(payment: payment));

  static const String name = 'SuccessRoute';
}

class SuccessRouteArgs {
  const SuccessRouteArgs({required this.payment});

  final bool payment;
}

/// generated route for [_i27.Address]
class AddressRoute extends _i34.PageRouteInfo<AddressRouteArgs> {
  AddressRoute({_i35.Key? key, required _i39.CartModel model})
      : super(name,
            path: '/Address', args: AddressRouteArgs(key: key, model: model));

  static const String name = 'AddressRoute';
}

class AddressRouteArgs {
  const AddressRouteArgs({this.key, required this.model});

  final _i35.Key? key;

  final _i39.CartModel model;
}

/// generated route for [_i28.CompleteOrder]
class CompleteOrderRoute extends _i34.PageRouteInfo<CompleteOrderRouteArgs> {
  CompleteOrderRoute(
      {required _i39.CartModel cartModel,
      required _i40.AddOrderModel addOrderModel,
      required String cityName})
      : super(name,
            path: '/complete-order',
            args: CompleteOrderRouteArgs(
                cartModel: cartModel,
                addOrderModel: addOrderModel,
                cityName: cityName));

  static const String name = 'CompleteOrderRoute';
}

class CompleteOrderRouteArgs {
  const CompleteOrderRouteArgs(
      {required this.cartModel,
      required this.addOrderModel,
      required this.cityName});

  final _i39.CartModel cartModel;

  final _i40.AddOrderModel addOrderModel;

  final String cityName;
}

/// generated route for [_i29.Locations]
class LocationsRoute extends _i34.PageRouteInfo<void> {
  const LocationsRoute() : super(name, path: '/Locations');

  static const String name = 'LocationsRoute';
}

/// generated route for [_i30.Languages]
class LanguagesRoute extends _i34.PageRouteInfo<void> {
  const LanguagesRoute() : super(name, path: '/Languages');

  static const String name = 'LanguagesRoute';
}

/// generated route for [_i31.Search]
class SearchRoute extends _i34.PageRouteInfo<void> {
  const SearchRoute() : super(name, path: '/Search');

  static const String name = 'SearchRoute';
}

/// generated route for [_i32.PrintCompleteOrder]
class PrintCompleteOrderRoute
    extends _i34.PageRouteInfo<PrintCompleteOrderRouteArgs> {
  PrintCompleteOrderRoute(
      {required _i41.AddPrintOrderModel addPrintOrderModel,
      required _i33.PrintAddressData printAddressData})
      : super(name,
            path: '/print-complete-order',
            args: PrintCompleteOrderRouteArgs(
                addPrintOrderModel: addPrintOrderModel,
                printAddressData: printAddressData));

  static const String name = 'PrintCompleteOrderRoute';
}

class PrintCompleteOrderRouteArgs {
  const PrintCompleteOrderRouteArgs(
      {required this.addPrintOrderModel, required this.printAddressData});

  final _i41.AddPrintOrderModel addPrintOrderModel;

  final _i33.PrintAddressData printAddressData;
}

/// generated route for [_i33.PrintAddress]
class PrintAddressRoute extends _i34.PageRouteInfo<PrintAddressRouteArgs> {
  PrintAddressRoute({required _i41.AddPrintOrderModel addPrintOrderModel})
      : super(name,
            path: '/print-address',
            args:
                PrintAddressRouteArgs(addPrintOrderModel: addPrintOrderModel));

  static const String name = 'PrintAddressRoute';
}

class PrintAddressRouteArgs {
  const PrintAddressRouteArgs({required this.addPrintOrderModel});

  final _i41.AddPrintOrderModel addPrintOrderModel;
}
