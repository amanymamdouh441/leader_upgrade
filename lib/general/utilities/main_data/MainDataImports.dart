import 'package:leader_upgrade/general/blocks/auth_cubit/auth_cubit.dart';
import 'package:leader_upgrade/general/blocks/cart_count_cubit/cart_count_cubit.dart';
import 'package:leader_upgrade/general/blocks/cats_cubit/cats_cubit.dart';
import 'package:leader_upgrade/general/blocks/notify_count_cubit/notify_count_cubit.dart';
import 'package:leader_upgrade/general/blocks/share_cubit/share_cubit.dart';
import 'package:leader_upgrade/general/blocks/sub_cats_cubit/sub_cats_cubit.dart';
import 'package:leader_upgrade/general/blocks/user_cubit/user_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:leader_upgrade/general/constants/MyColors.dart';


part 'MainData.dart';