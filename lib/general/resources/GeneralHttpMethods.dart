part of 'GeneralRepoImports.dart';

class GeneralHttpMethods {
  final BuildContext context;

  FirebaseMessaging messaging = FirebaseMessaging.instance;

  GeneralHttpMethods(this.context);

  Future<bool> userLogin(String phone, String pass, String cart) async {
    String? _token = await messaging.getToken();
    Map<String, dynamic> body = {
      "phone": "$phone",
      "password": "$pass",
      "addCartDtoJson": cart,
      "deviceId": "$_token",
      "projectName": "Leader",
      "deviceType": Platform.isIOS ? "ios" : "android",
    };

    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.login,
      json: body,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      returnDataFun: (data) => data,
      showLoader: false,
    );

    return Utils.manipulateLoginData(context, data["data"], _token ?? "");
  }

  Future<void> updateProfile(UpdateUserModel model) async {
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.updateProfile,
      returnType: ReturnType.Type,
      json: model.toJson(),
      showLoader: true,
      methodType: MethodType.UploadFile,
    );
    if (data != null) {
      UserModel user = UserModel.fromJson(data["data"]);
      user.token = GlobalState.instance.get("token");
      user.lang = context.read<LangCubit>().state.locale.languageCode;
      await Utils.saveUserData(user);
      context.read<UserCubit>().onUpdateUserData(user);
    }
  }

  Future<void> userRegister(RegisterModel model) async {
    String? _token = await messaging.getToken();
    model.deviceId = _token;
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.register,
      json: model.toJson(),
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      showLoader: false,
    );
    if (data != null) {
      AutoRouter.of(context)
          .popAndPush(ActiveAccountRoute(userId: data["data"]["id"]));
    }
  }

  Future<bool> sendCode(String code, String userId) async {
    Map<String, dynamic> body = {"code": code, "userId": userId};
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.sendCode,
      json: body,
      returnType: ReturnType.Type,
      showLoader: false,
      methodType: MethodType.Post,
    );
    return (data != null);
  }

  Future<bool> resendCode(String userId) async {
    Map<String, dynamic> body = {"userId": userId};
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.resendCode,
      json: body,
      returnType: ReturnType.Type,
      showLoader: true,
      methodType: MethodType.Post,
    );
    return (data != null);
  }

  Future<String?> aboutApp() async {
    return await PrevGenericHttp<String>(context).callApi(
        name: ApiNames.aboutApp,
        returnType: ReturnType.Type,
        showLoader: false,
        methodType: MethodType.Get,
        returnDataFun: (data) => data["data"]["aboutUs"]);
  }

  Future<String?> terms() async {
    return await PrevGenericHttp<String>(context).callApi(
        name: ApiNames.terms,
        returnType: ReturnType.Type,
        showLoader: false,
        methodType: MethodType.Get,
        returnDataFun: (data) => data["data"]["condtions"]);
  }

  Future<List<QuestionModel>> frequentQuestions() async {
    return await PrevGenericHttp<QuestionModel>(context).callApi(
      name: ApiNames.repeatedQuestions,
      returnType: ReturnType.List,
      showLoader: false,
      methodType: MethodType.Get,
    ) as List<QuestionModel>;
  }

  Future<List<SocialModel>> getSocialMedia(bool refresh) async {
    return await PrevGenericHttp<SocialModel>(context).callApi(
        name: ApiNames.socials,
        returnType: ReturnType.List,
        showLoader: false,
        refresh: refresh,
        methodType: MethodType.Get,
        toJsonFunc: (json) => SocialModel.fromJson(json),
        returnDataFun: (data) => data["data"]) as List<SocialModel>;
  }

  Future<bool> switchNotify() async {
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.switchNotify,
      returnType: ReturnType.Type,
      showLoader: false,
      methodType: MethodType.Post,
    );
    return (data != null);
  }

  Future<void> forgetPassword(String phone) async {
    Map<String, dynamic> body = {
      "phone": "$phone",
    };
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.forgetPassword,
      returnType: ReturnType.Type,
      json: body,
      showLoader: false,
      methodType: MethodType.Post,
    );
    if (data != null) {
      AutoRouter.of(context).push(ResetPasswordRoute(
        userId: data["data"]["userId"],
        code: data["data"]["code"],
      ));
    }
  }

  Future<bool> resetUserPassword(
      String userId, String code, String pass) async {
    Map<String, dynamic> body = {
      "userId": "$userId",
      "code": "$code",
      "newPassword": "$pass",
    };
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.resetPassword,
      returnType: ReturnType.Type,
      json: body,
      showLoader: false,
      methodType: MethodType.Post,
    );
    return (data != null);
  }

  Future<SettingModel> getSetting(bool refresh) async {
    return await PrevGenericHttp<SettingModel>(context).callApi(
      returnType: ReturnType.Model,
      methodType: MethodType.Get,
      name: ApiNames.getSetting,
      refresh: refresh,
      returnDataFun: (data) => data["data"],
      toJsonFunc: (json) => SettingModel.fromJson(json),
    ) as SettingModel;
  }

  Future<bool> sendMessage(String? name, String? mail, String? message) async {
    Map<String, dynamic> body = {
      "userName": "$name",
      "email": "$mail",
      "msg": "$message",
    };
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.contactUs,
      returnType: ReturnType.Type,
      json: body,
      showLoader: true,
      methodType: MethodType.Post,
    );
    return (data != null);
  }

  Future<bool> changeLanguage(String lang) async {
    Map<String, dynamic> body = {
      "language": lang,
    };
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.changeLang,
      returnType: ReturnType.Type,
      json: body,
      showLoader: false,
      methodType: MethodType.Post,
    );
    return (data != null);
  }

  Future<void> logout() async {
    String? deviceId = await messaging.getToken();
    dynamic data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.logout,
      returnType: ReturnType.Type,
      json: {"deviceId": deviceId},
      showLoader: true,
      methodType: MethodType.Post,
    );
    print(deviceId.toString()+'deviceIdlogout');
    if (data != null) {
      EasyLoading.dismiss().then((value) {
        Utils.clearSavedData();
        GlobalState.instance.set("token", "");
        Phoenix.rebirth(context);
      });
    }
  }

  Future<void> deleteAccount() async {
    String lang = context.read<LangCubit>().state.locale.languageCode;
    String param = "?lang=$lang";
    var data = await PrevGenericHttp<dynamic>(context).callApi(
      name: ApiNames.deleteAccount + param,
      returnType: ReturnType.Type,
      methodType: MethodType.Post,
      showLoader: true,
    );
    if (data != null) {
      EasyLoading.dismiss().then((value) {
        Utils.clearSavedData();
        GlobalState.instance.set("token", "");
        Phoenix.rebirth(context);
      });
    }
  }
}
